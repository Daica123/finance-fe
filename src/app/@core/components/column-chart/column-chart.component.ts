import {AfterViewChecked, Component, Input, Output, EventEmitter} from '@angular/core';
import {TableUtil} from "@app/utils/tableUtils";
import {SettingService} from "@finance/settings/services/setting.service";

@Component({
    selector	: 'column-chart',
    templateUrl	: './column-chart.html',
    styleUrls	: [ './column-chart.css' ],
})
export class ColumnChartComponent implements AfterViewChecked {
    @Input() public data: number[] = [];
    @Output() errorHandler: EventEmitter<boolean> = new EventEmitter<boolean>();
    public baseNumber: number = 0;
    public baseHeight: number = 0;
    public xAxyses: number[] = [];
    public numOfTickers: number = 0;
    public shows: boolean[] = [];
    public setting: any[] = [];
    public errorData: boolean = false;

    constructor(public settingService: SettingService) {
        this.shows = [true, true, true, true];
        this.settingService.getAll([]).subscribe((setting: any) => {
            this.setting = setting;
        });
    }

    public OVERVIEW_CHART_PAYABLE_COLOR() {
        const settingObject: any = this.setting.find((st: any) => st.key === 'OVERVIEW_CHART_PAYABLE_COLOR');
        return settingObject ? settingObject.value : '#ffffff';
    }

    public OVERVIEW_CHART_PAYABLE_PLAN_COLOR() {
        const settingObject: any = this.setting.find((st: any) => st.key === 'OVERVIEW_CHART_PAYABLE_PLAN_COLOR');
        return settingObject ? settingObject.value : '#ffffff';
    }

    public OVERVIEW_CHART_PAYABLE_PLAN_VO_COLOR() {
        const settingObject: any = this.setting.find((st: any) => st.key === 'OVERVIEW_CHART_PAYABLE_PLAN_VO_COLOR');
        return settingObject ? settingObject.value : '#ffffff';
    }

    public OVERVIEW_CHART_PAYABLE_REMAIN_COLOR() {
        const settingObject: any = this.setting.find((st: any) => st.key === 'OVERVIEW_CHART_PAYABLE_REMAIN_COLOR');
        return settingObject ? settingObject.value : '#ffffff';
    }

    public OVERVIEW_CHART_RECEIVABLE_COLOR() {
        const settingObject: any = this.setting.find((st: any) => st.key === 'OVERVIEW_CHART_RECEIVABLE_COLOR');
        return settingObject ? settingObject.value : '#ffffff';
    }

    public OVERVIEW_CHART_RECEIVABLE_PLAN_COLOR() {
        const settingObject: any = this.setting.find((st: any) => st.key === 'OVERVIEW_CHART_RECEIVABLE_PLAN_COLOR');
        return settingObject ? settingObject.value : '#ffffff';
    }

    public OVERVIEW_CHART_RECEIVABLE_PLAN_VO_COLOR() {
        const settingObject: any = this.setting.find((st: any) => st.key === 'OVERVIEW_CHART_RECEIVABLE_PLAN_VO_COLOR');
        return settingObject ? settingObject.value : '#ffffff';
    }

    ngAfterViewChecked(): void {
        this.xAxyses = [];
        let dataTemp: number[] = [];
        if (this.data.length > 2) {
            dataTemp.push(Math.abs(this.data[0]) + Math.abs(this.data[1]));
            dataTemp.push(Math.abs(this.data[2]) + Math.abs(this.data[3]));
            dataTemp.push(Math.abs(this.data[4]) + Math.abs(this.data[5]));
            dataTemp.push(Math.abs(this.data[6]) + Math.abs(this.data[7]));
        } else {
            dataTemp = this.data;
        }
        const maxNumber: number = Math.max(...dataTemp.map(i => Math.abs(i)));
        const vStr: string = (parseInt(maxNumber.toString(10), 10)).toString(10);
        const length = vStr.length;
        this.baseNumber = Math.pow(10, length - 1);
        this.numOfTickers = Math.round(maxNumber / this.baseNumber);
        if (this.numOfTickers < 4) {
            this.baseNumber = this.baseNumber / 4;
            this.numOfTickers = Math.round(maxNumber / this.baseNumber);
        }
        this.baseHeight = 300 / this.numOfTickers;
        if (Number.isNaN(this.data[0] / this.baseNumber * this.baseHeight)) {
            this.errorData = true;
            this.errorHandler.emit(true);
            return;
        }
        for (let i = 1; i <= this.numOfTickers; i++) {
            this.xAxyses.push(this.baseNumber * i);
        }
        for (let i = 0; i < this.numOfTickers; i++) {
            const ticker = document.getElementById('ticker_' + i);
            const tickerLabel = document.getElementById('ticker_label_' + i);
            try {
                ticker.setAttribute('y1', (350 - (i + 1) * this.baseHeight).toString(10));
                ticker.setAttribute('y2', (350 - (i + 1) * this.baseHeight).toString(10));
                tickerLabel.setAttribute('y', (350 - (i + 1) * this.baseHeight - 5).toString(10));
            } catch (e) {

            }
        }
        if (this.data.length > 2) {
            try {
                const plan_receivable = document.getElementById('plan_receivable');
                const real_receivable = document.getElementById('real_receivable');
                const plan_payable = document.getElementById('plan_payable');
                const real_payable = document.getElementById('real_payable');
                const rext1_1 = document.getElementById('rect1_1');
                const rect1_1_text = document.getElementById('rect1_1_text');
                const rext1_2 = document.getElementById('rect1_2');
                plan_receivable.style.background = this.OVERVIEW_CHART_RECEIVABLE_PLAN_COLOR();
                plan_receivable.style.border = '4px solid ' + this.OVERVIEW_CHART_RECEIVABLE_PLAN_COLOR();
                real_receivable.style.background = this.OVERVIEW_CHART_RECEIVABLE_COLOR();
                real_receivable.style.border = '4px solid ' + this.OVERVIEW_CHART_RECEIVABLE_COLOR();
                plan_payable.style.background = this.OVERVIEW_CHART_PAYABLE_PLAN_COLOR();
                plan_payable.style.border = '4px solid ' + this.OVERVIEW_CHART_PAYABLE_PLAN_COLOR();
                real_payable.style.background = this.OVERVIEW_CHART_PAYABLE_COLOR();
                real_payable.style.border = '4px solid ' + this.OVERVIEW_CHART_PAYABLE_COLOR();
                rext1_1.setAttribute('fill', this.OVERVIEW_CHART_RECEIVABLE_PLAN_COLOR());
                rext1_1.setAttribute('height', (this.data[0] / this.baseNumber * this.baseHeight).toString(10));
                rext1_1.setAttribute('y', (350 - this.data[0] / this.baseNumber * this.baseHeight).toString(10));
                rect1_1_text.innerHTML = this.commas(this.data[0]/1000000000 || 0, 1) + ' / ' + this.commas(this.data[1]/1000000000 || 0, 1);
                rext1_2.setAttribute('fill', this.OVERVIEW_CHART_RECEIVABLE_PLAN_VO_COLOR());
                rext1_2.setAttribute('height', (Math.abs(this.data[1] / this.baseNumber) * this.baseHeight).toString(10));
                rect1_1_text.parentElement.setAttribute('y', (350 - this.data[0] / this.baseNumber * this.baseHeight - 13).toString(10));
                rect1_1_text.parentElement.setAttribute('x', (250 - rect1_1_text.clientWidth / 2).toString(10));
                if (this.data[1] < 0) {
                    rext1_2.setAttribute('y', (350 - this.data[0] / this.baseNumber * this.baseHeight).toString(10));
                } else {
                    rect1_1_text.parentElement.setAttribute('y', (350 - this.data[0] / this.baseNumber * this.baseHeight - this.data[1] / this.baseNumber * this.baseHeight - 13).toString(10));
                    rext1_2.setAttribute('y', (350 - this.data[0] / this.baseNumber * this.baseHeight - this.data[1] / this.baseNumber * this.baseHeight).toString(10));
                }

                const rext2_1 = document.getElementById('rect2_1');
                const rect2_1_text = document.getElementById('rect2_1_text');
                rext2_1.setAttribute('fill', this.OVERVIEW_CHART_RECEIVABLE_COLOR());
                rext2_1.setAttribute('height', (this.data[2] / this.baseNumber * this.baseHeight).toString(10));
                rext2_1.setAttribute('y', (350 - this.data[2] / this.baseNumber * this.baseHeight).toString(10));
                rect2_1_text.parentElement.setAttribute('x', (360 - rect2_1_text.clientWidth / 2).toString(10));
                rect2_1_text.innerHTML = this.commas(this.data[2]/1000000000 || 0, 1);
                rect2_1_text.parentElement.setAttribute('y', (350 - this.data[2] / this.baseNumber * this.baseHeight - 13).toString(10));
                const rext3_1 = document.getElementById('rect3_1');
                const rect3_1_text = document.getElementById('rect3_1_text');
                const rext3_2 = document.getElementById('rect3_2');
                rext3_1.setAttribute('fill', this.OVERVIEW_CHART_PAYABLE_PLAN_COLOR());
                rext3_1.setAttribute('height', (this.data[4] / this.baseNumber * this.baseHeight).toString(10));
                rext3_1.setAttribute('y', (350 - this.data[4] / this.baseNumber * this.baseHeight).toString(10));
                rect3_1_text.innerHTML = this.commas(this.data[4]/1000000000 || 0, 1) + ' / ' + this.commas(this.data[5]/1000000000 || 0, 1);
                rext3_2.setAttribute('fill', this.OVERVIEW_CHART_PAYABLE_PLAN_VO_COLOR());
                rext3_2.setAttribute('height', (Math.abs(this.data[5] / this.baseNumber * this.baseHeight)).toString(10));
                rect3_1_text.parentElement.setAttribute('y', (350 - this.data[4] / this.baseNumber * this.baseHeight - 13).toString(10));
                rect3_1_text.parentElement.setAttribute('x', (470 - rect3_1_text.clientWidth / 2).toString(10));
                if (this.data[5] < 0) {
                    rext3_2.setAttribute('y', (350 - this.data[4] / this.baseNumber * this.baseHeight).toString(10));
                } else {
                    rect3_1_text.parentElement.setAttribute('y', (350 - this.data[4] / this.baseNumber * this.baseHeight - this.data[5] / this.baseNumber * this.baseHeight - 13).toString(10));
                    rext3_2.setAttribute('y', (350 - this.data[4] / this.baseNumber * this.baseHeight - this.data[5] / this.baseNumber * this.baseHeight).toString(10));
                }

                const rext4_1 = document.getElementById('rect4_1');
                const rect4_1_text = document.getElementById('rect4_1_text');
                const rext4_2 = document.getElementById('rect4_2');
                rext4_1.setAttribute('fill', this.OVERVIEW_CHART_PAYABLE_COLOR());
                rext4_1.setAttribute('height', (this.data[6] / this.baseNumber * this.baseHeight).toString(10));
                rext4_1.setAttribute('y', (350 - this.data[6] / this.baseNumber * this.baseHeight).toString(10));
                rect4_1_text.innerHTML = this.commas(this.data[6]/1000000000 || 0,1) + ' / ' + this.commas(this.data[7]/1000000000 || 0, 1);
                rext4_2.setAttribute('fill', this.OVERVIEW_CHART_PAYABLE_REMAIN_COLOR());
                rext4_2.setAttribute('height', (Math.abs(this.data[7] / this.baseNumber * this.baseHeight)).toString(10));
                rect4_1_text.parentElement.setAttribute('y', (350 - this.data[6] / this.baseNumber * this.baseHeight - 13).toString(10));
                rect4_1_text.parentElement.setAttribute('x', (580 - rect4_1_text.clientWidth / 2).toString(10));
                if (this.data[7] < 0) {
                    rext4_2.setAttribute('y', (350 - this.data[6] / this.baseNumber * this.baseHeight).toString(10));
                } else {
                    rect4_1_text.parentElement.setAttribute('y', (350 - this.data[6] / this.baseNumber * this.baseHeight - this.data[7] / this.baseNumber * this.baseHeight - 13).toString(10));
                    rext4_2.setAttribute('y', (350 - this.data[6] / this.baseNumber * this.baseHeight - this.data[7] / this.baseNumber * this.baseHeight).toString(10));
                }
            } catch (e) {
            }
        } else if (this.data.length === 2) {
            if ((this.data[0] / this.baseNumber * this.baseHeight)) {
                const height1 =  this.data[0] / this.baseNumber * this.baseHeight;
                const y1 =  350 - height1;
                const rext1 = document.getElementById('rect1');
                const rect1_text = document.getElementById('rect1_text');
                const total_line = document.getElementById('total_line');
                if (total_line) {
                    total_line.style.background = this.OVERVIEW_CHART_RECEIVABLE_PLAN_COLOR();
                    total_line.style.border = '4px solid ' + this.OVERVIEW_CHART_RECEIVABLE_PLAN_COLOR();
                }
                if (rext1) {
                    rext1.setAttribute('fill', this.OVERVIEW_CHART_RECEIVABLE_PLAN_COLOR());
                    rext1.setAttribute('height', height1.toString(10));
                    rext1.setAttribute('y', y1.toString(10));
                    rect1_text.innerHTML = this.commas(this.data[0]/1000000000 || 0, 1);
                    rect1_text.parentElement.setAttribute('y', (y1 - 13).toString(10));
                    rect1_text.parentElement.setAttribute('x', (300 - rect1_text.clientWidth / 2).toString(10));
                }
            }
            if ((this.data[1] / this.baseNumber * this.baseHeight)) {
                const height2 =  this.data[1] / this.baseNumber * this.baseHeight;
                const y2 =  350 - height2;
                const rext2 = document.getElementById('rect2');
                const rect2_text = document.getElementById('rect2_text');
                const total_bill = document.getElementById('total_bill');
                if (total_bill) {
                    total_bill.style.background = this.OVERVIEW_CHART_PAYABLE_PLAN_COLOR();
                    total_bill.style.border = '4px solid ' + this.OVERVIEW_CHART_PAYABLE_PLAN_COLOR();
                }
                if (rext2) {
                    rext2.setAttribute('fill', this.OVERVIEW_CHART_PAYABLE_PLAN_COLOR());
                    rext2.setAttribute('height', height2.toString(10));
                    rext2.setAttribute('y', y2.toString(10));
                    rect2_text.innerHTML = this.commas(this.data[1]/1000000000 || 0, 1);
                    rect2_text.parentElement.setAttribute('y', (y2 - 13).toString(10));
                    rect2_text.parentElement.setAttribute('x', (440 - rect2_text.clientWidth / 2).toString(10));
                }
            }
        }
    }

    public opacity(rectId: string, opc: number) {
        const rect = document.getElementById(rectId);
        rect.style.opacity = opc.toString();
    }

    public toogle (index: number){
        this.shows[index] = !this.shows[index];
    }

    public commas(number, digits) {
        return TableUtil.getNumberFormatForExcel(number, digits);
    }
}
