import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';

import { ApiService } from '@core';

@Injectable()
export class DocCategoryService {

	/**
	* @constructor
	* @param {ApiService} apiService
	*/
	constructor( private apiService: ApiService ) {}

	/**
	 * Get doc categories
	 * @param {string} queryFor
	 * @return {Observable}
	 */
	public getAll( ): Observable<any> {
		return new Observable( ( observer: Observer<any> ) => {
			this.apiService
				.call(
					'/api/finance/doc_category/list',
					'GET'
				)
				.subscribe(
					( result: any ) => observer.next( result ),
					( error: any ) => observer.error( error ),
					() => observer.complete()
				);
		} );
	}

	/**
	 * Get doc categories
	 * @param {string} queryFor
	 * @return {Observable}
	 */
	public getDocCategories( ): Observable<any> {
		return new Observable( ( observer: Observer<any> ) => {
			this.apiService
				.call(
					'/api/finance/doc_category/list_dropdown',
					'GET'
				)
				.subscribe(
					( result: any ) => observer.next( result ),
					( error: any ) => observer.error( error ),
					() => observer.complete()
				);
		} );
	}

	/**
	 * Get doc category by id
	 * @param {int} id: Doc category id
	 * @return {Observable}
	 */
	public getOne( id: number ): Observable<any> {
		return new Observable( ( observer: Observer<any> ) => {
			this.apiService
				.call(
					'/api/finance/doc_category/' + id,
					'GET'
				)
				.subscribe(
					( result: any ) => observer.next( result ),
					( error: any ) => observer.error( error ),
					() => observer.complete()
				);
		} );
	}

	/**
	 * Create doc category
	 * @param {any} data
	 * @return {Observable}
	 */
	public create( data: any ): Observable<any> {
		const params: any = {
			name				: data.name,
			description			: data.description
		};

		return new Observable( ( observer: Observer<any> ) => {
			this.apiService
				.call(
					'/api/finance/doc_category/create',
					'POST',
					params
				)
				.subscribe(
					( result: any ) => observer.next( result ),
					( error: any ) => observer.error( error ),
					() => observer.complete()
				);
		} );
	}

	/**
	 * Update doc category
	 * @param {number} id
	 * @param {any} data
	 * @return {Observable}
	 */
	public update( id: number, data: any ): Observable<any> {
		const params: any = {
			name		: data.name,
			description	: data.description,
		};

		return new Observable( ( observer: Observer<any> ) => {
			this.apiService
				.call(
					'/api/finance/doc_category/update/' + id,
					'PUT',
					params
				)
				.subscribe(
					( result: any ) => observer.next( result ),
					( error: any ) => observer.error( error ),
					() => observer.complete()
				);
		} );
	}

	/**
	 * Delete doc category
	 * @param {number} id
	 * @return {Observable}
	 */
	public delete( id: number ): Observable<any> {
		return new Observable( ( observer: Observer<any> ) => {
			this.apiService
				.call(
					'/api/finance/doc_category/delete/' + id,
					'DELETE'
				)
				.subscribe(
					( result: any ) => observer.next( result ),
					( error: any ) => observer.error( error ),
					() => observer.complete()
				);
		} );
	}
}
