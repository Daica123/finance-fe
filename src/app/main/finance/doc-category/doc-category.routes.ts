import { Routes } from '@angular/router';

import { DocCategoryComponent } from './components/doc-category.component';
import { AuthGrantService } from '@auth/services/auth-grant.service';
import { AuthRoleService } from '@auth/services/auth-role.service';

export const docCategoryRoutes: Routes = [
	{
		path		: 'doc-category',
		component	: DocCategoryComponent,
		canActivate	: [ AuthGrantService, AuthRoleService ],
		data: {
			title		: 'Document Categories',
			translate	: 'FINANCE.DOC_CATEGORY.LABELS.DOC_CATEGORIES',
			roles		: [
				'CEO', 'CFO', 'GENERAL_ACCOUNTANT'
			]
		},
	},
];
