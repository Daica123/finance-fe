import { NgModule } from '@angular/core';

import { CoreModule } from '@core/core.module';
import { LezoModule } from '@ext/lezo/lezo.module';

/* Component Import (Do not remove) */
import { DocCategoryComponent } from './components/doc-category.component';
import { DialogDocCategoryComponent } from './components/dialog-doc-category.component';
/* End Component Import (Do not remove) */

/* Service Import (Do not remove) */
import { DocCategoryService } from './services/doc-category.service';
/* End Service Import (Do not remove) */

@NgModule({
	imports: [ CoreModule, LezoModule ],
	declarations: [
		/* Component Inject (Do not remove) */
		DocCategoryComponent, DialogDocCategoryComponent
		/* End Component Inject (Do not remove) */
	],
	entryComponents: [
		/* Component Inject (Do not remove) */
		DialogDocCategoryComponent
		/* End Component Inject (Do not remove) */
	],
	providers: [
		/* Service Inject (Do not remove) */
		DocCategoryService,
		/* End Service Inject (Do not remove) */
	],
})
export class DocCategoryModule {

	/**
	* @constructor
	*/
	constructor() {}

}
