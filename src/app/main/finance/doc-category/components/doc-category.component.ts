import {Component, Injector, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material';
import {TranslateService} from '@ngx-translate/core';
import {FinanceBaseComponent} from '@finance/finance-base.component';
import {DialogConfirmComponent, SnackBarService} from '@core';
import {Direction} from '@finance/finance-direction.component';
import {DocCategoryService} from '../services/doc-category.service';
import {MatSort} from "@angular/material/sort";
import {ExcelService} from "@ext/lezo/services/excel.service";
import {DialogDocCategoryComponent} from "@finance/doc-category/components/dialog-doc-category.component";

@Component({
	selector	: 'doc-category',
	templateUrl	: '../templates/doc-category.pug',
	styleUrls	: [ '../styles/doc-category.scss' ],
})
@Direction({
	path	: 'doc-category',
	data	: { title: 'FINANCE.DIRECTION.DOC_CATEGORY', icon: 'icon icon-doc-category' },
	priority: 2,
	roles: [
		'CEO', 'CFO', 'GENERAL_ACCOUNTANT'
	]
})
export class DocCategoryComponent extends FinanceBaseComponent implements OnInit, OnDestroy {
	@ViewChild(MatSort) public sort: MatSort;

	public loaded: boolean;
	public isSubmitting: boolean;
	public displayedColumns: Array<string> = [
		'name', 'description', 'actions',
	];

	/**
	 * @constructor
	 * @param {Injector} injector
	 * @param {MatDialog} dialog
	 * @param {DocCategoryService} docCategoryService
	 * @param {SnackBarService} snackBarService
	 * @param {TranslateService} translateService
	 */
	constructor(
		public injector				: Injector,
		public dialog				: MatDialog,
		public docCategoryService	: DocCategoryService,
		public excelService			: ExcelService,
		public snackBarService		: SnackBarService,
		public translateService		: TranslateService
	) {
		super( injector );
	}

	/**
	 * @constructor
	 */
	public ngOnInit() {
		this.initData();
	}

	/**
	 * @constructor
	 */
	public ngOnDestroy() {
		super.ngOnDestroy();
	}

	/**
	 * Init all data
	 * @return {void}
	 */
	public initData() {
		this.getList();
	}

	/**
	 * Get document categories list
	 * @return {void}
	 */
	public getList() {
		this.setProcessing( true );
		this.loaded = false;

		this.docCategoryService
			.getAll()
			.subscribe( ( result: any ) => {
				this.setProcessing( false );
				this.loaded = true;
				this.dataSourceClone = result;
				this.dataSource.sort = this.sort;
				this.dataSource.data = result;
				this.applyFilter();
			} );
	}

	/**
	 * Open dialog document category
	 * @param {any} document category - Document category to bind to dialog
	 * @return {void}
	 */
	public openDialog( documentCategory?: any ) {
		const dialogRef: any = this.dialog.open(
			DialogDocCategoryComponent,
			{
				width	: '550px',
				data	: documentCategory || null,
			}
		);

		dialogRef
			.afterClosed()
			.subscribe( ( result: any ) => {
				if ( !result ) return;

				this.getList();
			} );
	}

	/**
	 * Delete document category
	 * @param {any} document category - Document category to delete
	 * @return {void}
	 */
	public delete( documentCategory: any ) {
		const dialogRef: any = this.dialog.open(
			DialogConfirmComponent,
			{
				width: '400px',
				data: {
					title	: this.translateService.instant( 'FINANCE.DOC_CATEGORY.TITLES.DELETE_DOC_CATEGORY' ),
					content	: this.translateService.instant( 'FINANCE.DOC_CATEGORY.MESSAGES.DELETE_DOC_CATEGORY_CONFIRMATION', documentCategory ),
					actions: {
						yes: { color: 'warn' },
					},
				},
			}
		);

		dialogRef
			.afterClosed()
			.subscribe( ( _result: any ) => {
				if ( !_result ) return;

				this.isSubmitting = true;
				this.setProcessing( true );

				this.docCategoryService
					.delete( documentCategory.id )
					.subscribe( ( result: any ) => {
						this.isSubmitting = false;
						this.setProcessing( false );

						if ( !result || !result.status ) {
							this.snackBarService.warn( 'FINANCE.DOC_CATEGORY.MESSAGES.DELETE_DOC_CATEGORY_FAIL', documentCategory );
							return;
						}

						this.snackBarService.success( 'FINANCE.DOC_CATEGORY.MESSAGES.DELETE_DOC_CATEGORY_SUCCESS', documentCategory );

						this.getList();
					} );
			} );
	}

	public exportExcel() {
		const headerSetting = {
			header: [ 'Name', 'Description' ],
			noData: this.translateService.instant('FINANCE.DOC_CATEGORY.MESSAGES.NO_RECORD_DATA_NAME', { name: 'document category' }),
			fgColor: 'ffffff',
			bgColor: '00245A',
		};
		const title = 'Document Categories List';
		const sheetName = 'DocumentCategories';
		const fileName = 'Document_Categories_List';
		const exportData: any[] = [];
		this.dataSource.data.forEach((item: any) => {
			const dataItem: any[] = [];
			dataItem.push(item.name || '');
			dataItem.push(item.description || '');
			exportData.push(dataItem);
		});
		this.excelService.exportArrayToExcel(
			exportData,
			title,
			headerSetting,
			sheetName,
			fileName
		);
	}
}
