import {Component, Inject, Injector, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SnackBarService} from 'angular-core';
import {TranslateService} from '@ngx-translate/core';
import _ from 'underscore';
import {FinanceBaseComponent} from '@finance/finance-base.component';
import {DocCategoryService} from "@finance/doc-category/services/doc-category.service";

@Component({
	selector	: 'dialog-doc-category',
	templateUrl	: '../templates/dialog-doc-category.pug',
})
export class DialogDocCategoryComponent extends FinanceBaseComponent implements OnInit, OnDestroy {

	public docForm: FormGroup;
	public isSubmitting: boolean;
	public doc: any = {
		name: null,
		description: null
	};

	constructor(
		@Inject( MAT_DIALOG_DATA ) public data: any,
		public dialogRef			: MatDialogRef<DialogDocCategoryComponent>,
		public injector				: Injector,
		public fb					: FormBuilder,
		public docCategoryService	: DocCategoryService,
		public snackBarService		: SnackBarService,
		public translateService		: TranslateService,
		public dialog				: MatDialog
	) {
		super( injector );

		this.docForm = fb.group({
			name: [
				{ value: null, disabled: false },
				Validators.compose([
					Validators.required,
					Validators.minLength( 1 ),
					Validators.maxLength( 255 ),
				]),
			],
			description: [
				{ value: null, disabled: false },
				Validators.compose([
					Validators.minLength( 0 ),
					Validators.maxLength( 255 ),
				]),
			]
		});
	}

	/**
	* @constructor
	*/
	public ngOnInit() {
		this.data && _.assign( this.doc, this.data );
	}

	/**
	* @constructor
	*/
	public ngOnDestroy() {
		super.ngOnDestroy();
	}

	/**
	* Click No button event
	* @return {void}
	*/
	public onNoClick() {
		this.dialogRef.close();
	}

	/**
	* Create client
	* @return {void}
	*/
	public create() {
		this.isSubmitting = true;

		this.docCategoryService
		.create( this.doc )
		.subscribe( ( result: any ) => {
			this.isSubmitting = false;

			if ( !result || !result.status ) {
				if ( result && result.message === 'DOC_CATEGORY_ALREADY_EXISTS' ) {
					this.snackBarService.warning( 'FINANCE.DOC_CATEGORY.MESSAGES.DOC_CATEGORY_ALREADY_EXISTS', this.doc );
					return;
				}

				this.snackBarService.warn( 'FINANCE.DOC_CATEGORY.MESSAGES.CREATE_DOC_CATEGORY_FAIL', this.doc );
				return;
			}

			this.snackBarService.success( 'FINANCE.DOC_CATEGORY.MESSAGES.CREATE_DOC_CATEGORY_SUCCESS', this.doc );

			this.dialogRef.close( true );
		} );
	}

	/**
	* Update client
	* @return {void}
	*/
	public update() {
		this.isSubmitting = true;

		this.docCategoryService
		.update( this.doc.id, this.doc )
		.subscribe( ( result: any ) => {
			this.isSubmitting = false;

			if ( !result || !result.status ) {
				if ( result && result.message === 'DOC_CATEGORY_ALREADY_EXISTS' ) {
					this.snackBarService.warning( 'FINANCE.DOC_CATEGORY.MESSAGES.DOC_CATEGORY_ALREADY_EXISTS', this.doc );
					return;
				}

				this.snackBarService.warn( 'FINANCE.DOC_CATEGORY.MESSAGES.UPDATE_DOC_CATEGORY_FAIL', this.doc );
				return;
			}

			this.snackBarService.success( 'FINANCE.DOC_CATEGORY.MESSAGES.UPDATE_DOC_CATEGORY_SUCCESS', this.doc );

			this.dialogRef.close( true );
		} );
	}

}
