import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import moment from 'moment-timezone';
import _ from 'underscore';
import 'chartjs-plugin-annotation';
import * as $ from 'jquery';

import {FinanceBaseComponent} from '@finance/finance-base.component';
import {Direction} from '@finance/finance-direction.component';
import {ProjectService} from '@finance/project/services/project.service';
import {COLORS} from '@resources';
import {NumberService, UtilitiesService} from '@core';
import {VOService} from '@finance/project/services/vo.service';
import {ProjectCostItemService} from '@finance/project/services/project-cost-item.service';
import {AppState} from '@app/app-state.service';
import {SettingService} from "@finance/settings/services/setting.service";
import {TableUtil} from '@app/utils/tableUtils';
import {ExcelService} from "@ext/lezo/services/excel.service";

@Component({
	selector	: 'dashboard',
	templateUrl	: '../templates/dashboard.pug',
	styleUrls	: [ '../styles/dashboard.scss' ],
})
@Direction({
	path	: 'dashboard',
	data	: { title: 'FINANCE.DIRECTION.DASHBOARD', icon: 'icon icon-dashboard' },
	priority: 100,
	roles: [
		'CEO', 'CFO', 'GENERAL_ACCOUNTANT',
		'LIABILITIES_ACCOUNTANT', 'PROCUREMENT_MANAGER', 'CONSTRUCTION_MANAGER',
		'PM', 'SALE',
		'QS', 'PURCHASING',
	],
})
export class DashboardComponent extends FinanceBaseComponent implements OnInit, OnDestroy {
	public settings: any[] = [];
	public loaded: boolean;
	public currentDate: any = moment();
	public clients: Array<any> = [];
	public projects: Array<any> = [];
	public salers: Array<any> = [];
	public managers: Array<any> = [];
	public dataTable: Array<any> = [];

	public filteredProjects: Array<any> = [];
	public customDatasets: Array<any> = [];
	public chartLabels: Array<string> = [];
	public chartOptions: any = this.chartJSOptions();
	public chartColors: Array<any> = [];
	public chartDatasets: Array<any> = [
		{ data: [] },
		{ data: [] },
	];

	public pieChartLabels: Array<string> = [];
	public pieChartOptions: any = {};
	public pieChartDatasets: Array<any> = [
		{ data: [], backgroundColor: []}
	];

	public radarChartLabels: Array<string> = [];
	public radarChartOptions: any = {};
	public radarChartColors: Array<any> = [];
	public radarChartDatasets: Array<any> = [
		{ data: [], label: '' },
		{ data: [], label: '' }
	];

	public masterChartLabels: Array<string> = [];
	public masterChartOptions: any = {
		responsive: true,
		scales: { xAxes: [{}], yAxes: [{}] },
		legend: {
			reverse: true,
			position: 'bottom',
			labels: {
				usePointStyle: true,
			},
		},
		title: {
			display: true,
			text: this.translateService.instant('FINANCE.PROJECT.LABELS.OVERVIEW')  + ' ' + this.translateService.instant("CHART.BILLIONS"),
			fontSize: 30
		},
		tooltips: {
			mode: 'label',
			label: 'mylabel',
			callbacks: {
				label: function(tooltipItem, _data) {
					return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); }, },
		},
	};

	public setting: any = {
		value: 25
	};

	public project_static: any = {
		receivable: 0,
		receivable_plan: 0,
		payable: 0,
		payable_plan: 0
	};

	public masterChartColors: Array<any> = [];
	public masterChartDatasets: Array<any> = [
		{ data: [], label: '' },
		{ data: [], label: '' }
	];
	public filters: any = {
		date: {
			begin	: this.currentDate.clone().startOf( 'year' ),
			end		: this.currentDate.clone().endOf( 'Y' ),
		},
		type		: 'month',
		project_ids	: [],
		client_ids	: [],
		saler_ids	: [],
		manager_ids	: []
	};
	public REAL_AND_PLAN: string = 'real_and_plan';
	public REAL_ONLY: string = 'real_only';
	public chartViewType: string = this.REAL_AND_PLAN; // or real_only
	public rrpCustomDatasets: Array<any> = [];
	public rrpChartLabels: Array<string> = [];
	public rrpChartOptions: any = this.chartJSOptions();
	public rrpChartColors: Array<any> = [];
	public rrpChartDatasets: Array<any> = [
		{ data: [] },
		{ data: [] },
		{ data: [] },
	];

	/**
	* @constructor
	* @param {Injector} injector
	* @param {ProjectService} projectService
	* @param {TranslateService} translateService
	*/
	constructor(
		public injector				  : Injector,
		public projectService		  : ProjectService,
		public voService			  : VOService,
		public projectCostItemService : ProjectCostItemService,
		public translateService		  : TranslateService,
		public settingService		  : SettingService,
		public excelService			  : ExcelService,
		public appState: AppState
	) {
		super( injector );
		this.settingService.getSettingByKey('MARGIN').subscribe((setting: any) => {
			this.setting = setting[0] || {value: '25'};
			this.setting.value = parseInt(this.setting.value);
		});
		moment.locale(this.appState.locale.code);
	}

	/**
	* @constructor
	*/
	public ngOnInit() {
		this.initData();
	}

	/**
	* @constructor
	*/
	public ngOnDestroy() {
		super.ngOnDestroy();
	}

	/**
	* Init all data
	* @return {void}
	*/
	public initData() {
		this.settingService.getAll([]).subscribe((settings: any) => {
			this.settings = settings;
			this.projectService
				.getAll( 'dashboard_reference' )
				.subscribe( ( result: any ) => {
					const projects: any = result;
					const clients: any = [];
					const salers: any = [];
					const managers: any = [];

					_.each( result, ( item: any ) => {
						if ( item.client && !_.findWhere( clients, { id: item.client.id } ) )
							clients.push( item.client );
						if ( item.saler && !_.findWhere( salers, { id: item.saler.id } ) )
							salers.push( item.saler );
						if ( item.user && !_.findWhere( managers, { id: item.user.id } ) )
							managers.push( item.user );
					} );

					this.clients = clients;
					this.salers = salers;
					this.managers = managers;
					this.projects = projects;
					this.filters.client_ids = _.map( clients, 'id' );
					this.filters.saler_ids = _.map( salers, 'id' );
					this.filters.manager_ids = _.map( managers, 'id' );
					this.updateCharts();
				} );
		});
	}

	public chartViewChange( event: any ) {
		if ( !event || !event.source ) return;

		this.updateCharts();
	}

	public updateCharts() {
		const _this = this;
		_this.pieChartLabels = [];
		_this.pieChartDatasets[0].data = [];
		_this.pieChartDatasets[0].backgroundColor = [];

		_this.radarChartLabels = [];
		_this.radarChartDatasets[ 0 ].data = [];
		_this.radarChartDatasets[ 1 ].data = [];

		_this.masterChartLabels = [];
		_this.masterChartDatasets[ 0 ].data = [];
		_this.masterChartDatasets[ 1 ].data = [];

		_this.chartLabels = [];
		_this.chartDatasets[ 0 ].data = [];
		_this.chartDatasets[ 1 ].data = [];
		_this.customDatasets[ 1 ] = { data: [] }; // Plan
		_this.customDatasets[ 0 ] = { data: [] }; // Real;

		_this.rrpChartLabels = [];
		_this.rrpChartDatasets[ 0 ].data = []; // Receivable
		_this.rrpChartDatasets[ 1 ].data = []; // Payable
		_this.rrpChartDatasets[ 2 ].data = []; // Receivable Plan
		_this.rrpCustomDatasets[ 0 ] = {
			data		: [],
			income		: 0,
			cost		: 0,
			income_plan	: 0,
		}; // Receivable
		_this.rrpCustomDatasets[ 1 ] = {
			data		: [],
			income		: 0,
			cost		: 0,
			income_plan	: 0,
		}; // Payable
		_this.rrpCustomDatasets[ 2 ] = {
			data		: [],
			income		: 0,
			cost		: 0,
			income_plan	: 0,
		}; // Receivable Plan
		if ( !this.filters.project_ids || !this.filters.project_ids.length ) return;
		this.loaded = false;
		this.setProcessing( true );
		Promise.all([
			_this.projectService.getDashboardProjectsAnalysis(
				_this.filters.date.begin, _this.filters.date.end, _this.filters.project_ids
			),
			_this.projectService
				.getDashboardRadar(
					_this.filters.date.begin, _this.filters.date.end, _this.filters.project_ids
				),
			_this.projectService
				.getDashboardStatistic(
					_this.filters.date.begin, _this.filters.date.end, _this.filters.type,
					_this.filters.project_ids, 'dashboard_reference'
				),
			_this.projectService
				.getDashboardStatisticRRP(
					_this.filters.date.begin, _this.filters.date.end, _this.filters.type,
					_this.filters.project_ids, 'dashboard_reference'
				),
			_this.projectService.getDashboardProjectsStatus(
				_this.filters.date.begin, _this.filters.date.end, _this.filters.project_ids
			)
		]).then(resultData => {
			resultData[0].subscribe(res1 => {
				_this.dataTable = res1 || [];
				_this.project_static = {
					receivable: 0,
					receivable_plan: 0,
					payable: 0,
					payable_plan: 0
				};

				res1.forEach(data => {
					_this.project_static.payable += data.payable;
					_this.project_static.payable_plan += data.payable_plan;
					_this.project_static.receivable += data.receivable;
					_this.project_static.receivable_plan += data.receivable_plan;
				});
				resultData[1].subscribe(res2 => {
					_this.calculateRadarChart( res2 );
					resultData[2].subscribe(res3 => {
						_this.chartViewType === _this.REAL_AND_PLAN
							? _this.calcualateRealAndPlan( res3 )
							: _this.calcualateRealOnly( res3 );
						_this.calculateMasterChart( res3 );
						resultData[3].subscribe(res4 => {
							_this.calcualateRealAndReceivablePlan( res4 );
							resultData[4].subscribe(res5 => {
								_this.loaded = true;
								_this.setProcessing( false );
								_this.calculatePieChart(res5);
							});
						});
					});
				});
			});
		});
	}

	public RECEIVABLE_PAYABLE_CHART_RECEIVABLE_PLAN_COLOR() {
		return this.settings.find((st: any) => st.key === 'RECEIVABLE_PAYABLE_CHART_RECEIVABLE_PLAN_COLOR').value;
	}

	public RECEIVABLE_PAYABLE_CHART_RECEIVABLE_COLOR() {
		return this.settings.find((st: any) => st.key === 'RECEIVABLE_PAYABLE_CHART_RECEIVABLE_COLOR').value;
	}

	public RECEIVABLE_PAYABLE_CHART_PAYABLE_COLOR() {
		return this.settings.find((st: any) => st.key === 'RECEIVABLE_PAYABLE_CHART_PAYABLE_COLOR').value;
	}

	public CASH_FLOW_CHART_PLAN_COLOR() {
		return this.settings.find((st: any) => st.key === 'CASH_FLOW_CHART_PLAN_COLOR').value;
	}

	public CASH_FLOW_CHART_REAL_COLOR() {
		return this.settings.find((st: any) => st.key === 'CASH_FLOW_CHART_REAL_COLOR').value;
	}

	public OVERVIEW_CHART_RECEIVABLE_COLOR() {
		const settingObject: any = this.settings.find((st: any) => st.key === 'OVERVIEW_CHART_RECEIVABLE_COLOR');
		return settingObject ? settingObject.value : '#ffffff';
	}

	public OVERVIEW_CHART_PAYABLE_COLOR() {
		const settingObject: any = this.settings.find((st: any) => st.key === 'OVERVIEW_CHART_PAYABLE_COLOR');
		return settingObject ? settingObject.value : '#ffffff';
	}

	public PROJECT_STATUS_DONE() {
		const settingObject: any = this.settings.find((st: any) => st.key === 'PROJECT_STATUS_DONE');
		return settingObject ? settingObject.value : '#ffffff';
	}

	public PROJECT_STATUS_PAYMENT_RECEIVED() {
		const settingObject: any = this.settings.find((st: any) => st.key === 'PROJECT_STATUS_PAYMENT_RECEIVED');
		return settingObject ? settingObject.value : '#ffffff';
	}

	public PROJECT_STATUS_PITCHING() {
		const settingObject: any = this.settings.find((st: any) => st.key === 'PROJECT_STATUS_PITCHING');
		return settingObject ? settingObject.value : '#ffffff';
	}

	public PROJECT_STATUS_SETTLEMENT() {
		const settingObject: any = this.settings.find((st: any) => st.key === 'PROJECT_STATUS_SETTLEMENT');
		return settingObject ? settingObject.value : '#ffffff';
	}

	/**
	* Handle client change event
	* @return {void}
	*/
	public onClientChange() {
		this.filteredProjects = this.filters.client_ids && this.filters.client_ids.length
			? _.filter( this.projects, ( project: any ) => _.contains( this.filters.client_ids, project.client_id ) )
			: [];
		this.filters.project_ids = _.map( this.filteredProjects, 'id' );
		this.updateCharts();
	}

	/**
	* Handle saler change event
	* @return {void}
	*/
	public onSalerChange() {
		this.filteredProjects = this.filters.saler_ids && this.filters.saler_ids.length
			? _.filter( this.projects, ( project: any ) => _.contains( this.filters.saler_ids, project.sale_by ) )
			: [];
		this.filters.project_ids = _.map( this.filteredProjects, 'id' );
		this.updateCharts();
	}

	/**
	* Handle manager change event
	* @return {void}
	*/
	public onManagerChange() {
		this.filteredProjects = this.filters.manager_ids && this.filters.manager_ids.length
			? _.filter( this.projects, ( project: any ) => _.contains( this.filters.manager_ids, project.manage_by ) )
			: [];
		this.filters.project_ids = _.map( this.filteredProjects, 'id' );
		this.updateCharts();
	}

	public calculatePieChart( result: any ) {
		if (!result) return;
		const labels: Array<string> = [
			this.translateService.instant('CHART.PITCHING'),
			this.translateService.instant('CHART.DONE'),
			this.translateService.instant('CHART.SETTLEMENT'),
			this.translateService.instant('CHART.PAYMENT_RECEIVED')
		];
		const customeDatasets: any[] = [
			{
				data: [
					this.translateService.instant('CHART.PITCHING'),
					this.translateService.instant('CHART.DONE'),
					this.translateService.instant('CHART.SETTLEMENT'),
					this.translateService.instant('CHART.PAYMENT_RECEIVED')
				]
			}
		];
		this.pieChartDatasets = [
			{
				data: [
					result.find(r => r.status === 0).count,
					result.find(r => r.status === 9).count,
					result.find(r => r.status === 10).count,
					result.find(r => r.status === 5).count
				],
				backgroundColor: [
					this.PROJECT_STATUS_PITCHING(),
					this.PROJECT_STATUS_DONE(),
					this.PROJECT_STATUS_SETTLEMENT(),
					this.PROJECT_STATUS_PAYMENT_RECEIVED()
				]
			}
		];
		this.pieChartLabels = labels;
		this.pieChartOptions = {
			responsive: true,
			legend: {
				reverse: true,
				position: 'bottom',
				labels: {
					usePointStyle: true,
				},
			},
			tooltips: {
				enabled: true,
				callbacks: {
					title: function(tooltipItem, _data) {
						return customeDatasets[tooltipItem[0].datasetIndex].data[tooltipItem[0].index];
					}
				}
			}
		};
	}

	public calculateRadarChart( result: any ) {
		if (!result) return;
		const _this = this;
		this.radarChartColors = [
			{
				borderColor			: this.OVERVIEW_CHART_RECEIVABLE_COLOR(),
				backgroundColor		: UtilitiesService.hexToRgba( this.OVERVIEW_CHART_RECEIVABLE_COLOR(), .4 ),
				pointBorderWidth	: 1,
				pointBackgroundColor: UtilitiesService.hexToRgba( this.OVERVIEW_CHART_RECEIVABLE_COLOR(), .9 ),
			},
			{
				borderColor			: this.OVERVIEW_CHART_PAYABLE_COLOR(),
				backgroundColor		: UtilitiesService.hexToRgba( this.OVERVIEW_CHART_PAYABLE_COLOR(), .4 ),
				pointBorderWidth	: 1,
				pointBackgroundColor: UtilitiesService.hexToRgba( this.OVERVIEW_CHART_PAYABLE_COLOR(), .9 ),
			},
		];
		this.radarChartDatasets = [
			{ label: _this.translateService.instant( 'GENERAL.LABELS.RECEIVABLE' ), data: [] },
			{ label: _this.translateService.instant( 'GENERAL.LABELS.PAYABLE' ), data: [] },
		];
		const sheets: any = {};
		const labels: Array<any> = [];
		_.each( result.receivables, ( item: any ) => {
			const key: string = 'P' + TableUtil.pad(item.project_id, 5);

			if ( !sheets[ key ] ) sheets[ key ] = { receivables: 0, payables: 0, title: '' };
			sheets[ key ].title = item.project_name;
			sheets[ key ].receivables += item.total / 1000000000;
			!_.include( labels.map(l => l.key), key ) && labels.push( {key, id: item.project_id} );
		} );
		_.each( result.payables, ( item: any ) => {
			const key: string = 'P' + TableUtil.pad(item.project_id, 5);

			if ( !sheets[ key ] ) sheets[ key ] = { receivables: 0, payables: 0, title: '' };
			sheets[ key ].title = item.project_name;
			sheets[ key ].payables += item.total / 1000000000;
			!_.include( labels.map(l => l.key), key ) && labels.push( {key, id: item.project_id} );
		} );

		if ( !labels || !labels.length ) return;
		labels.sort((l1: any, l2: any) => {
			if (l1.id === l2.id) return 0;
			if (l1.id < l2.id) return -1;
			return 1;
		});
		Object.keys(sheets).forEach(key => {
			if (!sheets[key].receivables) {
				sheets[key].receivables = 0;
			}
			if (!sheets[key].payables) {
				sheets[key].payables = 0;
			}
		});
		const customeDatasets: any[] = [
			{data: []},
			{data: []}
		];
		_.each( labels.map(l => l.key), ( label: any, _index: number ) => {
			let sheet: any = sheets[ label ];
			if (sheet && sheet.receivables && (sheet.receivables).toFixed(2)) {
				this.radarChartDatasets[ 0 ].data.push( (sheet.receivables).toFixed(2));
				customeDatasets[ 0 ].data.push(sheet.title);
			}
			if (sheet && sheet.payables && (sheet.payables).toFixed(2)) {
				this.radarChartDatasets[ 1 ].data.push( (sheet.payables).toFixed(2));
				customeDatasets[ 1 ].data.push(sheet.title);
			}
		} );

		this.radarChartLabels = labels.map(l => l.key);
		this.radarChartOptions = {
			responsive: true,
			legend: {
				reverse: true,
				position: 'bottom',
				labels: {
					usePointStyle: true,
				},
			},
			title: {
				display: true,
				text: _this.translateService.instant('FINANCE.PROJECT.LABELS.RECEIVABLE_PAYABLE_BY_PROJECT')  + ' ' + this.translateService.instant("CHART.BILLIONS"),
				fontSize: 30
			},
			tooltips: {
				enabled: true,
				callbacks: {
					title: function(tooltipItem, _data) {
						return customeDatasets[tooltipItem[0].datasetIndex].data[tooltipItem[0].index];
					}
				}
			}
		};
	}

	public calculateMasterChart( result: any ) {
		if (!result) return;
		const _this = this;
		this.masterChartColors = [
			{
				borderColor			: UtilitiesService.hexToRgba( this.OVERVIEW_CHART_RECEIVABLE_COLOR(), .9 ),
				backgroundColor		: this.OVERVIEW_CHART_RECEIVABLE_COLOR(),
				pointBorderWidth	: 3,
				pointBackgroundColor: UtilitiesService.hexToRgba( this.OVERVIEW_CHART_RECEIVABLE_COLOR(), .9 ),
			},
			{
				borderColor			: UtilitiesService.hexToRgba( this.OVERVIEW_CHART_PAYABLE_COLOR(), .9 ),
				backgroundColor		: this.OVERVIEW_CHART_PAYABLE_COLOR(),
				pointBorderWidth	: 3,
				pointBackgroundColor: UtilitiesService.hexToRgba( this.OVERVIEW_CHART_PAYABLE_COLOR(), .9 ),
			},
		];
		this.masterChartDatasets = [
			{ label: _this.translateService.instant( 'GENERAL.LABELS.RECEIVABLE' ), data: [] },
			{ label: _this.translateService.instant( 'GENERAL.LABELS.PAYABLE' ), data: [] },
		];
		const sheets: any = {};
		const labels: Array<any> = [];
		_.each( result.receivables, ( item: any ) => {
			const key: string = this.filters.type === 'month'
				? this.translateService.instant("YEAR_MONTH", {
					month: this.translateService.instant("MONTHS." + item.month.toString()),
					year: item.year,
				})
				: this.translateService.instant("YEAR_MONTH_WEEK", {
				week: this.translateService.instant("WEEKS", {week: item.week}),
				month: this.translateService.instant("MONTHS." + item.month.toString()),
				year: item.year,
			});

			if ( !sheets[ key ] ) sheets[ key ] = { receivables: 0, payables: 0 };

			sheets[ key ].receivables += item.total;
			!_.include( labels.map(l => l.key), key ) && labels.push( {key, month: item.month, year: item.year, week: item.week} );
		} );
		_.each( result.payables, ( item: any ) => {
			const key: string = this.filters.type === 'month'
				? this.translateService.instant("YEAR_MONTH", {
					month: this.translateService.instant("MONTHS." + item.month.toString()),
					year: item.year,
				})
				: this.translateService.instant("YEAR_MONTH_WEEK", {
					week: this.translateService.instant("WEEKS", {week: item.week}),
					month: this.translateService.instant("MONTHS." + item.month.toString()),
					year: item.year,
				});

			if ( !sheets[ key ] ) sheets[ key ] = { receivables: 0, payables: 0 };

			sheets[ key ].payables += item.total;
			!_.include( labels.map(l => l.key), key ) && labels.push( {key, month: item.month, year: item.year, week: item.week} );
		} );

		if ( !labels || !labels.length ) return;
		labels.sort((l1: any, l2: any) => {
			if (_this.filters.type === 'month') {
				if (l1.month === l2.month && l1.year === l2.year) return 0;
				if (l1.year < l2.year) return -1;
				if (l1.year > l2.year) return 1;
				if (l1.year === l2.year) {
					if (l1.month < l2.month) return -1;
					return 1;
				}
			} else {
				if (l1.week === l2.week && l1.month === l2.month && l1.year === l2.year) return 0;
				if (l1.year < l2.year) return -1;
				if (l1.year > l2.year) return 1;
				if (l1.year === l2.year) {
					if (l1.month < l2.month) return -1;
					if (l1.month > l2.month) return 1;
					if (l1.month === l2.month) {
						if (l1.week < l2.week) return -1;
						return 1;
					}
				}
			}
		});
		Object.keys(sheets).forEach(key => {
			if (!sheets[key].receivables) {
				sheets[key].receivables = 0;
			}
			if (!sheets[key].payables) {
				sheets[key].payables = 0;
			}
		});
		_.each( labels.map(l => l.key), ( label: any, index: number ) => {
			let sheet: any = sheets[ label ];
			if ( !sheet ) {
				sheet = sheets[ labels[ index - 1 ] ];
				// Receivable
				this.masterChartDatasets[ 0 ].data.push( (sheet && sheet.receivables) ? (sheet.receivables/1000000000).toFixed(2) : 0 );

				// Payable
				this.masterChartDatasets[ 1 ].data.push( (sheet && sheet.payables) ? (sheet.payables/1000000000).toFixed(2) : 0 );
				return;
			}
			this.masterChartDatasets[ 0 ].data.push( (sheet && sheet.receivables) ? (sheet.receivables/1000000000).toFixed(2) : 0 );
			this.masterChartDatasets[ 1 ].data.push( (sheet && sheet.payables) ? (sheet.payables/1000000000).toFixed(2) : 0 );
		} );

		this.masterChartLabels = labels.map(l => l.key);
	}

	/**
	* Calcualte real and plan
	* @param {any} result
	* @return {void}
	*/
	public calcualateRealAndPlan( result: any ) {
		if (!result) return;
		const _this = this;
		this.chartColors = [
			{
				borderColor			: this.CASH_FLOW_CHART_REAL_COLOR(),
				backgroundColor		: UtilitiesService.hexToRgba( this.CASH_FLOW_CHART_REAL_COLOR(), .3 ),
				pointBorderWidth	: 3,
				pointBackgroundColor: UtilitiesService.hexToRgba( this.CASH_FLOW_CHART_REAL_COLOR(), .9 ),
			},
			{
				borderColor			: this.CASH_FLOW_CHART_PLAN_COLOR(),
				backgroundColor		: UtilitiesService.hexToRgba( this.CASH_FLOW_CHART_PLAN_COLOR(), .9 ),
				pointBorderWidth	: 3,
				pointBackgroundColor: UtilitiesService.hexToRgba( this.CASH_FLOW_CHART_PLAN_COLOR(), .9 ),
			},
		];
		this.chartDatasets = [
			{ label: _this.translateService.instant( 'FINANCE.PROJECT.LABELS.REAL' ), data: [], pointRadius: [] },
			{ label: _this.translateService.instant( 'FINANCE.PROJECT.LABELS.PLAN' ), data: [], pointRadius: [] },
		];

		const sheets: any = {};
		const labels: Array<string> = [];
		const projectTotalLine: any = {};
		const projectTotalCost: any = {};
		let totalRevenue: number = 0;
		let totalRevenuePlan: number = 0;

		_.each( result.total_line, ( item: any ) => {
			item.total = item.total / 1000000000;
			projectTotalLine[item.project_id] = item.total;
		});

		_.each( result.total_cost, ( item: any ) => {
			item.total = item.total / 1000000000;
			projectTotalCost[item.project_id] = item.total;
		});

		_.each( result.income, ( item: any ) => {
			const key: string = this.chartJSLabelKey( item.year, item.month, item.week );

			if ( !sheets[ key ] ) sheets[ key ] = { income: 0, cost: 0, income_plan: 0, cost_plan: 0 };
			item.final_total = item.final_total / 1000000000;
			item.final_total_vat = item.final_total_vat / 1000000000;
			sheets[ key ].income = sheets[ key ].income / 1000000000;

			sheets[ key ].income += (item.final_total + item.final_total_vat);
			!_.include( labels, key ) && labels.push( key );
		} );

		_.each( result.cost, ( item: any ) => {
			const key: string = this.chartJSLabelKey( item.year, item.month, item.week );

			if ( !sheets[ key ] ) sheets[ key ] = { income: 0, cost: 0, income_plan: 0, cost_plan: 0 };

			item.final_total = item.final_total / 1000000000;
			item.final_total_vat = item.final_total_vat / 1000000000;
			sheets[ key ].cost = sheets[ key ].cost / 1000000000;

			sheets[ key ].cost += item.final_total + item.final_total_vat;
			!_.include( labels, key ) && labels.push( key );
		} );

		_.each( result.income_plan, ( item: any ) => {
			const key: string = this.chartJSLabelKey( item.year, item.month, item.week );

			if ( !sheets[ key ] ) sheets[ key ] = { income: 0, cost: 0, income_plan: 0, cost_plan: 0 };

			item.final_total = item.final_total / 1000000000;
			item.final_total_vat = item.final_total_vat / 1000000000;
			sheets[ key ].income_plan = sheets[ key ].income_plan / 1000000000;

			sheets[ key ].income_plan += ( item.target_percent || 0 ) * ( projectTotalLine[ item.project_id ] || 0 ) / 100;
			!_.include( labels, key ) && labels.push( key );
		} );

		_.each( result.cost_plan, ( item: any ) => {
			const key: string = this.chartJSLabelKey( item.year, item.month, item.week );

			if ( !sheets[ key ] ) sheets[ key ] = { income: 0, cost: 0, income_plan: 0, cost_plan: 0 };
			sheets[ key ].cost_plan = sheets[ key ].cost_plan / 1000000000;

			sheets[ key ].cost_plan += ( item.target_percent || 0 ) * ( projectTotalCost[ item.project_id ] || 0 ) / 100;
			!_.include( labels, key ) && labels.push( key );
		} );

		if ( !labels || !labels.length ) return;

		const currentDateLabelKey: string = this.chartJSLabelKey(
			this.currentDate.year(),
			this.currentDate.month() + 1,
			this.filters.type === 'month' ? null : this.currentDate.week()
		);


		!_.include( labels, currentDateLabelKey ) && labels.push( currentDateLabelKey );

		_.each( labels.sort(), ( label: any, index: number ) => {
			let sheet: any = sheets[ label ];

			if ( !sheet ) {
				sheet = sheets[ labels[ index - 1 ] ];

				if ( !sheet ) return;

				// Real
				this.chartDatasets[ 0 ].data.push( sheet.revenue || 0 );
				this.customDatasets[ 0 ].data.push( { income: sheet.income || 0, cost: sheet.cost || 0, date: label } );

				// Plan
				this.chartDatasets[ 1 ].data.push( sheet.revenue_plan || 0 );
				this.customDatasets[ 1 ].data.push( { income: sheet.income_plan || 0, cost: sheet.cost_plan || 0, date: label } );
				return;
			}

			// Real
			totalRevenue += ( sheet.income || 0 ) - ( sheet.cost || 0 );
			sheet.revenue = _.clone( totalRevenue );
			this.chartDatasets[ 0 ].data.push( sheet.revenue || 0 );
			this.customDatasets[ 0 ].data.push( { income: sheet.income || 0, cost: sheet.cost || 0, date: label } );

			// Plan
			totalRevenuePlan += ( sheet.income_plan || 0 ) - ( sheet.cost_plan || 0 );
			sheet.revenue_plan = _.clone( totalRevenuePlan );
			this.chartDatasets[ 1 ].data.push( sheet.revenue_plan || 0 );
			this.customDatasets[ 1 ].data.push( { income: sheet.income_plan || 0, cost: sheet.cost_plan || 0, date: label } );
		} );
		const currentDateLabelIndex: number = _.indexOf( labels, currentDateLabelKey );
		let xAdjust: number = 0;

		if ( currentDateLabelIndex === 0 ) xAdjust = 50;
		if ( currentDateLabelIndex === ( labels.length - 1 ) ) xAdjust = -50;
		this.chartLabels = this.hiddenLabels(labels, currentDateLabelKey);
		this.chartDatasets[ 0 ].pointRadius = this.processChartData(this.chartDatasets[ 0 ], this.customDatasets[ 0 ]);
		this.chartDatasets[ 1 ].pointRadius = this.processChartData(this.chartDatasets[ 1 ], this.customDatasets[ 1 ]);

		this.chartOptions = this.chartJSOptions({
			legend: {
				reverse: true,
				position: 'bottom',
				labels: {
					usePointStyle: true,
				},
			},
			scales: {
				yAxes: [{
					ticks: {
						callback: function(label, _index, _labels) {
							return label;
						}
					}
				}],
				xAxes: [{
					ticks: {
						callback: function(label, _index, _labels) {
							return label ? _this.convertLabelToDate(label) : '';
						}
					}
				}]
			},
			title: {
				display: true,
				text: _this.translateService.instant('FINANCE.PROJECT.LABELS.CASH_FLOW')  + ' ' + this.translateService.instant("CHART.BILLIONS"),
				fontSize: 30
			},
			datasets: this.chartDatasets,
			tooltipLabel: _this.translateService.instant('FINANCE.PROJECT.LABELS.TOTAL'),
			annotation: {
				annotations: [
					{
						value: currentDateLabelKey,
						drawTime: 'afterDatasetsDraw',
						type: 'line',
						mode: 'vertical',
						scaleID: 'x-axis-0',
						borderColor: 'red',
						borderWidth: 3,
						label: {
							xAdjust,
							enabled: true,
							position: 'center',
							content: _this.translateService.instant(
								this.filters.type === 'month' ? 'GENERAL.LABELS.CURRENT_MONTH' : 'GENERAL.LABELS.CURRENT_WEEK'
							),
						},
					},
				],
			},
			tooltips: {
				enabled: false,
				intersect: false,
				/* tslint:disable-next-line */
				custom(tooltipModel: any) {
					let tooltipEl: any = document.getElementById('chartjs-tooltip');

					// Create element on first render
					if (!tooltipEl) {
						tooltipEl = document.createElement('div');
						tooltipEl.id = 'chartjs-tooltip';
						document.body.appendChild(tooltipEl);
					}

					// Hide if no tooltip
					if (!tooltipModel.opacity) {
						tooltipEl.style.opacity = 0;
						$('.chartjs-tooltip').css({ opacity: 0 });
						return;
					}

					// Set caret position
					tooltipEl.classList.remove('above', 'below', 'no-transform');

					if (tooltipModel.yAlign) {
						tooltipEl.classList.add(tooltipModel.yAlign);
					} else {
						tooltipEl.classList.add('no-transform');
					}

					let revenue: number = 0;

					// Set Text
					let isShowTooltip = false;
					if (tooltipModel.body && _this.chartDatasets) {
						const index: number = tooltipModel.dataPoints[ 0 ].index;
						const datasetIndex: number = tooltipModel.dataPoints[ 0 ].datasetIndex;

						revenue = _this.chartDatasets[ datasetIndex ].data[ index ]
							? _this.chartDatasets[ datasetIndex ].data[ index ]
							: 0;

						tooltipEl.innerHTML = '<b>' + _this.translateService.instant('FINANCE.PROJECT.LABELS.INCOME') + ':</b> '
							+ (_this.customDatasets[ datasetIndex ].data[ index ].income
								? NumberService.addCommas(_this.customDatasets[ datasetIndex ].data[ index ].income)
								: 0)
							+ '<br/><b>' + _this.translateService.instant('FINANCE.PROJECT.LABELS.COST') + ':</b> '
							+ (_this.customDatasets[ datasetIndex ].data[ index ].cost
								? NumberService.addCommas(_this.customDatasets[ datasetIndex ].data[ index ].cost)
								: 0)
							+ '<br/><b>' + _this.translateService.instant('FINANCE.PROJECT.LABELS.REVENUE') + ':</b> '
							+ NumberService.addCommas(revenue)
							+ '<br/><b>' + _this.translateService.instant('FINANCE.PROJECT.LABELS.DATE') + ':</b> '
							+ _this.convertLabelToDate(_this.customDatasets[ datasetIndex ].data[ index ].date);


						if (_this.customDatasets[ datasetIndex ].data[ index ].income === 0 && _this.customDatasets[ datasetIndex ].data[ index ].cost === 0) {
							isShowTooltip = true;
						}

					}

					// `this` will be the overall tooltip
					const position: any = this._chart.canvas.getBoundingClientRect();


					// Display, position, and set styles for font
					tooltipEl.style.opacity = 1;
					tooltipEl.style.position = 'absolute';
					tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX
						+ (tooltipEl.offsetLeft > position.width / 2 ? -tooltipEl.offsetWidth : 0) + 'px';
					tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY - 60 + 'px';
					tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
					tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
					tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
					tooltipEl.style.pointerEvents = 'none';
					tooltipEl.style.zIndex = 1;
					tooltipEl.style.borderRadius = '4px';
					tooltipEl.style.padding = '8px 20px';
					tooltipEl.style.backgroundColor = revenue >= 0
						? UtilitiesService.hexToRgba(_this.CASH_FLOW_CHART_PLAN_COLOR(), .41)
						: UtilitiesService.hexToRgba(_this.CASH_FLOW_CHART_REAL_COLOR(), .41);

					if (isShowTooltip) tooltipEl.style.opacity = 0;

					// Real data fake point
					let realPointEl: any = document.getElementById('tooltip-point');

					if (!realPointEl) {
						realPointEl = document.createElement('div');
						realPointEl.id = 'tooltip-point';
						realPointEl.classList.add('chartjs-tooltip');
						document.body.appendChild(realPointEl);
					}
					if (!isShowTooltip) {
						realPointEl.innerHTML = '<div style="width: 10px; height: 10px; border: 1px none; position: absolute; background-color:'
							+ UtilitiesService.hexToRgba(_this.CASH_FLOW_CHART_PLAN_COLOR(), .9)
							+ '; left: ' + (tooltipModel.dataPoints[ 0 ].x + 120) + 'px'
							+ '; top: ' + (tooltipModel.dataPoints[ 0 ].y + position.top + window.pageYOffset - 5) + 'px'
							+ '; border-radius: 50%;"></div>';

						realPointEl.style.opacity = 1;
						realPointEl.style.zIndex = 1;
					}
				},
			},
		});
		//

	}

	/**
	* Calcualte real only
	* @param {any} result
	* @return {void}
	*/
	public calcualateRealOnly( result: any ) {
		if (!result) return;
		const _this = this;
		this.chartColors = [
			{
				borderColor: this.CASH_FLOW_CHART_REAL_COLOR(),
				backgroundColor: UtilitiesService.hexToRgba(this.CASH_FLOW_CHART_REAL_COLOR(), .3),
				pointBorderWidth: 3,
				pointBackgroundColor: UtilitiesService.hexToRgba(this.CASH_FLOW_CHART_REAL_COLOR(), .9),
			},
			{
				borderColor: this.CASH_FLOW_CHART_PLAN_COLOR(),
				backgroundColor: UtilitiesService.hexToRgba(this.CASH_FLOW_CHART_PLAN_COLOR(), .3),
				pointBorderWidth: 3,
				pointBackgroundColor: UtilitiesService.hexToRgba(this.CASH_FLOW_CHART_PLAN_COLOR(), .9),
			},
		];
		this.chartDatasets = [
			{ label: _this.translateService.instant( 'FINANCE.PROJECT.LABELS.PAYABLES' ), data: [] },
			{ label: _this.translateService.instant( 'FINANCE.PROJECT.LABELS.RECEIVABLES' ), data: [] },
		];

		const sheets: any = {};
		const labels: Array<string> = [];
		const projectTotalLine: any = {};
		const projectTotalCost: any = {};
		let totalPayable: number = 0;
		let totalBill: number = 0;

		_.each( result.total_line, ( item: any ) => {
			item.total = item.total / 1000000000;
			projectTotalLine[ item.project_id ] = item.total;
		} );

		_.each( result.total_cost, ( item: any ) => {
			item.total = item.total / 1000000000;
			projectTotalCost[ item.project_id ] = item.total;
		} );

		_.each( result.income, ( item: any ) => {
			const key: string = this.chartJSLabelKey( item.year, item.month, item.week );

			if ( !sheets[ key ] ) sheets[ key ] = { income: 0, cost: 0 };
			item.final_total = item.final_total / 1000000000;
			item.final_total_vat = item.final_total_vat / 1000000000;
			sheets[ key ].income = sheets[ key ].income / 1000000000;

			sheets[ key ].income += item.final_total + item.final_total_vat;
			!_.include( labels, key ) && labels.push( key );
		} );

		_.each( result.cost, ( item: any ) => {
			const key: string = this.chartJSLabelKey( item.year, item.month, item.week );

			if ( !sheets[ key ] ) sheets[ key ] = { income: 0, cost: 0 };
			item.final_total = item.final_total / 1000000000;
			item.final_total_vat = item.final_total_vat / 1000000000;
			sheets[ key ].cost = sheets[ key ].cost / 1000000000;

			sheets[ key ].cost += item.final_total + item.final_total_vat;
			!_.include( labels, key ) && labels.push( key );
		} );

		if ( !labels || !labels.length ) return;

		const currentDateLabelKey: string = this.chartJSLabelKey(
			this.currentDate.year(),
			this.currentDate.month() + 1,
			this.filters.type === 'week' ? this.currentDate.week() : null
		);

		!_.include( labels, currentDateLabelKey ) && labels.push( currentDateLabelKey );

		_.each( labels.sort(), ( label: any, index: number ) => {
			let sheet: any = sheets[ label ];

			if ( !sheet ) {
				sheet = sheets[ labels[ index - 1 ] ];

				if ( !sheet ) return;

				// Payable
				this.chartDatasets[ 0 ].data.push( totalPayable || 0 );
				this.customDatasets[ 0 ].data.push({
					income	: sheet.income || 0,
					cost	: sheet.cost || 0,
					payable	: _.clone( totalPayable ),
					date    : label,
				});

				// Receivable
				this.chartDatasets[ 1 ].data.push( totalBill || 0 );
				this.customDatasets[ 1 ].data.push({
					income		: sheet.income || 0,
					cost		: sheet.cost || 0,
					receivable	: _.clone( totalBill ),
					date   		: label,
				});
				return;
			}

			// Payable
			totalPayable += sheet.cost || 0;
			sheet.payable = _.clone( totalPayable );
			this.chartDatasets[ 0 ].data.push( sheet.payable || 0 );
			this.customDatasets[ 0 ].data.push({
				income	: sheet.income || 0,
				cost	: sheet.cost || 0,
				payable	: _.clone( totalPayable ),
				date    : label,
			});

			// Receivable
			totalBill += sheet.income || 0;
			sheet.receivable = _.clone( totalBill );
			this.chartDatasets[ 1 ].data.push( sheet.receivable || 0 );
			this.customDatasets[ 1 ].data.push({
				income		: sheet.income || 0,
				cost		: sheet.cost || 0,
				receivable	: _.clone( totalBill ),
				date   		: label,
			});
		} );
		const currentDateLabelIndex: number = _.indexOf( labels, currentDateLabelKey );
		let xAdjust: number = 0;

		if ( currentDateLabelIndex === 0 ) xAdjust = 50;
		if ( currentDateLabelIndex === ( labels.length - 1 ) ) xAdjust = -50;

		this.chartLabels = this.hiddenLabels(labels, currentDateLabelKey);
		this.chartDatasets[ 0 ].pointRadius = this.processChartData(this.chartDatasets[ 0 ], this.customDatasets[ 0 ]);
		this.chartDatasets[ 1 ].pointRadius = this.processChartData(this.chartDatasets[ 1 ], this.customDatasets[ 1 ]);

		this.chartOptions = this.chartJSOptions({
			datasets: this.chartDatasets,
			scales: {
				yAxes: [{
					ticks: {
						callback: function(label, _index, _labels) {
							return label;
						}
					}
				}],
				xAxes: [{
					ticks: {
						callback: function(label, _index, _labels) {
							return label ? _this.convertLabelToDate(label) : '';
						}
					}
				}]
			},
			tooltipLabel: _this.translateService.instant('FINANCE.PROJECT.LABELS.TOTAL'),
			annotation: {
				annotations: [
					{
						value: currentDateLabelKey,
						drawTime: 'afterDatasetsDraw',
						type: 'line',
						mode: 'vertical',
						scaleID: 'x-axis-0',
						borderColor: 'red',
						borderWidth: 3,
						label: {
							xAdjust,
							enabled: true,
							position: 'center',
							content: _this.translateService.instant(
								this.filters.type === 'week' ? 'GENERAL.LABELS.CURRENT_WEEK' : 'GENERAL.LABELS.CURRENT_MONTH'
							),
						},
					},
				],
			},
			title: {
				display: true,
				text: _this.translateService.instant('FINANCE.PROJECT.LABELS.CASH_FLOW')  + ' ' + this.translateService.instant("CHART.BILLIONS"),
				fontSize: 30
			},
			tooltips: {
				enabled: false,
				intersect: false,
				/* tslint:disable-next-line */
				custom(tooltipModel: any) {
					// Tooltip Element
					let tooltipEl: any = document.getElementById('chartjs-tooltip');

					// Create element on first render
					if (!tooltipEl) {
						tooltipEl = document.createElement('div');
						tooltipEl.id = 'chartjs-tooltip';
						document.body.appendChild(tooltipEl);
					}

					// Hide if no tooltip
					if (!tooltipModel.opacity) {
						tooltipEl.style.opacity = 0;
						$('.chartjs-tooltip').css({ opacity: 0 });
						return;
					}

					// Set caret position
					tooltipEl.classList.remove('above', 'below', 'no-transform');

					if (tooltipModel.yAlign) {
						tooltipEl.classList.add(tooltipModel.yAlign);
					} else {
						tooltipEl.classList.add('no-transform');
					}

					// Set Text
					let isShowTooltip = false;
					if (tooltipModel.body && _this.chartDatasets) {
						const index: number = tooltipModel.dataPoints[ 0 ].index;
						const datasetIndex: number = tooltipModel.dataPoints[ 0 ].datasetIndex;


						if (datasetIndex === 1 && _this.customDatasets[ datasetIndex ].data[ index ].income === 0) {
							isShowTooltip = true;
						}

						if (datasetIndex === 0 && _this.customDatasets[ datasetIndex ].data[ index ].cost === 0) {
							isShowTooltip = true;
						}

						tooltipEl.innerHTML = datasetIndex
							? ('<b>' + _this.translateService.instant('FINANCE.PROJECT.LABELS.INCOME') + ':</b> '
								+ (_this.customDatasets[ datasetIndex ].data[ index ].income
									? NumberService.addCommas(_this.customDatasets[ datasetIndex ].data[ index ].income)
									: 0)
								+ '<br/><b>' + _this.translateService.instant('FINANCE.PROJECT.LABELS.TOTAL') + ':</b> '
								+ (_this.customDatasets[ datasetIndex ].data[ index ].receivable
									? NumberService.addCommas(_this.customDatasets[ datasetIndex ].data[ index ].receivable)
									: 0))
							: ('<b>' + _this.translateService.instant('FINANCE.PROJECT.LABELS.COST') + ':</b> '
							+ (_this.customDatasets[ datasetIndex ].data[ index ].cost
								? NumberService.addCommas(_this.customDatasets[ datasetIndex ].data[ index ].cost)
								: 0)
							+ '<br/><b>' + _this.translateService.instant('FINANCE.PROJECT.LABELS.TOTAL') + ':</b> '
							+ (_this.customDatasets[ datasetIndex ].data[ index ].payable
								? NumberService.addCommas(_this.customDatasets[ datasetIndex ].data[ index ].payable)
								: 0))
							+ '<br/><b>' + _this.translateService.instant('FINANCE.PROJECT.LABELS.DATE') + ':</b> '
							+ _this.convertLabelToDate(_this.customDatasets[ datasetIndex ].data[ index ].date);

					}

					// `this` will be the overall tooltip
					const position: any = this._chart.canvas.getBoundingClientRect();

					// Display, position, and set styles for font
					tooltipEl.style.opacity = 1;
					tooltipEl.style.position = 'absolute';
					tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX
						+ (tooltipEl.offsetLeft > position.width / 2 ? -tooltipEl.offsetWidth : 0) + 'px';
					tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY - 60 + 'px';
					tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
					tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
					tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
					tooltipEl.style.pointerEvents = 'none';
					tooltipEl.style.zIndex = 1;
					tooltipEl.style.borderRadius = '4px';
					tooltipEl.style.padding = '8px 20px';

					if (isShowTooltip) tooltipEl.style.opacity = 0;

					tooltipEl.style.backgroundColor = UtilitiesService.hexToRgba(_this.RECEIVABLE_PAYABLE_CHART_RECEIVABLE_PLAN_COLOR(), .41);

					// Real data fake point
					let realPointEl: any = document.getElementById('tooltip-point');

					if (!realPointEl) {
						realPointEl = document.createElement('div');
						realPointEl.id = 'tooltip-point';
						realPointEl.classList.add('chartjs-tooltip');
						document.body.appendChild(realPointEl);
					}

					if (!isShowTooltip) {
						realPointEl.innerHTML = '<div style="width: 10px; height: 10px; border: 1px none; position: absolute; background-color:'
							+ UtilitiesService.hexToRgba(_this.RECEIVABLE_PAYABLE_CHART_RECEIVABLE_PLAN_COLOR(), .9)
							+ '; left: ' + (tooltipModel.dataPoints[ 0 ].x + 120) + 'px'
							+ '; top: ' + (tooltipModel.dataPoints[ 0 ].y + position.top + window.pageYOffset - 5) + 'px'
							+ '; border-radius: 50%;"></div>';

						realPointEl.style.opacity = 1;
						realPointEl.style.zIndex = 1;
					}
				},
			},
		});
	}

	/**
	* Calcualte real and receivable plan
	* @param {any} result
	* @return {void}
	*/
	public calcualateRealAndReceivablePlan( result: any ) {
		if (!result) return;
		const _this = this;
		this.rrpChartColors = [
			{
				borderColor: this.RECEIVABLE_PAYABLE_CHART_RECEIVABLE_PLAN_COLOR(),
				backgroundColor: UtilitiesService.hexToRgba(this.RECEIVABLE_PAYABLE_CHART_RECEIVABLE_PLAN_COLOR(), .3),
				pointBorderWidth: 3,
				pointBackgroundColor: UtilitiesService.hexToRgba(this.RECEIVABLE_PAYABLE_CHART_RECEIVABLE_PLAN_COLOR(), .9),
			},
			{
				borderColor: this.RECEIVABLE_PAYABLE_CHART_RECEIVABLE_COLOR(),
				backgroundColor: UtilitiesService.hexToRgba(this.RECEIVABLE_PAYABLE_CHART_RECEIVABLE_COLOR(), .3),
				pointBorderWidth: 3,
				pointBackgroundColor: UtilitiesService.hexToRgba(this.RECEIVABLE_PAYABLE_CHART_RECEIVABLE_COLOR(), .9),
			},
			{
				borderColor: this.RECEIVABLE_PAYABLE_CHART_PAYABLE_COLOR(),
				backgroundColor: UtilitiesService.hexToRgba(this.RECEIVABLE_PAYABLE_CHART_PAYABLE_COLOR(), .3),
				pointBorderWidth: 3,
				pointBackgroundColor: UtilitiesService.hexToRgba(this.RECEIVABLE_PAYABLE_CHART_PAYABLE_COLOR(), .9),
			},
		];
		this.rrpChartDatasets = [
			{ label: _this.translateService.instant('FINANCE.PROJECT.LABELS.RECEIVABLES_PLAN'), data: [], pointRadius: [] },
			{ label: _this.translateService.instant('FINANCE.PROJECT.LABELS.RECEIVABLES'), data: [], pointRadius: [] },
			{ label: _this.translateService.instant('FINANCE.PROJECT.LABELS.PAYABLES'), data: [], pointRadius: [] },
		];

		const sheets: any = {};
		const labels: Array<string> = [];
		const projectTotalLine: any = {};
		let totalIncome: number = 0;
		let totalCost: number = 0;
		let totalIncomePlan: number = 0;

		_.each( result.total_line, ( item: any ) => {
			item.total = item.total / 1000000000;
			projectTotalLine[ item.project_id ] = item.total;
		} );

		_.each( result.income_plan, ( item: any ) => {
			const key: string = this.chartJSLabelKey( item.year, item.month, item.week );

			if ( !sheets[ key ] ) sheets[ key ] = { income: 0, cost: 0, income_plan: 0 };
			sheets[ key ].income_plan = sheets[ key ].income_plan / 1000000000;

			sheets[ key ].income_plan += ( item.target_percent || 0 ) * ( projectTotalLine[ item.project_id ] || 0 ) / 100;
			!_.include( labels, key ) && labels.push( key );
		} );

		_.each( result.income, ( item: any ) => {
			const key: string = this.chartJSLabelKey( item.year, item.month, item.week );

			if ( !sheets[ key ] ) sheets[ key ] = { income: 0, cost: 0, income_plan: 0 };
			item.final_total = item.final_total / 1000000000;
			item.final_total_vat = item.final_total_vat / 1000000000;
			sheets[ key ].income = sheets[ key ].income / 1000000000;

			sheets[ key ].income += item.final_total + item.final_total_vat;
			!_.include( labels, key ) && labels.push( key );
		} );

		_.each( result.cost, ( item: any ) => {
			const key: string = this.chartJSLabelKey( item.year, item.month, item.week );

			if ( !sheets[ key ] ) sheets[ key ] = { income: 0, cost: 0, income_plan: 0 };
			item.final_total = item.final_total / 1000000000;
			item.final_total_vat = item.final_total_vat / 1000000000;
			sheets[ key ].cost = sheets[ key ].cost / 1000000000;

			sheets[ key ].cost += item.final_total + item.final_total_vat;
			!_.include( labels, key ) && labels.push( key );
		} );

		if ( !labels || !labels.length ) return;

		const currentDateLabelKey: string = this.chartJSLabelKey(
			this.currentDate.year(),
			this.currentDate.month() + 1,
			this.filters.type === 'week' ? this.currentDate.week() : null
		);

		!_.include( labels, currentDateLabelKey ) && labels.push( currentDateLabelKey );

		_.each( labels.sort(), ( label: any, index: number ) => {
			let sheet: any = sheets[ label ];

			if ( !sheet ) {
				sheet = sheets[ labels[ index - 1 ] ];

				if ( !sheet ) return;

				// Receivable Plan
				this.rrpChartDatasets[ 2 ].data.push( sheet.income_plan || 0 );
				this.rrpCustomDatasets[ 2 ].data.push( { income: sheet.income || 0, cost: sheet.cost || 0, income_plan: sheet.income_plan || 0, date: label } );

				// Receivable
				this.rrpChartDatasets[ 0 ].data.push( sheet.income || 0 );
				this.rrpCustomDatasets[ 0 ].data.push( { income: sheet.income || 0, cost: sheet.cost || 0, income_plan: sheet.income_plan || 0, date: label } );

				// Payable
				this.rrpChartDatasets[ 1 ].data.push( sheet.cost || 0 );
				this.rrpCustomDatasets[ 1 ].data.push( { income: sheet.income || 0, cost: sheet.cost || 0, income_plan: sheet.income_plan || 0, date: label } );
				return;
			}

			// Receivable Plan
			totalIncomePlan += sheet.income_plan || 0;
			sheet.income_plan = _.clone( totalIncomePlan );
			this.rrpChartDatasets[ 0 ].data.push( sheet.income_plan || 0 );
			this.rrpCustomDatasets[ 0 ].data.push( { income: sheet.income || 0, cost: sheet.cost || 0, income_plan: sheet.income_plan || 0, date: label } );
			this.rrpCustomDatasets[ 0 ].income += sheet.income || 0;
			this.rrpCustomDatasets[ 0 ].cost += sheet.cost || 0;
			this.rrpCustomDatasets[ 0 ].income_plan += sheet.income_plan || 0;

			// Receivable
			totalIncome += sheet.income || 0;
			sheet.income = _.clone( totalIncome );
			this.rrpChartDatasets[ 1 ].data.push( sheet.income || 0 );
			this.rrpCustomDatasets[ 1 ].data.push( { income: sheet.income || 0, cost: sheet.cost || 0, income_plan: sheet.income_plan || 0, date: label } );
			this.rrpCustomDatasets[ 1 ].income += sheet.income || 0;
			this.rrpCustomDatasets[ 1 ].cost += sheet.cost || 0;
			this.rrpCustomDatasets[ 1 ].income_plan += sheet.income_plan || 0;

			// Payable
			totalCost += sheet.cost || 0;
			sheet.cost = _.clone( totalCost );
			this.rrpChartDatasets[ 2 ].data.push( sheet.cost || 0 );
			this.rrpCustomDatasets[ 2 ].data.push( { income: sheet.income || 0, cost: sheet.cost || 0, income_plan: sheet.income_plan || 0, date: label } );
			this.rrpCustomDatasets[ 2 ].income += sheet.income || 0;
			this.rrpCustomDatasets[ 2 ].cost += sheet.cost || 0;
			this.rrpCustomDatasets[ 2 ].income_plan += sheet.income_plan || 0;
		} );

		const currentDateLabelIndex: number = _.indexOf(labels, currentDateLabelKey);
		let xAdjust: number = 0;

		if (currentDateLabelIndex === 0) xAdjust = 50;
		if (currentDateLabelIndex === (labels.length - 1)) xAdjust = -50;

		this.rrpChartLabels = this.hiddenLabels(labels, currentDateLabelKey);
		this.rrpChartDatasets[ 0 ].pointRadius = this.processChartDataRRP(this.rrpChartDatasets[ 0 ], this.rrpCustomDatasets[ 0 ], 0); // blue
		this.rrpChartDatasets[ 1 ].pointRadius = this.processChartDataRRP(this.rrpChartDatasets[ 1 ], this.rrpCustomDatasets[ 1 ], 1); // green
		this.rrpChartDatasets[ 2 ].pointRadius = this.processChartDataRRP(this.rrpChartDatasets[ 2 ], this.rrpCustomDatasets[ 2 ], 2); // red

		this.rrpChartOptions = this.chartJSOptions({
			datasets: this.rrpChartDatasets,
			scales: {
				yAxes: [{
					ticks: {
						callback: function(label, _index, _labels) {
							return label;
						}
					}
				}],
				xAxes: [{
					ticks: {
						callback: function(label, _index, _labels) {
							return label ? _this.convertLabelToDate(label) : '';
						}
					}
				}]
			},
			tooltipLabel: _this.translateService.instant('FINANCE.PROJECT.LABELS.TOTAL'),
			annotation: {
				annotations: [
					{
						value: currentDateLabelKey,
						drawTime: 'afterDatasetsDraw',
						type: 'line',
						mode: 'vertical',
						scaleID: 'x-axis-0',
						borderColor: 'red',
						borderWidth: 3,
						label: {
							xAdjust,
							enabled: true,
							position: 'center',
							content: _this.translateService.instant(
								this.filters.type === 'week' ? 'GENERAL.LABELS.CURRENT_WEEK' : 'GENERAL.LABELS.CURRENT_MONTH'
							),
						},
					},
				],
			},
			title: {
				display: true,
				text: _this.translateService.instant('FINANCE.PROJECT.LABELS.RECEIVABLE_PAYABLE')  + ' ' + this.translateService.instant("CHART.BILLIONS"),
				fontSize: 30
			},
			tooltips: {
				enabled: false,
				mode: 'index',
				intersect: false,
				/* tslint:disable-next-line */
				custom(tooltipModel: any) {
					// Hide if no tooltip
					if (!tooltipModel.opacity) {
						$('.chartjs-tooltip').css({ opacity: 0 });
						return;
					}

					// Set Text
					if (tooltipModel.body && _this.rrpChartDatasets) {
						let dataPoints: Array<any> = tooltipModel.dataPoints;
						const pointIndexs = _.map(dataPoints, 'datasetIndex');

						if (dataPoints.length < 3) {
							if (!pointIndexs.includes(0)) {
								dataPoints.push({
									datasetIndex: 0,
									status: 0,
									index: dataPoints[ 0 ].index,
									label: dataPoints[ 0 ].label,
									value: '',
									x: 0,
									xLabel: '',
									y: 0,
									yLabel: 0,
								});
							}
							if (!pointIndexs.includes(1)) {
								dataPoints.push({
									datasetIndex: 1,
									status: 0,
									index: dataPoints[ 0 ].index,
									label: dataPoints[ 0 ].label,
									value: '',
									x: 0,
									xLabel: '',
									y: 0,
									yLabel: 0,
								});
							}
							if (!pointIndexs.includes(2)) {
								dataPoints.push({
									datasetIndex: 2,
									status: 0,
									index: dataPoints[ 0 ].index,
									label: dataPoints[ 0 ].label,
									value: '',
									x: 0,
									xLabel: '',
									y: 0,
									yLabel: 0,
								});
							}
						}

						dataPoints = dataPoints.sort(function(a, b) {
							return a.datasetIndex - b.datasetIndex;
						});


						// `this` will be the overall tooltip
						const position: any = this._chart.canvas.getBoundingClientRect();

						// Prevent tootip overlap
						const posArr: Array<any> = [
							{ position: (dataPoints[ 0 ] && dataPoints[ 0 ].y) || 0, index: 0 },
							{ position: (dataPoints[ 1 ] && dataPoints[ 1 ].y) || 0, index: 1 },
							{ position: (dataPoints[ 2 ] && dataPoints[ 2 ].y) || 0, index: 2 },
						];

						const newPos: Array<number> = [
							(dataPoints[ 0 ] && dataPoints[ 0 ].y) || 0,
							(dataPoints[ 1 ] && dataPoints[ 1 ].y) || 0,
							(dataPoints[ 2 ] && dataPoints[ 2 ].y) || 0,
						];

						posArr.sort((a: any, b: any) => {
							return b.position > a.position ? 1 : -1;
						});

						if (posArr[ 0 ].position - newPos[ posArr[ 1 ].index ] < 68) {
							newPos[ posArr[ 1 ].index ] -= 68;
						}

						if (newPos[ posArr[ 1 ].index ] - newPos[ posArr[ 2 ].index ] < 68) {
							newPos[ posArr[ 2 ].index ] = newPos[ posArr[ 1 ].index ] - 68;
						}
						let cost;
						let costAfter;

						// Receivable Plan tooltip
						let receivablePlanTooltipEl: any = document.getElementById('tooltip-' + dataPoints[ 0 ].datasetIndex);
						if (dataPoints[ 0 ] &&  dataPoints[ 0 ].status != 0 && _this.rrpChartDatasets[ 0 ].pointRadius[ dataPoints[ 0 ].index ]) {
							if (!receivablePlanTooltipEl) {
								receivablePlanTooltipEl = document.createElement('div');
								receivablePlanTooltipEl.id = 'tooltip-' + dataPoints[ 0 ].datasetIndex;
								receivablePlanTooltipEl.classList.add('chartjs-tooltip');
								document.body.appendChild(receivablePlanTooltipEl);
							}

							cost = _this.rrpCustomDatasets[ dataPoints[ 0 ].datasetIndex ].data[ dataPoints[ 0 ].index ].income_plan
								? NumberService.addCommas(_this.rrpCustomDatasets[ dataPoints[ 0 ].datasetIndex ].data[ dataPoints[ 0 ].index ].income_plan)
								: 0;

							costAfter = NumberService.addCommas(
								(
									_this.rrpCustomDatasets[ dataPoints[ 0 ].datasetIndex ].data[ dataPoints[ 0 ].index ] &&
									_this.rrpCustomDatasets[ dataPoints[ 0 ].datasetIndex ].data[ dataPoints[ 0 ].index ].income_plan ?
										_this.rrpCustomDatasets[ dataPoints[ 0 ].datasetIndex ].data[ dataPoints[ 0 ].index ].income_plan : 0
								) -
								(
									_this.rrpCustomDatasets[ dataPoints[ 0 ].datasetIndex ].data[ dataPoints[ 0 ].index - 1 ] &&
									_this.rrpCustomDatasets[ dataPoints[ 0 ].datasetIndex ].data[ dataPoints[ 0 ].index - 1 ].income_plan ?
										_this.rrpCustomDatasets[ dataPoints[ 0 ].datasetIndex ].data[ dataPoints[ 0 ].index - 1 ].income_plan : 0
								)
							);


							// tslint:disable-next-line:max-line-length
							receivablePlanTooltipEl.innerHTML = _this.translateService.instant('FINANCE.PROJECT.LABELS.DATE') + ': ' + _this.convertLabelToDate(_this.rrpCustomDatasets[ dataPoints[ 0 ].datasetIndex ].data[ dataPoints[ 0 ].index ].date) +
								'<br/><strong>' + _this.translateService.instant('FINANCE.PROJECT.LABELS.INCOME') + ': ' + cost + '</strong><br/><strong style="color: ' + _this.RECEIVABLE_PAYABLE_CHART_RECEIVABLE_PLAN_COLOR() + '">+ ' + costAfter + '</strong>'
							;

							if (+costAfter !== 0) receivablePlanTooltipEl.style.opacity = 1;
							receivablePlanTooltipEl.style.position = 'absolute';
							receivablePlanTooltipEl.style.top = newPos[ 0 ] + position.top + window.pageYOffset - 45 + 'px';
							receivablePlanTooltipEl.style.left = dataPoints[ 0 ].x + 125
								+ (receivablePlanTooltipEl.offsetLeft > position.width / 2 ? -receivablePlanTooltipEl.offsetWidth : 0) + 'px';
							receivablePlanTooltipEl.style.zIndex = 1;
							receivablePlanTooltipEl.style.borderRadius = '4px';
							receivablePlanTooltipEl.style.borderColor = UtilitiesService.hexToRgba(COLORS.ACCENT, .41);
							receivablePlanTooltipEl.style.borderStyle = 'solid';
							receivablePlanTooltipEl.style.padding = '8px 20px';
							receivablePlanTooltipEl.style.backgroundColor = UtilitiesService.hexToRgba(COLORS.WHITE);

						} else {
							if (receivablePlanTooltipEl) {
								receivablePlanTooltipEl.style.opacity = 0;
							}
						}

						// Receivable tooltip
						let receivableTooltipEl: any = document.getElementById('tooltip-' + dataPoints[ 1 ].datasetIndex);
						if (dataPoints[ 1 ] && dataPoints[ 1 ].datasetIndex && dataPoints[ 1 ].status !== 0 &&
							_this.rrpChartDatasets[ 1 ].pointRadius[ dataPoints[ 1 ].index ]) {
							if (!receivableTooltipEl) {
								receivableTooltipEl = document.createElement('div');
								receivableTooltipEl.id = 'tooltip-' + dataPoints[ 1 ].datasetIndex;
								receivableTooltipEl.classList.add('chartjs-tooltip');
								document.body.appendChild(receivableTooltipEl);
							}

							cost = (_this.rrpCustomDatasets[ dataPoints[ 1 ].datasetIndex ].data[ dataPoints[ 1 ].index ] &&
								_this.rrpCustomDatasets[ dataPoints[ 1 ].datasetIndex ].data[ dataPoints[ 1 ].index ].income)
								? NumberService.addCommas(_this.rrpCustomDatasets[ dataPoints[ 1 ].datasetIndex ].data[ dataPoints[ 1 ].index ].income)
								: 0;
							costAfter = NumberService.addCommas(
								(
									(
										_this.rrpCustomDatasets[ dataPoints[ 1 ].datasetIndex ].data[ dataPoints[ 1 ].index ] &&
										_this.rrpCustomDatasets[ dataPoints[ 1 ].datasetIndex ].data[ dataPoints[ 1 ].index ].income) ?
										_this.rrpCustomDatasets[ dataPoints[ 1 ].datasetIndex ].data[ dataPoints[ 1 ].index ].income : 0
								) -
								(
									_this.rrpCustomDatasets[ dataPoints[ 1 ].datasetIndex ].data[ dataPoints[ 1 ].index - 1 ] &&
									_this.rrpCustomDatasets[ dataPoints[ 1 ].datasetIndex ].data[ dataPoints[ 1 ].index - 1 ].income ?
										_this.rrpCustomDatasets[ dataPoints[ 1 ].datasetIndex ].data[ dataPoints[ 1 ].index - 1 ].income : 0
								)
							);
							// tslint:disable-next-line:max-line-length
							receivableTooltipEl.innerHTML = _this.translateService.instant('FINANCE.PROJECT.LABELS.DATE') + ': ' +
								(
									_this.rrpCustomDatasets[ dataPoints[ 1 ].datasetIndex ].data[ dataPoints[ 1 ].index ] ?
										_this.convertLabelToDate(_this.rrpCustomDatasets[ dataPoints[ 1 ].datasetIndex ].data[ dataPoints[ 1 ].index ].date) : ''
								) +
								'<br/><strong>' + _this.translateService.instant('FINANCE.PROJECT.LABELS.INCOME') + ': ' + cost + '</strong><br/><strong style="color: ' + _this.RECEIVABLE_PAYABLE_CHART_RECEIVABLE_COLOR() + '">+ ' + costAfter + '</strong>';

							if (+costAfter !== 0) receivableTooltipEl.style.opacity = 1;
							receivableTooltipEl.style.position = 'absolute';
							receivableTooltipEl.style.top = newPos[ 1 ] + position.top + window.pageYOffset - 45 + 'px';
							receivableTooltipEl.style.left = dataPoints[ 1 ].x + 125
								+ (receivableTooltipEl.offsetLeft > position.width / 2 ? -receivableTooltipEl.offsetWidth : 0) + 'px';
							receivableTooltipEl.style.zIndex = 1;
							receivableTooltipEl.style.borderRadius = '4px';
							receivableTooltipEl.style.borderColor = UtilitiesService.hexToRgba(COLORS.SUCCESS, .41);
							receivableTooltipEl.style.borderStyle = 'solid';
							receivableTooltipEl.style.padding = '8px 20px';
							receivableTooltipEl.style.backgroundColor = UtilitiesService.hexToRgba(COLORS.WHITE);
						} else {
							if (receivableTooltipEl) {
								receivableTooltipEl.style.opacity = 0;
							}
						}

						// Payable tooltip
						let payableTooltipEl: any = document.getElementById('tooltip-' + dataPoints[ 2 ].datasetIndex);
						if (dataPoints[ 1 ] && dataPoints[ 1 ].datasetIndex && dataPoints[ 2 ] && dataPoints[ 2 ].datasetIndex &&
							dataPoints[ 2 ].status !== 0 && _this.rrpChartDatasets[ 2 ].pointRadius[ dataPoints[ 2 ].index ]) {
							if (!payableTooltipEl) {
								payableTooltipEl = document.createElement('div');
								payableTooltipEl.id = 'tooltip-' + dataPoints[ 2 ].datasetIndex;
								payableTooltipEl.classList.add('chartjs-tooltip');
								document.body.appendChild(payableTooltipEl);
							}

							cost = _this.rrpCustomDatasets[ dataPoints[ 2 ].datasetIndex ].data[ dataPoints[ 2 ].index ].cost
								? NumberService.addCommas(_this.rrpCustomDatasets[ dataPoints[ 2 ].datasetIndex ].data[ dataPoints[ 2 ].index ].cost)
								: 0;

							costAfter = NumberService.addCommas(
								(
									_this.rrpCustomDatasets[ dataPoints[ 2 ].datasetIndex ].data[ dataPoints[ 2 ].index ] &&
									_this.rrpCustomDatasets[ dataPoints[ 2 ].datasetIndex ].data[ dataPoints[ 2 ].index ].cost ?
										_this.rrpCustomDatasets[ dataPoints[ 2 ].datasetIndex ].data[ dataPoints[ 2 ].index ].cost : 0
								) -
								(
									_this.rrpCustomDatasets[ dataPoints[ 2 ].datasetIndex ].data[ dataPoints[ 2 ].index - 1 ] &&
									_this.rrpCustomDatasets[ dataPoints[ 2 ].datasetIndex ].data[ dataPoints[ 2 ].index - 1 ].cost ?
										_this.rrpCustomDatasets[ dataPoints[ 2 ].datasetIndex ].data[ dataPoints[ 2 ].index - 1 ].cost : 0)
							);

							// tslint:disable-next-line:max-line-length
							payableTooltipEl.innerHTML = _this.translateService.instant('FINANCE.PROJECT.LABELS.DATE') + ': ' + _this.convertLabelToDate(_this.rrpCustomDatasets[ dataPoints[ 2 ].datasetIndex ].data[ dataPoints[ 2 ].index ].date) +
								'<br /> <strong>' + _this.translateService.instant('FINANCE.PROJECT.LABELS.COST') + ': ' + cost + '</strong><br/><strong style="color: ' + _this.RECEIVABLE_PAYABLE_CHART_PAYABLE_COLOR() + '">+ ' + costAfter + '</strong>'
							;

							if (+costAfter !== 0) payableTooltipEl.style.opacity = 1;
							payableTooltipEl.style.position = 'absolute';
							payableTooltipEl.style.top = newPos[ 2 ] + position.top + window.pageYOffset - 45 + 'px';
							payableTooltipEl.style.left = dataPoints[ 2 ].x + 125
								+ (payableTooltipEl.offsetLeft > position.width / 2 ? -payableTooltipEl.offsetWidth : 0) + 'px';
							payableTooltipEl.style.zIndex = 1;
							payableTooltipEl.style.borderRadius = '4px';
							payableTooltipEl.style.borderColor = UtilitiesService.hexToRgba(_this.CASH_FLOW_CHART_REAL_COLOR(), .41);
							payableTooltipEl.style.borderStyle = 'solid';
							payableTooltipEl.style.padding = '8px 20px';
							payableTooltipEl.style.backgroundColor = UtilitiesService.hexToRgba(COLORS.WHITE);

						} else {
							if (payableTooltipEl) {
								payableTooltipEl.style.opacity = 0;
							}
						}
					}
				},
			},
		});
	}

	// tslint:disable-next-line:valid-jsdoc
	/**
	 * Calcualte real and receivable plan
	 * @param {array} result
	 * @return {array}
	 */
	public processChartData(data: any, data2: any) {
		const radius = [];
		radius.push(3);
		for (let index: number = 1; index < data2.data.length; index++) {
			radius.push(0);
		}
		for (let index: number = 0; index < data2.data.length; index++) {
			if (data2.data[ index ].income !== 0 || data2.data[ index ].cost !== 0) {
				radius[ index ] = 3;
			}
		}
		const data3: Array<any> = [];
		for (let i: number = 0; i < data.data.length; i++) {
			if (radius[ i ]) {
				data3.push(data.data[ i ]);
			}
		}
		for (let i: number = 0; i < data3.length - 1; i++) {
			const index1: number = data.data.indexOf(data3[ i ]);
			const index2: number = data.data.indexOf(data3[ i + 1 ]);
			const y0: number = data.data[ index1 ];
			const yn: number = data.data[ index2 ];
			const deltaX1: number = index2 - index1;
			for (let j: number = index1 + 1; j < index2; j++) {
				const deltaX2: number = index2 - j;
				data.data[ j ] = (y0 - yn) * deltaX2 / deltaX1 + yn;
			}
		}
		const ind0: number = data.data.indexOf(data3[ 0 ]);
		if (ind0 > 1) {
			data.data[ 0 ] = 0;
			const y0: number = 0;
			const yn: number = data.data[ ind0 ];
			const deltaX1: number = ind0;
			for (let j: number = 1; j < ind0; j++) {
				const deltaX2: number = ind0 - j;
				data.data[ j ] = (y0 - yn) * deltaX2 / deltaX1 + yn;
			}
		}
		// end cutting
		const lastIndex: number = data.data.length - 1;

		for (let i = data.data.indexOf(data3[ data3.length - 1 ]) + 1; i < data.data.length; i++) {
			data.data.pop();
			data2.data.pop();
		}

		if (!radius[ lastIndex ]) {
			data.data.pop();
			data2.data.pop();
		}

		return radius;
	}

	public processChartDataRRP(data: any, data2: any, layer: number) {
		const radius = [];
		radius.push(3);
		for (let index: number = 1; index < data2.data.length; index++) {
			radius.push(0);
		}
		for (let index: number = 1; index < data2.data.length; index++) {
			let canChange: number = 0;
			if (layer === 1) {
				canChange = +NumberService.addCommas(
					(
						data2.data[ index ] &&
						data2.data[ index ].income ?
							data2.data[ index ].income : 0
					) -
					(
						data2.data[ index ] &&
						data2.data[ index - 1 ].income ?
							data2.data[ index - 1 ].income : 0
					)
				);
			} else if (layer === 2) {
				canChange = +NumberService.addCommas(
					(
						data2.data[ index ] &&
						data2.data[ index ].cost ?
							data2.data[ index ].cost : 0
					) -
					(
						data2.data[ index ] &&
						data2.data[ index - 1 ].cost ?
							data2.data[ index - 1 ].cost : 0
					)
				);
			} else {
				canChange = +NumberService.addCommas(
					(
						data2.data[ index ] &&
						data2.data[ index ].income_plan ?
							data2.data[ index ].income_plan : 0
					) -
					(
						data2.data[ index ] &&
						data2.data[ index - 1 ].income_plan ?
							data2.data[ index - 1 ].income_plan : 0
					)
				);
			}
			if (+canChange !== 0) {
				radius[ index ] = 3;
			}
		}
		const data3: Array<any> = [];
		for (let i: number = 0; i < data.data.length; i++) {
			if (radius[ i ]) {
				data3.push(data.data[ i ]);
			}
		}
		for (let i: number = 0; i < data3.length - 1; i++) {
			const index1: number = data.data.indexOf(data3[ i ]);
			const index2: number = data.data.indexOf(data3[ i + 1 ]);
			const y0: number = data.data[ index1 ];
			const yn: number = data.data[ index2 ];
			const deltaX1: number = index2 - index1;
			for (let j: number = index1 + 1; j < index2; j++) {
				const deltaX2: number = index2 - j;
				data.data[ j ] = (y0 - yn) * deltaX2 / deltaX1 + yn;
			}
		}
		const ind0: number = data.data.indexOf(data3[ 0 ]);
		if (ind0 > 1) {
			data.data[ 0 ] = 0;
			const y0: number = 0;
			const yn: number = data.data[ ind0 ];
			const deltaX1: number = ind0;
			for (let j: number = 1; j < ind0; j++) {
				const deltaX2: number = ind0 - j;
				data.data[ j ] = (y0 - yn) * deltaX2 / deltaX1 + yn;
			}
		}

		const lastIndex: number = data.data.length - 1;

		for (let i = data.data.indexOf(data3[ data3.length - 1 ]) + 1; i < data.data.length; i++) {
			data.data.pop();
			data2.data.pop();
		}

		if (!radius[ lastIndex ]) {
			data.data.pop();
			data2.data.pop();
		}

		return radius;
	}

	public roundNumber(number, digits) {
		return TableUtil.getNumberFormatForExcel(number, digits);
	}

	public exportExcel() {
		const headerSetting = {
			header: [
				this.translateService.instant('FINANCE.PROJECT.LABELS.PROJECT_NAME'),
				this.translateService.instant('FINANCE.PROJECT.LABELS.COST')
			],
			noData: this.translateService.instant('FINANCE.SETTINGS.MESSAGES.NO_RECORD_DATA_NAME', { name: 'item' }),
			fgColor: 'ffffff',
			bgColor: '00245A',
		};
		const title = 'Project Analysis';
		const sheetName = 'Projects';
		const fileName = 'Project_Analysis';
		const exportData: any[] = [];
		this.dataTable.forEach((item: any) => {
			const dataItem: any[] = [];
			dataItem.push(item.project_name || 'N/A');
			const cost: any = {
				richText: [
					{
						font: { size: 12 },
						text: item.cost_total ? ( this.roundNumber((item.purchase_total + item.po_total - item.cost_total)/item.cost_total * 100, 1) + '%') : 'N/A'
					}
				],
			};
			if (item.purchase_total + item.po_total - item.cost_total === 0) {
				cost.richText[ 0 ].font.color = { argb: '31708f' };
			} else if (item.purchase_total + item.po_total - item.cost_total > 0) {
				cost.richText[ 0 ].font.color = { argb: '38ae00' };
				cost.richText[ 0 ].text += '▼';
			} else {
				cost.richText[ 0 ].font.color = { argb: 'ff3636' };
				cost.richText[ 0 ].text += '▲';
			}
			dataItem.push(cost);
			exportData.push(dataItem);
		});
		this.excelService.exportArrayToExcel(
			exportData,
			title,
			headerSetting,
			sheetName,
			fileName,
			null,
			null,
			true
		);
	}
}
