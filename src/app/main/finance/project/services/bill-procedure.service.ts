import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';

import { ApiService } from '@core';

@Injectable()
export class BillProcedureService {

	/**
	* @constructor
	* @param {ApiService} apiService
	*/
	constructor( private apiService: ApiService ) {}

	/**
	* Get project bill procedures
	* @param {number} billId
	* @return {Observable}
	*/
	public getProcedures( ): Observable<any> {

		return new Observable( ( observer: Observer<any> ) => {
			this.apiService
			.call(
				'/api/finance/bill_procedure/list',
				'GET'
			)
			.subscribe(
				( result: any ) => observer.next( result ),
				( error: any ) => observer.error( error ),
				() => observer.complete()
			);
		} );
	}

	/**
	* Get project bill procedures
	* @param {number} billId
	* @return {Observable}
	*/
	public getAll( billId?: number ): Observable<any> {

		return new Observable( ( observer: Observer<any> ) => {
			this.apiService
			.call(
				'/api/finance/bill_procedure/list/' + billId,
				'GET'
			)
			.subscribe(
				( result: any ) => observer.next( result ),
				( error: any ) => observer.error( error ),
				() => observer.complete()
			);
		} );
	}

	/**
	* Get project bill
	* @param {number} id
	* @return {Observable}
	*/
	public getOne( id?: number ): Observable<any> {
		return new Observable( ( observer: Observer<any> ) => {
			this.apiService
			.call( '/api/finance/bill_procedure/' + id )
			.subscribe(
				( result: any ) => observer.next( result ),
				( error: any ) => observer.error( error ),
				() => observer.complete()
			);
		} );
	}

	/**
	* Create project bill
	* @param {any} data - project data to create
	* @return {Observable}
	*/
	public create( data: any ): Observable<any> {
		const params: any = {
			project_id				: data.project_id,
			is_settlement			: data.is_settlement,
			name					: data.name,
			total					: data.total,
			total_vat				: data.total_vat,
			expected_invoice_date	: data.expected_invoice_date,
			transfer_type			: data.transfer_type,
		};

		return new Observable( ( observer: Observer<any> ) => {
			this.apiService
			.call(
				'/api/finance/bill_procedure/create',
				'POST',
				params
			)
			.subscribe(
				( result: any ) => observer.next( result ),
				( error: any ) => observer.error( error ),
				() => observer.complete()
			);
		} );
	}

	/**
	* Create project bills
	* @param {any} data - project data to create
	* @return {Observable}
	*/
	public createBatch( data: any ): Observable<any> {

		return new Observable( ( observer: Observer<any> ) => {
			this.apiService
			.call(
				'/api/finance/bill_procedure/create_batch',
				'POST',
				data
			)
			.subscribe(
				( result: any ) => observer.next( result ),
				( error: any ) => observer.error( error ),
				() => observer.complete()
			);
		} );
	}

	/**
	* Update   procedure bill
	* @param {any} id - project id to update
	* @param {any} data - project data to update
	* @return {Observable}
	*/
	public update( id: number, data: any ): Observable<any> {
		const params: any = {
			name					: data.name,
			bill_id					: data.bill_id,
			created_by				: data.created_by,
			assignee				: data.assignee,
			doc_category_id			: data.doc_category_id,
			attachments				: data.attachments
		};

		return new Observable( ( observer: Observer<any> ) => {
			this.apiService
			.call(
				'/api/finance/bill_procedure/update/' + id,
				'PUT',
				params
			)
			.subscribe(
				( result: any ) => observer.next( result ),
				( error: any ) => observer.error( error ),
				() => observer.complete()
			);
		} );
	}

	/**
	* Update bill procedure status
	* @param {int} id - bill procedure id to update
	* @param {int} status - bill procedure status to update
	* @return {Observable}
	*/
	public updateStatus( data: any ): Observable<any> {
		const params: any = {
			status: data.status,
			comment: data.comment
		};

		return new Observable( ( observer: Observer<any> ) => {
			this.apiService
			.call(
				'/api/finance/bill_procedure/update_status/' + data.id,
				'PUT',
				params
			)
			.subscribe(
				( result: any ) => observer.next( result ),
				( error: any ) => observer.error( error ),
				() => observer.complete()
			);
		} );
	}

	/**
	* Delete bill procedure
	* @param {number} id - Bill procedure id to delete
	* @return {Observable}
	*/
	public delete( id: number ): Observable<any> {
		return new Observable( ( observer: Observer<any> ) => {
			this.apiService
			.call(
				'/api/finance/bill_procedure/delete/' + id,
				'DELETE'
			)
			.subscribe(
				( result: any ) => observer.next( result ),
				( error: any ) => observer.error( error ),
				() => observer.complete()
			);
		} );
	}
}
