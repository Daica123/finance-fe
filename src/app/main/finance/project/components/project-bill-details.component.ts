import {Component, Injector, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {SnackBarService} from 'angular-core';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import 'chartjs-plugin-annotation';
import {ProjectBaseComponent} from '@finance/project/project-base.component';
import {ProjectService} from '@finance/project/services/project.service';
import _ from 'underscore';
import {BILL_PROCEDURE_STATUS, COLORS} from '@resources';
import {DocCategoryService} from "@finance/doc-category/services/doc-category.service";
import {UserService} from "@finance/user/services/user.service";
import {AccountService} from "@account/services/account.service";
import {MatSort} from "@angular/material/sort";
import {ProjectBillService} from "@finance/project/services/project-bill.service";
import {FileService} from "@finance/procedure/services/file.service";
import {TableUtil} from "@app/utils/tableUtils";
import {BillProcedureService} from "@finance/project/services/bill-procedure.service";
import {DialogProcedureAttachmentComponent} from "@finance/procedure/components/dialog-procedure-attachment.component";
import {MatDialog, MatTableDataSource} from '@angular/material';
import {DialogProcedureApproveComponent} from "@finance/procedure/components/dialog-procedure-approve.component";

@Component({
    selector: 'project-bill-details',
    templateUrl: '../templates/project-bill-details.pug',
    styleUrls: ['../styles/project-bill-details.scss'],
})
export class ProjectBillDetailsComponent extends ProjectBaseComponent implements OnInit, OnDestroy {

    @ViewChild('billDetailsSort') public billDetailsSort: MatSort;
    @Input() public projectId: number;
    @Input() public loaded: boolean;

    public COLORS: any = COLORS;
    public userProfile: any = {};

    public dataSourceDetails: MatTableDataSource<any> = new MatTableDataSource<any>( [] );
    public displayedColumnsDetails: Array<string> = [
        'item', 'name', 'belong_to', 'assignee', 'type', 'status',
        'deadline', 'attachments', 'comment', 'actions'
    ];

    constructor(
        public dialog					: MatDialog,
        public injector                 : Injector,
        public snackBarService          : SnackBarService,
        public translateService         : TranslateService,
        public router                   : Router,
        public route                    : ActivatedRoute,
        public projectService           : ProjectService,
        public projectBillService		: ProjectBillService,
        public billProcedureService		: BillProcedureService,
        public docCategoryService		: DocCategoryService,
        public userService				: UserService,
        public accountService			: AccountService,
        public fileService			    : FileService
    ) {
        super(injector);
        this.accountService.getProfile().subscribe(userProfile => {
           this.userProfile = userProfile;
        });
    }

    /**
     * @constructor
     */
    public ngOnInit() {
        this.initData();
        this.dataSourceDetails.sortingDataAccessor = (data: any, sortHeaderId: string) => {
            const value: any = data[ sortHeaderId ];
            return typeof value === 'string' ? value.toLowerCase() : value;
        };
    }

    /**
     * @constructor
     */
    public ngOnDestroy() {
        super.ngOnDestroy();
    }

    /**
     * Init all data
     * @return {void}
     */
    public initData() {
        this.projectBillService
            .getAll( this.projectId )
            .subscribe( ( projects: any ) => {
                const data: any[] = [];
                const totalArr: number[] = projects.map(pr => pr.procedures.length ? pr.procedures.length : 1);
                const total = totalArr.length ? totalArr.reduce((a, b) =>  a + b) : 0;
                let run = 0;
                projects.forEach(project => {
                    if (project.procedures.length) {
                        project.procedures.forEach((billProcedure: any) => {
                            billProcedure.bill_procedure_status_name = _.findWhere( this.billProcedureStatus, {
                                id: (billProcedure.status === 2) ? billProcedure.status : ((this.datediff(billProcedure.deadline) > 0) ? 0 : 1)
                            } );
                            const promises = [];
                            if (billProcedure.doc_category_id) {
                                promises.push(
                                    this.docCategoryService.getOne(billProcedure.doc_category_id)
                                );
                            } else {
                                promises.push({});
                            }
                            if (billProcedure.created_by) {
                                promises.push(
                                    this.userService.getUser(billProcedure.created_by)
                                );
                            } else {
                                promises.push({});
                            }
                            if (billProcedure.assignee) {
                                promises.push(
                                    this.userService.getUser(billProcedure.assignee)
                                );
                            } else {
                                promises.push({});
                            }
                            Promise.all(promises).then(resData => {
                                resData[0].subscribe((docCategory: any) => {
                                    billProcedure.type = docCategory.name || '';
                                    resData[1].subscribe((createdBy: any) => {
                                        billProcedure.created_by = createdBy.full_name || '';
                                        resData[2].subscribe((assignee: any) => {
                                            billProcedure.assignee = assignee.full_name || '';
                                            data.push({
                                                ...billProcedure,
                                                bill_name: project.name,
                                                is_settlement: project.is_settlement
                                            });
                                            run++;
                                            if (run === total) {
                                                data.sort((a, b) => a.bill_name === b.bill_name ? 0 : (a.bill_name > b.bill_name ? 1 : -1));
                                                this.dataSourceDetails.data = data;
                                                this.dataSourceDetails.sort = this.billDetailsSort;
                                            }
                                        });
                                    });
                                });
                            });
                        });
                    } else {
                        run++;
                        data.push({
                            bill_name: project.name,
                            name: '',
                            belong_to: '',
                            assignee: '',
                            deadline: '',
                            is_settlement: project.is_settlement,
                            type: '',
                            status: 0,
                            attachments: []
                        });
                        if (run === total) {
                            data.sort((a, b) => a.bill_name === b.bill_name ? 0 : (a.bill_name > b.bill_name ? 1 : -1));
                            this.dataSourceDetails.data = data;
                            this.dataSourceDetails.sort = this.billDetailsSort;
                        }
                    }
                });
            });
    }

    public download(fileRealName: string):  void {
        this.fileService.download(fileRealName);
    }

    public datediff(d2: Date) {
        const d1 = new Date();
        return TableUtil.datediff(d1, new Date(d2));
    }

    public datediff2(item: any) {
        const d2 = new Date(item.deadline);
        const d1 = new Date(item.updated_at);
        return TableUtil.datediff(d1, d2);
    }

    public deleteReal(id: number, name: string) {
        this.billProcedureService.delete(id).subscribe( _res => {
            this.snackBarService.success( 'GENERAL.MESSAGES.DELETE_SUCCESS', { name: 'bill procedure \"' +  name + '\"'});
            this.initData();
        });
    }

    public canApprove() {
        const list = ['CEO', 'CFO', 'GENERAL_ACCOUNTANT', 'PM'];
        if (this.userProfile.role_key) {
            return list.indexOf(this.userProfile.role_key) !== -1;
        }
        return false;
    }

    public canReject() {
        const list = ['CEO', 'CFO'];
        if (this.userProfile.role_key) {
            return list.indexOf(this.userProfile.role_key) !== -1;
        }
        return false;
    }

    public canAttachment(item: any) {
        const can: boolean = item.assignee && (this.userProfile.id == item.assignee.id || this.userProfile.role_key === 'GENERAL_ACCOUNTANT');
        return can;
    }

    public openAttachmentDialog(item) {
        const dialogRef: any = this.dialog.open(
            DialogProcedureAttachmentComponent,
            {
                hasBackdrop: true,
                position: {
                    top: '70px'
                },
                width: '1050px',
                data: {
                    ...item,
                    bill: {
                        project: {
                            id: this.projectId
                        },
                        id: item.bill_id
                    },
                    doc_category: {
                        id: item.doc_category_id
                    }
                }
            }
        );
        dialogRef
            .afterClosed()
            .subscribe( ( _result: any ) => {
                if (!_result) return;
                this.initData();
            } );
    }

    public approve( item: any ) {
        const dialogData: any = {
            id		: item.id,
            status	: BILL_PROCEDURE_STATUS.CONFIRMED,
            confirmation:  this.translateService.instant(
                'APPROVE_BILL_PROCEDURE_CONFIRMATION',
                {name: item.name}
            )
        };
        const dialogRef: any = this.dialog.open(
            DialogProcedureApproveComponent,
            {
                width	: '400px',
                data	: dialogData,
            }
        );

        dialogRef
            .afterClosed()
            .subscribe( ( _result: any ) => {
                if (!_result) return;
                this.initData();
            } );
    }

    public reject( item: any ) {
        const dialogData: any = {
            id			: item.id,
            status		: BILL_PROCEDURE_STATUS.DELAYED,
            confirmation:  this.translateService.instant(
                'REJECT_BILL_PROCEDURE_CONFIRMATION',
                {name: item.name}
            )
        };
        const dialogRef: any = this.dialog.open(
            DialogProcedureApproveComponent,
            {
                width	: '400px',
                data	: dialogData,
            }
        );

        dialogRef
            .afterClosed()
            .subscribe( ( _result: any ) => {
                if (!_result) return;
                this.initData();
            } );
    }
}
