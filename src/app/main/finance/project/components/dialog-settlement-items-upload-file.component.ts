import {Component, Inject, Injector, OnDestroy, OnInit} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import _ from 'underscore';

import {FinanceBaseComponent} from '@finance/finance-base.component';
import {AccountService} from '@account/services/account.service';
import {SheetService} from '@finance/project/services/sheet.service';

@Component({
	selector	: 'dialog-settlement-items-upload-file',
	templateUrl	: '../templates/dialog-settlement-items-upload-file.pug',
})
export class DialogSettlementItemsUploadFileComponent extends FinanceBaseComponent implements OnInit, OnDestroy {
	private bill: any = {
		id: null,
		settlement_discount_type: null,
		settlement_discount: null
	};
	public discount: number = null;
	public discount_type: string = null;
	public isUploading: boolean;
	public fileLocation: any;
	public DISCOUNT_TYPE: Array<any> = [
		{ id: '%', name: '%' },
		{ id: '$', name: '$' },
	];
	/**
	* @constructor
	* @param {MatDialogRef} dialogRef
	* @param {Injector} injector
	* @param {AccountService} accountService
	* @param {SheetService} sheetService
	*/
	constructor(
		@Inject( MAT_DIALOG_DATA ) public data: any,
		public dialogRef		: MatDialogRef<DialogSettlementItemsUploadFileComponent>,
		public injector			: Injector,
		public accountService	: AccountService,
		public sheetService		: SheetService
	) {
		super( injector );
	}

	/**
	* @constructor
	*/
	public ngOnInit() {
		this.data && _.assign( this.bill, this.data.bill );
		if (this.bill.id) {
			this.discount = this.bill.settlement_discount;
			this.discount_type = this.bill.settlement_discount_type;
		}
	}

	/**
	* @constructor
	*/
	public ngOnDestroy() {
		super.ngOnDestroy();
	}

	/**
	* Click No button event
	* @return {void}
	*/
	public onNoClick() {
		this.dialogRef.close( false );
	}

	/**
	* Handle upload Project line items file
	* @param {any} event
	* @return {void}
	*/
	public onFileSelected( event: any ) {
		this.fileLocation = event.file;

		const reader: FileReader = new FileReader();

		reader.onload = ( _e: any ) => {
		};

		reader.readAsArrayBuffer( this.fileLocation );
	}

	/**
	* Handle upload
	* @return {void}
	*/
	public onUpload() {
		this.dialogRef.close( {
			file_location       : this.fileLocation,
			discount			: this.discount,
			discount_type		: this.discount_type
		} );
	}

	public validateDiscount() {
		if (this.discount_type) {
			if (this.discount_type === this.DISCOUNT_TYPE[0].id) {
				if (this.discount >= 0 && this.discount <= 100) {
					return true;
				}

			} else {
				if (this.discount >= 0) {
					return true;
				}
			}
		}
		return false;
	}

}
