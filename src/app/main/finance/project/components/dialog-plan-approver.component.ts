import {
	Component, Inject, OnInit,
	OnDestroy, Injector
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SnackBarService } from 'angular-core';
import _ from 'underscore';

import { FinanceBaseComponent } from '@finance/finance-base.component';
import { ProjectService } from '@finance/project/services/project.service';
import {ProjectBillService} from "@finance/project/services/project-bill.service";

@Component({
	selector	: 'dialog-plan-approver',
	templateUrl	: '../templates/dialog-plan-approver.pug',
})
export class DialogPlanApproverComponent extends FinanceBaseComponent implements OnInit, OnDestroy {

	public planApproverForm: FormGroup;
	public isSubmitting: boolean;
	public planApprover: any = {};

	/**
	* @constructor
	* @param {any} data
	* @param {MatDialogRef} dialogRef
	* @param {Injector} injector
	* @param {FormBuilder} fb
	* @param {SnackBarService} snackBarService
	* @param {ProjectService} projectService
	*/
	constructor(
		@Inject( MAT_DIALOG_DATA ) public data: any,
		public dialogRef				: MatDialogRef<DialogPlanApproverComponent>,
		public injector					: Injector,
		public fb						: FormBuilder,
		public snackBarService			: SnackBarService,
		public projectService			: ProjectService,
		public projectBillService		: ProjectBillService
	) {
		super( injector );

		this.planApproverForm = fb.group({
			comment: [{ value: null, disabled: false }],
		});
	}

	/**
	* @constructor
	*/
	public ngOnInit() {
		this.data && _.assign( this.planApprover, this.data );
	}

	/**
	* @constructor
	*/
	public ngOnDestroy() {
		super.ngOnDestroy();
	}

	/**
	* Click No button event
	* @return {void}
	*/
	public onNoClick() {
		this.dialogRef.close();
	}

	/**
	* Update project payment approver
	* @return {void}
	*/
	public update() {
		this.isSubmitting = true;

		const updateData: any = {
			status	: this.planApprover.status,
			comment	: this.planApprover.comment,
		};
		const billForCreate: any[] = this.data.bills || [];
		const quotationInfo: any = this.data.quotationInfo;
		this.projectService
		.updatePlanStatus( this.planApprover.project.id, updateData, this.planApprover.type )
		.subscribe( ( result: any ) => {
			this.isSubmitting = false;

			if ( !result || !result.status ) {
				this.snackBarService.warn( 'FINANCE.PROJECT.MESSAGES.UPDATE_PLAN_FAIL', this.planApprover.project );
				return;
			}
			this.snackBarService.success('FINANCE.PROJECT.MESSAGES.UPDATE_PLAN_SUCCESS', this.planApprover.project);
			if (billForCreate.length > 0 && updateData.status === 2) {
				let runBill: number = 0;
				const totalProcessBills: number = billForCreate.length;
				let sumTotal: number = 0;
				let sumTotalVat: number = 0;
				billForCreate.filter((bill: any) => bill.is_settlement !== 1).forEach((bill: any) => {
					sumTotal += quotationInfo.normal.total * bill.target_percent / 100;
					sumTotalVat += quotationInfo.normal.vat * bill.target_percent / 100;
				});
				billForCreate.forEach((bill: any) => {
					const cloneBill: any = {
						is_settlement: bill.is_settlement ? true : false,
						name: bill.name,
						expected_invoice_date: bill.target_date,
						project_id: this.planApprover.project.id,
						transfer_type: 0,
						total: bill.is_settlement ? (quotationInfo.settlement.total - sumTotal) : quotationInfo.normal.total * bill.target_percent / 100,
						total_vat: (bill.is_settlement ? (quotationInfo.settlement.vat - sumTotalVat) : quotationInfo.normal.vat  * bill.target_percent / 100)
					};
					this.projectBillService.create(cloneBill).subscribe( (res: any) => {
						if ( !res || !res.status ) {
							this.snackBarService.warn( 'FINANCE.PROJECT.MESSAGES.CREATE_PROJECT_BILL_FAIL', this.planApprover.project );
							return;
						}
						runBill++;
						if (runBill === totalProcessBills) {
							this.snackBarService.success('FINANCE.PROJECT.MESSAGES.CREATE_PROJECT_BILL_SUCCESS', this.planApprover.project);
						}
					});
				});
			}
		} );

		this.dialogRef.close( true );
	}

}
