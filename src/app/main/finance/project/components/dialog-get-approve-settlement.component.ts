import {Component, Inject, Injector, OnDestroy, OnInit} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import moment from 'moment-timezone';

import {FinanceBaseComponent} from '@finance/finance-base.component';
import {AccountService} from '@account/services/account.service';
import {SheetService} from '@finance/project/services/sheet.service';

@Component({
	selector	: 'dialog-get-approve-settlement',
	styleUrls	: ['../styles/dialog-get-approve-settlement.scss'],
	templateUrl	: '../templates/dialog-get-approve-settlement.pug',
})
export class DialogGetApproveSettlementComponent extends FinanceBaseComponent implements OnInit, OnDestroy {
	public today: any;
	public available: number = null;
	public percentToMoney: number = null;
	public DISCOUNT_TYPE: Array<any> = [
		{ id: '%', name: '%' },
		{ id: '$', name: '$' },
	];
	public settlement_maintaince: number = 0;
	public settlement_maintaince_type: string = this.DISCOUNT_TYPE[0].id;
	public settlement_maintaince_deadline: any;
	/**
	* @constructor
	* @param {MatDialogRef} dialogRef
	* @param {Injector} injector
	* @param {AccountService} accountService
	* @param {SheetService} sheetService
	*/
	constructor(
		@Inject( MAT_DIALOG_DATA ) public data: any,
		public dialogRef		: MatDialogRef<DialogGetApproveSettlementComponent>,
		public injector			: Injector,
		public accountService	: AccountService,
		public sheetService		: SheetService
	) {
		super( injector );
		this.today = moment().clone();
		this.settlement_maintaince_deadline = moment().clone();
	}

	/**
	* @constructor
	*/
	public ngOnInit() {
		if (this.data) {
			this.available = this.data.available;
		}
	}

	/**
	* @constructor
	*/
	public ngOnDestroy() {
		super.ngOnDestroy();
	}

	/**
	* Click No button event
	* @return {void}
	*/
	public onNoClick() {
		this.dialogRef.close( false );
	}

	/**
	 * Handle send get approve
	 * @return {void}
	 */
	public onSend() {
		this.dialogRef.close( {
			settlement_maintaince_type: this.settlement_maintaince_type,
			settlement_maintaince: +this.settlement_maintaince,
			settlement_maintaince_deadline: this.settlement_maintaince_deadline.format()
		} );
	}

	public validateMaintaince() {
		if (this.settlement_maintaince_type) {
			if (this.settlement_maintaince_type === this.DISCOUNT_TYPE[0].id) {
				if (this.settlement_maintaince >= 0 && this.settlement_maintaince <= 50) {
					return true;
				}

			} else {
				if (this.settlement_maintaince >= 0 && this.settlement_maintaince <= this.available) {
					return true;
				}
			}
		}
		return false;
	}

	public getPercentToMoney() {
		if (this.settlement_maintaince_type) {
			if (this.settlement_maintaince_type === this.DISCOUNT_TYPE[0].id) {
				return this.settlement_maintaince * this.available / 100;

			} else {
				return null;
			}
		}
		return null;
	}

}
