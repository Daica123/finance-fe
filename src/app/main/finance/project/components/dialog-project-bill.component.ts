import {Component, Inject, Injector, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatTable, MatTableDataSource} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SnackBarService} from 'angular-core';
import {TranslateService} from '@ngx-translate/core';
import _ from 'underscore';
import moment from 'moment-timezone';

import {FinanceBaseComponent} from '@finance/finance-base.component';
import {ProjectService} from '@finance/project/services/project.service';
import {ProjectBillService} from '@finance/project/services/project-bill.service';
import {BillProcedureService} from '@finance/project/services/bill-procedure.service';
import {ProjectLineItemService} from '@finance/project/services/project-line-item.service';
import {VOService} from '@finance/project/services/vo.service';
import {DialogConfirmComponent} from '@core';
import {BILL_PROCEDURE_STATUS, BILL_STATUS, TRANSFER_TYPE} from '@resources';
import {TableUtil} from '@app/utils/tableUtils';
import {ExcelService} from '@ext/lezo/services/excel.service';
import {DocCategoryService} from "@finance/doc-category/services/doc-category.service";
import {UserService} from "@finance/user/services/user.service";
import {AccountService} from "@account/services/account.service";
import {FileService} from "@finance/procedure/services/file.service";
import {DialogSettlementItemsUploadFileComponent} from "@finance/project/components/dialog-settlement-items-upload-file.component";

@Component({
	selector		: 'dialog-project-bill',
	templateUrl		: '../templates/dialog-project-bill.pug',
	styleUrls		: [ '../styles/dialog-project-bill.scss' ],
	encapsulation	: ViewEncapsulation.None,
})
export class DialogProjectBillComponent extends FinanceBaseComponent implements OnInit, OnDestroy {
	@ViewChild(MatTable) matTable: MatTable<any>;
	@ViewChild('table0') table0: MatTable<any>;
	public maxDate: Date;
	public totalExtSettlementItems: number = 0;
	public totalVATSettlementItems: number = 0;
	public totalIncludeSettlementItems: number = 0;
	public uploadedFile: any;
	public loaded: boolean;
	public billForm: FormGroup;
	public billStatusForm: FormGroup;
	public billFinanceNote: FormGroup;
	public procedureForm: FormGroup;
	public isSubmitting: boolean;
	public isUploading: boolean;
	public canUploadInvoice: boolean;
	public viewOnly: boolean;
	public remaining: number = 0;
	public remainingReal: number = 0;
	public remainingVATReal: number = 0;
	public selectedTabIndex: number = 0;
	public procedureIndex: number = -1;
	public userProfile: any = {};
	public canCollectMoney: boolean = true;
	public invoice: any = null;
	public selected: boolean = false;
	// inline table data
	public users: any[] = [];
	public doc_Categories: any[] = [];
	public RATIO: Array<number> = _.range( 0, 105, 5 );
	public ratioPercent: number = this.RATIO[ 2 ]; // 10%
	public vatPercent: number = this.RATIO[ 2 ]; // 10%
	public available_bill_total: number = 0;
	public available_set_settlement: boolean = true;
	public BILL_STATUS: any = BILL_STATUS;
	public BILL_PROCEDURE_STATUS: any = BILL_PROCEDURE_STATUS;
	public bill: any = {
		total			: 0,
		total_vat		: 0,
		status			: 0, // Pending
		invoice			: null,
		procedures		: null,
		transfer_type	: TRANSFER_TYPE.CASH,
		total_real		: 0,
		total_vat_real	: 0,
		total_real_change: 0,
		is_settlement	: false
	};
	public valid_duration: number = 0;
	public canEditValues: boolean = true;

	public transferType: Array<any> = [
		{
			id	: TRANSFER_TYPE.CASH,
			name: 'FINANCE.PROJECT.LABELS.CASH',
		},
		{
			id	: TRANSFER_TYPE.BANKING_TRANSFER,
			name: 'FINANCE.PROJECT.LABELS.BANKING_TRANSFER',
		},
		{
			id	: TRANSFER_TYPE.OTHER,
			name: 'GENERAL.LABELS.OTHER',
		},
	];
	public displayedColumns: Array<string> = [
		'no', 'name', 'belong_to', 'assignee', 'type', 'status', 'attachments',
		'deadline', 'comment', 'actions',
	];

	public settlementItemsDataSource: MatTableDataSource<any> = new MatTableDataSource<any>( [] );
	public settlementItemsDisplayedColumns: Array<string> = [
		'no', 'name', 'unit', 'amount', 'price',
		'total', 'note',
	];

	/**
	* @constructor
	* @param {any} data
	* @param {MatDialogRef} dialogRef
	* @param {MatDialog} dialog
	* @param {Injector} injector
	* @param {FormBuilder} fb
	* @param {SnackBarService} snackBarService
	* @param {TranslateService} translateService
	* @param {ProjectService} projectService
	* @param {ProjectBillService} projectBillService
	* @param {ProjectLineItemService} projectLineItemService
	* @param {VOService} voService
	*/
	constructor(
		@Inject( MAT_DIALOG_DATA ) public data: any,
		public dialogRef				: MatDialogRef<DialogProjectBillComponent>,
		public dialog					: MatDialog,
		public injector					: Injector,
		public fb						: FormBuilder,
		public snackBarService			: SnackBarService,
		public translateService			: TranslateService,
		public excelService				: ExcelService,
		public projectService			: ProjectService,
		public projectBillService		: ProjectBillService,
		public billProcedureService		: BillProcedureService,
		public docCategoryService		: DocCategoryService,
		public userService				: UserService,
		public accountService			: AccountService,
		public fileService				: FileService,
		public projectLineItemService	: ProjectLineItemService,
		public voService				: VOService
	) {
		super( injector );
	}

	/**
	* @constructor
	*/
	public ngOnInit() {
		this.initData();
	}

	private initData() {
		this.settlementItemsDataSource.data = [];
		this.bill = this.data;
		const procedures: any[] = this.data.procedures;
		this.data && (this.available_bill_total = this.data.available_bill_total) && (this.available_set_settlement = this.data.available_set_settlement);
		this.canEditValues = (this.bill.id && this.bill.is_settlement === 1) ? false : true;
		if (!this.bill.id) {
			this.bill.total = this.available_bill_total * this.ratioPercent / 100;
			this.updateVAT();
		}
		if (this.bill.id) {
			try {
				const items: any[] = (this.bill.settlement_items) ? JSON.parse(this.bill.settlement_items) : [];
				if (items.length) {
					items.forEach((item: any) => {
						this.totalExtSettlementItems += item.total;
					});
					if (this.bill.settlement_discount_type === '$') {
						this.totalExtSettlementItems -= this.bill.settlement_discount;
					} else {
						this.totalExtSettlementItems -= this.totalExtSettlementItems * this.bill.settlement_discount / 100;
					}
					this.totalVATSettlementItems = this.totalExtSettlementItems * 0.1;
					this.totalIncludeSettlementItems = this.totalExtSettlementItems + this.totalVATSettlementItems;
					this.settlementItemsDataSource.data = items;
				}
			} catch (e) {
			}
		}

		this.maxDate = new Date();

		this.billForm = this.fb.group({
			is_settlement: [
				{ value: (this.bill.id) ? ((this.bill.is_settlement && !this.bill.settlement_items) ? true : false) : false, disabled: this.viewOnly || !this.available_set_settlement }
			],
			name: [
				{ value: null, disabled: !this.canEditValues },
				Validators.compose([
					Validators.required,
					Validators.minLength( 1 ),
					Validators.maxLength( 255 ),
				]),
			],
			total: [
				{ value: this.bill.total, disabled: !this.canEditValues },
				Validators.compose([
					Validators.required,
					Validators.min( 1 ),
				]),
			],
			vat_percent: [
				{ value: null, disabled: !this.canEditValues },
				Validators.compose([
					Validators.min( 0 ),
					Validators.max( 100 ),
				]),
			],
			total_vat: [
				{ value: null, disabled: !this.canEditValues },
				Validators.compose([
					Validators.required,
					Validators.min( 0 ),
				]),
			],
			expected_invoice_date: [
				{ value: null, disabled: !this.canEditValues },
				Validators.compose([
					Validators.required,
				]),
			],
			ratio			: [{ value: null, disabled: !this.canEditValues }],
			subtotal		: [{ value: null, disabled: !this.canEditValues }],
			transfer_type	: [{ value: null, disabled: !this.canEditValues }],
		});

		this.billStatusForm = this.fb.group({
			status: [
				{ value: null, disabled: false },
				Validators.compose([
					Validators.required,
					Validators.pattern(
						new RegExp( _.values( BILL_STATUS ).join( '|' ) )
					),
				]),
			],
			total_real		: [{ value: null, disabled: false }],
			total_vat_real	: [{ value: null, disabled: false }],
			received_date	: [{ value: null, disabled: false }],
			subtotal		: [{ value: null, disabled: false }],
		});

		this.billFinanceNote = this.fb.group({
			finance_note: [{ value: null, disabled: false }],
		});

		this.procedureForm = this.fb.group({
			name: [
				{ value: null, disabled: false },
				Validators.compose([
					Validators.required,
					Validators.minLength( 1 ),
					Validators.maxLength( 255 ),
				]),
			],
			status: [
				{ value: null, disabled: false },
				Validators.compose([
					Validators.required,
					Validators.min( 0 ),
				]),
			],
			deadline: [
				{ value: null, disabled: false },
				Validators.compose([
					Validators.required,
				]),
			],
			note: [{ value: null, disabled: false }],
		});

		this.viewOnly = this.data.status === BILL_STATUS.MONEY_COLLECTED;

		if (!this.bill.received_date) {
			this.bill.received_date = new Date();
		} else {
			this.bill.received_date = new Date(this.bill.received_date);
		}

		if ( this.bill.expected_invoice_date ) {
			const expectedInvoiceDate: any = moment( this.bill.expected_invoice_date );
			this.bill.expected_invoice_date = expectedInvoiceDate.format();
		}

		if (this.bill.id) {
			this.users = [];
			this.dataSource.data = [];
			this.bill.procedures = [];
			this.doc_Categories = [];
			Promise.all([
				this.projectService.getOne( this.bill.project.id, 'project_info' ),
				this.billProcedureService.getAll(this.bill.id),
				this.userService.getUsers(),
				this.docCategoryService.getDocCategories(),
				this.accountService.getProfile()
			]).then(initRes => {
				initRes[0].subscribe((project: any) => {
					this.valid_duration = project.valid_duration || 0;
					initRes[1].subscribe((billProcedures: any[]) => {
						const total = billProcedures.length;
						let run = 0;
						initRes[2].subscribe(usrs => {
							this.users = usrs;
							initRes[3].subscribe(docCategories => {
								this.doc_Categories = docCategories;
								initRes[4].subscribe((userProfile: any) => {
									this.userProfile = userProfile;
									if (billProcedures.length < 1) {
										this.bill.procedures = [];
										this.canCollectMoney = true;
										this.canUploadInvoice = ( this.isLiabilitiesAccountant || this.isCFO )
											&& this.bill.status === BILL_STATUS.PROCESSING;

										this.bill.total_real = this.viewOnly ? this.data.total_real : this.data.total ;
										this.bill.total_vat_real = this.viewOnly ? this.data.total_vat_real : this.data.total_vat ;
									}
									billProcedures.forEach((billProcedure: any) => {
										if (billProcedure.status !== 2) {
											this.canCollectMoney = false;
										}
										billProcedure.bill_procedure_status_name = _.findWhere( this.billProcedureStatus, {
											id: (billProcedure.status === 2) ? billProcedure.status : ((this.datediff(billProcedure.deadline) > 0) ? 0 : 1)
										} );
										const promises = [];
										if (billProcedure.doc_category_id) {
											promises.push(
												this.docCategoryService.getOne(billProcedure.doc_category_id)
											);
										} else {
											promises.push({});
										}
										if (billProcedure.created_by) {
											promises.push(
												this.userService.getUser(billProcedure.created_by)
											);
										} else {
											promises.push({});
										}
										if (billProcedure.assignee) {
											promises.push(
												this.userService.getUser(billProcedure.assignee)
											);
										} else {
											promises.push({});
										}
										Promise.all(promises).then(resData => {
											resData[0].subscribe((docCategory: any) => {
												billProcedure.type = docCategory.name || '';
												resData[1].subscribe((createdBy: any) => {
													billProcedure.created_by = createdBy.full_name || '';
													resData[2].subscribe((assignee: any) => {
														billProcedure.assignee = assignee;
														billProcedure.attachments = procedures.length ? procedures.find((pr: any) => pr.id === billProcedure.id).attachments : [];
														run++;
														if (run === total) {
															this.bill.procedures = billProcedures;
															this.dataSource.data = billProcedures;
															this.canUploadInvoice = ( this.isLiabilitiesAccountant || this.isCFO )
																&& this.bill.status === BILL_STATUS.PROCESSING;

															this.bill.total_real = this.viewOnly ? this.data.total_real : this.data.total ;
															this.bill.total_vat_real = this.viewOnly ? this.data.total_vat_real : this.data.total_vat ;
														}
													});
												});
											});
										});
									});
								});
							});
						});
					});
				});
			});
		} else {
			this.dataSource.data = [];
		}
	}

	public updateSettlementStatus() {
		if (this.bill.is_settlement) {
			// update values automatically
			this.bill.total = this.data.settlement.total;
			this.bill.total_vat = this.data.settlement.total_vat;
			this.ratioPercent = this.available_bill_total ? (this.bill.total * 100 / this.available_bill_total) : 0;
			this.vatPercent = this.bill.total_vat * 100 / this.bill.total;
			this.billForm.controls[ 'total' ].setValue(this.data.settlement.total);
			this.billForm.controls[ 'total_vat' ].setValue(this.data.settlement.total_vat);
			this.billForm.controls[ 'vat_percent' ].setValue(this.vatPercent);
		}
	}

	/**
	* @constructor
	*/
	public ngOnDestroy() {
		super.ngOnDestroy();
	}

	/**
	* Click No button event
	* @return {void}
	*/
	public onNoClick() {
		this.dialogRef.close();
	}

	/**
	* Click No button event
	* @return {void}
	*/
	public closeDiaglog() {
		this.dialogRef.close( true );
	}

	/**
	* Update total
	* @return {void}
	*/
	public updateTotal() {
		if ( !this.remaining ) return;

		// const tempTotal: number = this.totalBill * this.ratioPercent / 100;

		// this.bill.total = tempTotal > this.remaining ? this.remaining : tempTotal;
		this.updateVAT();
	}

	/**
	* Update total
	* @return {void}
	*/
	public updateVAT() {
		if ( !this.bill.total ) {
			this.bill.total_vat = 0;
			return;
		}
		this.bill.total_vat = this.bill.total * this.vatPercent / 100;
	}

	/**
	* Create project bill
	* @return {void}
	*/
	public create() {
		this.isSubmitting = true;

		this.projectBillService
		.create( this.bill )
		.subscribe( ( result: any ) => {
			this.isSubmitting = false;

			if ( !result || !result.status ) {
				if ( result && result.message === 'PROJECT_BILL_OVER' ) {
					this.snackBarService.warn( 'FINANCE.PROJECT.MESSAGES.PROJECT_BILL_OVER', this.bill );
					return;
				}

				if ( result && result.message === 'PROJECT_BILL_VAT_OVER' ) {
					this.snackBarService.warn( 'FINANCE.PROJECT.MESSAGES.PROJECT_BILL_VAT_OVER', this.bill );
					return;
				}

				this.snackBarService.warn( 'FINANCE.PROJECT.MESSAGES.CREATE_PROJECT_BILL_FAIL', this.bill );
				return;
			}

			this.snackBarService.success( 'FINANCE.PROJECT.MESSAGES.CREATE_PROJECT_BILL_SUCCESS', this.bill );

			this.dialogRef.close( true );
		} );
	}

	/**
	* Update project bill
	* @return {void}
	*/
	public update() {
		this.isSubmitting = true;

		this.projectBillService
		.update( this.bill.id, this.bill )
		.subscribe( ( result: any ) => {
			this.isSubmitting = false;

			if ( !result || !result.status ) {
				if ( result && result.message === 'PROJECT_BILL_OVER' ) {
					this.snackBarService.warn( 'FINANCE.PROJECT.MESSAGES.PROJECT_BILL_OVER', this.bill );
					return;
				}

				if ( result && result.message === 'PROJECT_BILL_VAT_OVER' ) {
					this.snackBarService.warn( 'FINANCE.PROJECT.MESSAGES.PROJECT_BILL_VAT_OVER', this.bill );
					return;
				}

				this.snackBarService.warn( 'FINANCE.PROJECT.MESSAGES.UPDATE_PROJECT_BILL_FAIL', this.bill );
				return;
			}

			this.snackBarService.success( 'FINANCE.PROJECT.MESSAGES.UPDATE_PROJECT_BILL_SUCCESS', this.bill );

			this.dialogRef.close( true );
		} );
	}

	/**
	* Update project bill invoice
	* @return {void}
	*/
	public updateInvoice() {
		this.isSubmitting = true;

		this.projectBillService
		.updateInvoice( this.bill.id, this.bill )
		.subscribe( ( result: any ) => {
			this.isSubmitting = false;

			if ( !result || !result.status ) {
				this.snackBarService.warn( 'FINANCE.PROJECT.MESSAGES.UPDATE_PROJECT_BILL_FAIL', this.bill );
				return;
			}

			this.snackBarService.success( 'FINANCE.PROJECT.MESSAGES.UPDATE_PROJECT_BILL_SUCCESS', this.bill );

			this.dialogRef.close( true );
		} );
	}

	/**
	* Update project bill status with confirm
	* @return {void}
	*/
	public updateStatus() {
		// Update status only
		if ( this.bill.status !== BILL_STATUS.MONEY_COLLECTED ) {
			this._updateStatus();
			return;
		}

		// Update status width total real
		const dialogRef: any = this.dialog.open(
			DialogConfirmComponent,
			{
				width: '400px',
				data: {
					title	: this.translateService.instant( 'FINANCE.PROJECT.TITLES.CONFIRM_BILL' ),
					content	: this.translateService.instant( 'FINANCE.PROJECT.MESSAGES.BILL_TOTAL_CONFIRMATION', this.bill ),
					actions: {
						yes: { color: 'warn' },
					},
				},
			}
		);

		dialogRef
		.afterClosed()
		.subscribe( ( _result: any ) => {
			if ( !_result ) return;

			this._updateStatus();
		} );
	}

	/**
	* Update project bill status
	* @return {void}
	*/
	public _updateStatus() {
		this.isSubmitting = true;

		this.projectBillService
		.updateStatus( this.bill.id, this.bill )
		.subscribe( ( result: any ) => {
			this.isSubmitting = false;

			if ( !result || !result.status ) {
				if ( result && result.message === 'PROJECT_BILL_OVER' ) {
					this.snackBarService.warn( 'FINANCE.PROJECT.MESSAGES.PROJECT_BILL_OVER', this.bill );
					return;
				}

				if ( result && result.message === 'PROJECT_BILL_VAT_OVER' ) {
					this.snackBarService.warn( 'FINANCE.PROJECT.MESSAGES.PROJECT_BILL_VAT_OVER', this.bill );
					return;
				}

				this.snackBarService.warn( 'FINANCE.PROJECT.MESSAGES.UPDATE_PROJECT_BILL_FAIL', this.bill );
				return;
			}

			this.snackBarService.success( 'FINANCE.PROJECT.MESSAGES.UPDATE_PROJECT_BILL_SUCCESS', this.bill );

			this.dialogRef.close( true );
		} );
	}

	/**
	* Update project bill finance note
	* @return {void}
	*/
	public updateFinanceNote() {
		this.isSubmitting = true;

		this.projectBillService
		.updateFinanceNote( this.bill.id, this.bill )
		.subscribe( ( result: any ) => {
			this.isSubmitting = false;

			if ( !result || !result.status ) {
				this.snackBarService.warn( 'FINANCE.PROJECT.MESSAGES.UPDATE_PROJECT_BILL_FAIL', this.bill );
				return;
			}

			this.snackBarService.success( 'FINANCE.PROJECT.MESSAGES.UPDATE_PROJECT_BILL_SUCCESS', this.bill );

			this.dialogRef.close( true );
		} );
	}

	/**
	* Bill status change
	* @param {any} event
	* @return {void}
	*/
	public billStatusChange( event: any ) {
		if ( !event || !event.source ) return;

		const totalRealControl: any = this.billStatusForm.get( 'total_real' );
		const totalVATRealControl: any = this.billStatusForm.get( 'total_vat_real' );
		const receivedDateControl: any = this.billStatusForm.get( 'received_date' );

		if ( event.value === BILL_STATUS.MONEY_COLLECTED ) {
			totalRealControl.setValidators([
				Validators.required,
				Validators.min( 0 ),
				Validators.max( this.remainingReal ),
			]);

			totalVATRealControl.setValidators([
				Validators.required,
				Validators.min( 0 ),
				Validators.max( this.remainingVATReal ),
			]);

			receivedDateControl.setValidators([
				Validators.required,
			]);
		} else {
			totalRealControl.setValidators([]);
			totalVATRealControl.setValidators([]);
			receivedDateControl.setValidators([]);
		}

		// totalRealControl.updateValueAndValidity();
		// totalVATRealControl.updateValueAndValidity();
		// receivedDateControl.updateValueAndValidity();
	}

	private exportBillInfo() {
		const title = 'Bill Info';
		const sheetName = (this.data.po_name || 'info').toString().toUpperCase();
		const fileName = `Bill_Info_${TableUtil.slug(this.data.po_name || '')}`;
		let bill_status = '';
		if (this.bill.status === BILL_STATUS.WAITING) {
			bill_status = this.translateService.instant('FINANCE.PROJECT.LABELS.WAITING');
		} else if (this.bill.status === BILL_STATUS.PROCESSING) {
			bill_status = this.translateService.instant('FINANCE.PROJECT.LABELS.PROCESSING');
		} else if (this.bill.status === BILL_STATUS.INVOICE_SENT) {
			bill_status = this.translateService.instant('FINANCE.PROJECT.LABELS.INVOICE_SENT');
		} else {
			bill_status = this.translateService.instant('FINANCE.PROJECT.LABELS.MONEY_COLLECTED');
		}
		const infoData = {
			data: [
				{
					richText: [
						{
							font: { name: 'Tahoma', family: 4, size: 12, bold: true },
							text: this.translateService.instant('GENERAL.LABELS.NAME') + ':\n',
						},
						{
							font: { name: 'Tahoma', family: 4, size: 12 },
							text: this.bill.name || '',
						},
					],
				},
				{
					richText: [
						{
							font: { name: 'Tahoma', family: 4, size: 12, bold: true },
							text: this.translateService.instant('FINANCE.PROJECT.LABELS.EXPECTED_INVOICE_DATE') + ':\n',
						},
						{
							font: { name: 'Tahoma', family: 4, size: 12 },
							text: this.bill.expected_invoice_date ? TableUtil.getDateFormatForExcel(new Date(this.bill.expected_invoice_date)) : '',
						},
					],
				},
				{
					richText: [
						{
							font: { name: 'Tahoma', family: 4, size: 12, bold: true },
							text: this.translateService.instant('FINANCE.PROJECT.LABELS.TOTAL') + ':\n',
						},
						{
							font: { name: 'Tahoma', family: 4, size: 12 },
							text: TableUtil.getNumberFormatForExcel(this.bill.total || 0),
						},
					],
				},
				{
					richText: [
						{
							font: { name: 'Tahoma', family: 4, size: 12, bold: true },
							text: this.translateService.instant('GENERAL.LABELS.VAT') + ':\n',
						},
						{
							font: { name: 'Tahoma', family: 4, size: 12 },
							text: TableUtil.getNumberFormatForExcel(this.bill.total_vat || 0),
						},
					],
				},
				{
					richText: [
						{
							font: { name: 'Tahoma', family: 4, size: 12, bold: true },
							text: this.translateService.instant('FINANCE.PROJECT.LABELS.TRANSFER_TYPE') + ':\n',
						},
						{
							font: { name: 'Tahoma', family: 4, size: 12 },
							text: this.translateService.instant(this.transferType[ this.bill.transfer_type || 0 ].name),
						},
					],
				},
				{
					richText: [
						{
							font: { name: 'Tahoma', family: 4, size: 12, bold: true },
							text: this.translateService.instant('FINANCE.PROJECT.LABELS.SUBTOTAL') + ':\n',
						},
						{
							font: { name: 'Tahoma', family: 4, size: 12 },
							text: TableUtil.getNumberFormatForExcel((this.bill.total || 0) + (this.bill.total_vat || 0)),
						},
					],
				},
				{
					richText: [
						{
							font: { name: 'Tahoma', family: 4, size: 12, bold: true },
							text: this.translateService.instant('GENERAL.LABELS.STATUS').toUpperCase() + ' (' +
								  this.translateService.instant('FINANCE.PROJECT.LABELS.LIABILITIES_ACCOUNTANT_ONLY').toUpperCase() + '):\n',
						},
						{
							font: { name: 'Tahoma', family: 4, size: 12 },
							text: bill_status,
						},
					],
				},
				{
					richText: [],
				},
				{
					richText: [
						{
							font: { name: 'Tahoma', family: 4, size: 12, bold: true },
							text: this.translateService.instant('FINANCE.PROJECT.LABELS.TOTAL') + ':\n',
						},
						{
							font: { name: 'Tahoma', family: 4, size: 12 },
							text: TableUtil.getNumberFormatForExcel(this.bill.total_real || 0),
						},
					],
				},
				{
					richText: [
						{
							font: { name: 'Tahoma', family: 4, size: 12, bold: true },
							text: this.translateService.instant('GENERAL.LABELS.VAT') + ':\n',
						},
						{
							font: { name: 'Tahoma', family: 4, size: 12 },
							text: TableUtil.getNumberFormatForExcel(this.bill.total_vat_real || 0),
						},
					],
				},
				{
					richText: [
						{
							font: { name: 'Tahoma', family: 4, size: 12, bold: true },
							text: this.translateService.instant('FINANCE.PROJECT.LABELS.RECEIVED_DATE') + ':\n',
						},
						{
							font: { name: 'Tahoma', family: 4, size: 12 },
							text: this.bill.received_date ? TableUtil.getDateFormatForExcel(new Date(this.bill.received_date)) : '',
						},
					],
				},
				{
					richText: [
						{
							font: { name: 'Tahoma', family: 4, size: 12, bold: true },
							text: this.translateService.instant('FINANCE.PROJECT.LABELS.SUBTOTAL') + ':\n',
						},
						{
							font: { name: 'Tahoma', family: 4, size: 12 },
							text: TableUtil.getNumberFormatForExcel((this.bill.total_real || 0) + (this.bill.total_vat_real || 0)),
						},
					],
				},
			],
			cols: 2,
		};
		this.excelService.exportNormalViewToExcel(infoData, title, sheetName, fileName);
	}

	public exportProcedure() {
		const headerSetting = {
			header: [
				this.translateService.instant('GENERAL.ATTRIBUTES.NUMBER_ORDER'),
				this.translateService.instant('GENERAL.ATTRIBUTES.NAME'),
				this.translateService.instant('GENERAL.ATTRIBUTES.STATUS'),
				this.translateService.instant('FINANCE.PROJECT.ATTRIBUTES.DEADLINE'),
				this.translateService.instant('FINANCE.PROJECT.ATTRIBUTES.PROOF'),
			],
			noData: this.translateService.instant('FINANCE.SETTINGS.MESSAGES.NO_RECORD_DATA_NAME', { name: 'cost' }),
			fgColor: 'ffffff',
			bgColor: '00245A',
			widths: [ 50, 30, 15, 15, 20, 20, 50 ],
		};
		const title = 'Receivables Procedure';
		const sheetName = (this.data.po_name || 'data').toString().toUpperCase();
		const fileName = `Receivables_Procedure_${TableUtil.slug(this.data.po_name || '')}`;
		const exportData: any[] = [];
		this.dataSource.data.forEach((item: any, i: number) => {
			const dataItem: any[] = [];
			dataItem.push(TableUtil.pad(i + 1, 2));
			dataItem.push(item.name || 'N/A');
			const status = (item.status_name && item.status_name.name) ? { value: this.translateService.instant(item.status_name.name), fgColor: item.status_name.color ? item.status_name.color.replace('#', '') : 'FF0000' } : { value: '', fgColor: 'FF0000' };
			dataItem.push(status);
			dataItem.push((item.deadline) ? TableUtil.getDateFormatForExcel(new Date(item.deadline)) : '');
			dataItem.push((item.proof && item.proof.key) ? item.proof.key : '');
			exportData.push(dataItem);
		});
		this.excelService.exportArrayToExcel(
			exportData,
			title,
			headerSetting,
			sheetName,
			fileName
		);
	}

	public exportExcel() {
		if (this.selectedTabIndex === 0) {
			this.exportBillInfo();
		} else if (this.selectedTabIndex === 1) {
			this.exportProcedure();
		}

		this.dialogRef.close('');
	}

	public openDialog(_billProcedure: any) {
	}

	public delete(index: number) {
		this.dataSource.data.splice(index, 1);
		this.matTable.renderRows();
	}

	public deleteReal(id: number, name: string) {
		this.billProcedureService.delete(id).subscribe( _res => {
			this.snackBarService.success( 'GENERAL.MESSAGES.DELETE_SUCCESS', { name: 'bill procedure \"' +  name + '\"'});
			this.initData();
		});
	}

	public newBillProcedure() {
		this.dataSource.data.push({
			name: null,
			assignee: {full_name: '-1'},
			type: -1,
			deadline: this.deadlineDay(),
			attachments: null,
			belong_to: '',
			is_settlement: null,
			is_new: true
		});
		this.matTable.renderRows();
	}

	public deadlineDay() {
		return moment().add(this.valid_duration, 'd').format('YYYY-MM-DD');
	}

	public canSubmit() {
		if (this.dataSource.data.length < 1) {
			return false;
		}
		let check: boolean = (this.dataSource.data.filter(it => !it.id).length < 1) ? false : true;
		// check form validation
		this.dataSource.data.forEach(data => {
			if (+data.type === -1 || +data.assignee === -1 || !data.name || !data.deadline) {
				check = false;
				return;
			}
		});
		// check unique names
		const uniqueNames = new Set(this.dataSource.data.map(dt => dt && dt.name && dt.name.toLowerCase()));
		if (uniqueNames.size < this.dataSource.data.length) {
			return false;
		}
		return check;
	}

	public checkName(item: any, ind: number) {
		if (item && item.name) {
			if (item.name.trim()) {
				if (this.dataSource.data.length < 1) {
					return true;
				}
				const namesList: string[] = this.dataSource.data.map((dt, i) => dt && dt.name && i !== ind && dt.name.toLowerCase());
				if ((namesList.length > 0 && namesList.indexOf(item.name.toLowerCase()) === -1) || namesList.length < 1) {
					return true;
				}
			}
		}
		return false;
	}

	public submitBillProcedures() {
		const dataSend: any[] = [];
		this.dataSource.data.filter(it => !it.id).forEach(data => {
			dataSend.push({
				created_by: this.userProfile.id,
				doc_category_id: +data.type,
				assignee: +data.assignee,
				belong_to: data.belong_to,
				name: data.name,
				bill_id: this.bill.id,
				deadline: data.deadline
			});
		});
		this.billProcedureService.createBatch(dataSend).subscribe( res => {
			if ( !res || !res.status ) {
				this.snackBarService.warn( 'GENERAL.MESSAGES.CREATES_FAILED', { name: 'bill procedures' } );
				return;
			}

			this.snackBarService.success( 'GENERAL.MESSAGES.CREATES_SUCCESS', { name: 'bill procedures' });
			this.dialogRef.close('');
		});
	}

	public getBelongList(index: number) {
		const result: string[] = [];
		const avaibleList: any[] = [];
		this.dataSource.data.forEach((data, ind) => {
			if (ind !== index && data.name && data.name.trim() ) {
				if (!data.belong_to || (data.belong_to && !data.belong_to.trim()) ||
					(data.belong_to && data.belong_to.trim() && this.dataSource.data[index].name &&
						data.belong_to.toLowerCase() !== this.dataSource.data[index].name.toLowerCase())) {
					if (!data.belong_to || (data.belong_to && !data.belong_to.trim())) {
						avaibleList.push(data);
					} else {
						const findInd = this.dataSource.data.findIndex(d => d.name && d.name.toLowerCase() === data.belong_to.toLowerCase());
						if (findInd !== -1) {
							const temp = this.dataSource.data[findInd];
							if (!temp.belong_to || (temp.belong_to && !temp.belong_to.trim())) {
								avaibleList.push(data);
							} else {
								if (temp.belong_to.toLowerCase() !== this.dataSource.data[index].name.toLowerCase()) {
									avaibleList.push(data);
								}
							}
						} else {
							avaibleList.push(data);
						}
					}
				}
			}
		});
		avaibleList.forEach(item => {
			if (result.length < 1 || (result.length && result.map((rs: string) => rs.toLowerCase()).indexOf(item.name.toLowerCase()) === -1)) {
				if (item.name && item.name.trim()) {
					if (!this.dataSource.data[index].name || (this.dataSource.data[index].name && !this.dataSource.data[index].name.trim())) {
						if (this.dataSource.data.length && this.dataSource.data.filter(s => s.name && s.name.toLowerCase() === item.name.toLowerCase()).length < 2) {
							result.push(item.name);
						}
					} else {
						if (item.name.toLowerCase() !== this.dataSource.data[index].name.toLowerCase()) {
							if (this.dataSource.data.length && this.dataSource.data.filter(s => s.name && s.name.toLowerCase() === item.name.toLowerCase()).length < 2) {
								result.push(item.name);
							}
						}
					}
				}
			}
		});
		if (result.length) {
			result.sort();
		}
		if (result.length && !this.dataSource.data[index].belong_to) {
			this.dataSource.data[index].belong_to = '';
		}
		return result;
	}

	public datediff(d2: Date) {
		const d1 = new Date();
		return TableUtil.datediff(d1, new Date(d2));
	}

	public datediff2(item: any) {
		const d2 = new Date(item.deadline);
		const d1 = new Date(item.updated_at);
		return TableUtil.datediff(d1, d2);
	}

	/**
	 * Handle upload invoice file
	 * @param {any} event
	 * @return {void}
	 */
	public onFileSelected( event: any ) {
		this.isUploading = true;
		this.invoice = null;

		const reader = new FileReader();
		const filename: string = event.file.name;
		const fileTypeExtension: string = (filename && filename.indexOf('.') !== -1) ? filename.substr(filename.lastIndexOf('.') + 1) : '';
		const modFileName: string = (filename && filename.indexOf('.') !== -1)
			? (filename.substr(0, filename.lastIndexOf('.'))  + '.' + fileTypeExtension)
			: filename;
		const file = event.file;
		reader.readAsDataURL(file);

		reader.onload = () => {
			this.upload(event, reader.result, fileTypeExtension, modFileName);
		};
	}

	private upload(event, file, fileTypeExtension, filename): void {
		const fileRealName: string = this.getNewFileName() + (fileTypeExtension ? ('.' + fileTypeExtension) : '');
		this.fileService.uploadInvoice(filename, fileRealName, file).subscribe((attachment: any) => {
			event.input.nativeElement.value = null;
			if ( !attachment ) {
				this.snackBarService.warn( 'FINANCE.PROJECT.MESSAGES.UPLOAD_INVOICE_FAIL' );
				return;
			}
			this.isSubmitting = false;
			this.isUploading = false;
			this.bill.invoice = attachment;
			this.invoice = attachment;
		});
	}

	public download(fileRealName: string):  void {
		this.fileService.downloadInvoiceFile(fileRealName);
	}

	private getFilePrefix(): string[] {
		return [
			'P' + TableUtil.pad(this.bill.project.id, 5),
			'B' + TableUtil.pad(this.bill.id, 5),
			'Invoice',
		];
	}

	public getNewFileName(): string {
		const listNames: string[] = this.getFilePrefix();
		return listNames.join('_');
	}

	/**
	 * Download template
	 * @return {void}
	 */
	public downloadSettlement() {
		this.projectService
			.downloadSettlement()
			.subscribe( ( result: any ) => {
				if ( !result || !result.status ) {
					this.snackBarService.warn( 'FINANCE.PROJECT.MESSAGES.DOWNLOAD_TEMPLATE_FAIL' );
					return;
				}

				const win: any = window.open( result.data, '_blank' );
				win.focus();
			} );
	}

	/**
	 * Open dialog import file
	 * @return {void}
	 */
	public openDialogUploadFile() {
		const bill: any = this.bill;
		const dialogRef: any = this.dialog.open(
			DialogSettlementItemsUploadFileComponent,
			{
				width: '400px',
				data: {
					bill
				}
			}
		);

		dialogRef
			.afterClosed()
			.subscribe( ( result: any ) => {
				if ( !result || !result.file_location ) return;
				this.uploadSettlementItems( result.file_location, result.discount, result.discount_type );
			} );
	}

	public uploadSettlementItems( fileLocation: File, discount: number, discount_type: string ) {
		this.isUploading = true;
		this.loaded = false;
		this.projectBillService
			.updateSettlementItems( fileLocation, this.bill.id, { discount, discount_type } )
			.subscribe( ( result: any ) => {
				this.isUploading = false;
				this.loaded = true;

				if ( !result || !result.status ) {
					this.uploadedFile = null;

					if ( result && result.message === 'INVALID_FILE_TYPE' ) {
						this.snackBarService.warning( 'FORM_ERROR_MESSAGES.INVALID_FILE_TYPE' );
						return;
					}

					if ( result && result.message === 'INVALID_FILE_SIZE' ) {
						this.snackBarService.warning( 'FORM_ERROR_MESSAGES.INVALID_FILE_SIZE' );
						return;
					}

					this.snackBarService.warn( 'FINANCE.PROJECT.MESSAGES.UPLOAD_BILL_SETTLEMENT_ITEMS_FAIL' );
					return;
				}

				this.snackBarService.success( 'FINANCE.PROJECT.MESSAGES.UPLOAD_BILL_SETTLEMENT_ITEMS_SUCCESS', result.data );
				this.dialogRef.close( true );
			} );
	}

	public hasSettlementItems() {
		if (this.bill.id) {
			return false;
		}
		try {
			const items: any[] = (this.bill.settlement_items) ? JSON.parse(this.bill.settlement_items) : [];
			if (items.length) {
				return true;
			}
		} catch (e) {
			return false;
		}
	}

	public downloadAttachment(fileRealName: string):  void {
		this.fileService.download(fileRealName);
	}
}
