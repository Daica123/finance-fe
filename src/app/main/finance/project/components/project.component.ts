import {Component, Injector, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog} from '@angular/material';
import {SnackBarService} from 'angular-core';
import {TranslateService} from '@ngx-translate/core';
import _ from 'underscore';
import moment from 'moment-timezone';

import {ProjectBaseComponent} from '@finance/project/project-base.component';
import {DialogCreateProjectComponent} from './dialog-create-project.component';
import {DialogProjectComponent} from './dialog-project.component';
import {Direction} from '@finance/finance-direction.component';
import {ProjectService} from '@finance/project/services/project.service';
import {DialogConfirmComponent} from '@core';
import {MatSort} from '@angular/material/sort';
import {ExcelService} from '@ext/lezo/services/excel.service';
import {TableUtil} from '@app/utils/tableUtils';
import {ProjectPurchaseOrderService} from "@finance/project/services/project-purchase-order.service";
import {ProjectCostItemService} from "@finance/project/services/project-cost-item.service";
import {SelectionModel} from '@angular/cdk/collections';
import {AppState} from "@app/app-state.service";
import {ProjectBillService} from "@finance/project/services/project-bill.service";

@Component({
	selector	: 'project',
	templateUrl	: '../templates/project.pug',
	styleUrls	: [ '../styles/project.scss' ],
	encapsulation	: ViewEncapsulation.None,
})
@Direction({
	path	: 'project',
	data	: { title: 'FINANCE.DIRECTION.QUOTATION_MANAGEMENT', icon: 'icon icon-quotation' },
	priority: 90,
	roles: [
		'CEO', 'CFO', 'GENERAL_ACCOUNTANT',
		'LIABILITIES_ACCOUNTANT', 'PROCUREMENT_MANAGER', 'CONSTRUCTION_MANAGER',
		'PM', 'SALE', 'QS',
		'PURCHASING', 'CONSTRUCTION',
	],
})
export class ProjectComponent extends ProjectBaseComponent implements OnInit, OnDestroy {
	@ViewChild(MatSort) public sort: MatSort;
	public projects: any[];
	public projectsInfo: any = {
		clients: [],
		managers: []
	};
	public loading: boolean = true;
	public selection = new SelectionModel<any>(true, []);
	public canAdd: boolean;
	public isDeleting: boolean;
	public isHideQuotation: boolean;
	public displayedColumns: Array<string> = [
		'select', 'project_code', 'project', 'client',
		'aic', 'receivable', 'payable',
		'quotation_status', 'project_status', 'actions',
	];
	public firstLoad: boolean = true;

	public length = 0;
	public pageSize = 10;
	public pageIndex = 0;
	public pageSizeOptions: number[] = [5, 10, 20, 30, 50, 100];

	public footerRow: any = {};
	public queryOptions: any = {
		begin	: (this.isCFO || this.isGeneralAccountant) ? moment().clone().subtract( 1, 'year' ).startOf( 'year' ) : moment().clone().startOf( 'year' ),
		end		: moment().clone().endOf( 'day' ),
		limit	: 10,
		offset	: 0,
		search 	: {
			project_code: '',
			project_name: '',
		}
	};

	/**
	 * @constructor
	 * @param {Injector} injector
	 * @param {MatDialog} dialog
	 * @param {SnackBarService} snackBarService
	 * @param {TranslateService} translateService
	 * @param {ProjectService} projectService
	 */
	constructor(
		public injector						: Injector,
		public dialog						: MatDialog,
		public excelService     			: ExcelService,
		public snackBarService				: SnackBarService,
		public translateService				: TranslateService,
		public appState						: AppState,
		public projectPurchaseOrderService	: ProjectPurchaseOrderService,
		public projectService				: ProjectService,
		public projectBillService			: ProjectBillService,
		public projectCostItemService		: ProjectCostItemService
	) {
		super( injector );

		moment.locale(this.appState.locale.code);
		this.translateService.onLangChange.subscribe(
			() => {
				moment.locale(this.appState.locale.code);
			}
		);

		this.canAdd = this.isCEO || this.isSale;

		// Hide Quotation and Received
		this.isHideQuotation = this.isGeneralAccountant || this.isLiabilitiesAccountant || this.isPurchasing
			|| this.isProcurementManager || this.isConstructionManager || this.isConstruction;
		this.isHideQuotation && ( this.displayedColumns.splice( 4, 2 ) );
		localStorage.removeItem('sub_tab');
	}

	/**
	 * @constructor
	 */
	public ngOnInit() {
		this.dataSource.sortingDataAccessor = (data: any, sortHeaderId: string) => {
			const value: any = data[ sortHeaderId ] || '';
			return typeof value === 'string' ? value.toLowerCase() : value;
		};
		this.initData();
		this.dataSource.sort = this.sort;
		this.selection.onChange.subscribe( () => {
			// do later
		});
	}

	/**
	 * @constructor
	 */
	public ngOnDestroy() {
		super.ngOnDestroy();
	}

	/**
	 * Init all data
	 * @return {void}
	 */
	public initData() {
		this.firstLoad = true;
		this.projectService
			.getAllPaginatorInfo('', this.queryOptions.begin, this.queryOptions.end).subscribe( result => {
			this.projectsInfo = result;
			this.getList();
		});
	}

	public seachProjectCode(value) {
		this.queryOptions.search.project_code = value;
		this.customFilter();
	}

	public seachProjectName(value) {
		this.queryOptions.search.project_name = value;
		this.customFilter();
	}

	/**
	 * Get projects
	 * @return {void}
	 */
	public getList(update?: boolean) {
		this.selection.clear();
		this.setProcessing( true );
		this.loaded = false;

		this.projectService
			.getAllPaginator('', this.queryOptions.begin, this.queryOptions.end, this.queryOptions.limit, this.queryOptions.offset, this.filters, this.queryOptions.search)
			.subscribe( ( result: any ) => {
				this.setProcessing( false );
				result.projects = _.map( result.projects, ( item: any ) => {
					item.project_status_name = _.findWhere( this.projectStatus, { id: item.project_status || 0 } );
					item.quotation_status_name = _.findWhere( this.quotationStatus, { id: item.quotation_status || 0 } );
					item.payable = item.payable / 1000000;
					item.payable_plan = item.payable_plan / 1000000;
					item.receivable = item.receivable / 1000000;
					item.receivable_plan = item.receivable_plan / 1000000;
					return item;
				} );

				this.loaded = true;
				this.projects = result.projects;
				if (this.queryOptions.offset === 0) {
					this.length = result.total;
				}
				this.dataSourceClone = this.projects;
				this.dataSource.data = this.projects;
				this.dataSource.paginator = this.paginator;
				if (update) {
					this.dataSource._updateChangeSubscription();
				}
				this.dataSource.sort = this.sort;
				if (this.queryOptions.offset === 0) {
					this.pageIndex = 0;
					this.footerRow.total_payable = result.payable / 1000000;
					this.footerRow.total_receivable = result.receivable / 1000000;
					this.footerRow.total_payable_plan = result.payable_plan / 1000000;
					this.footerRow.total_receivable_plan = result.receivable_plan / 1000000;
				}
				this.masterToggle();
				this.firstLoad = false;
			} );
	}

	public pageChanged(event){
		this.queryOptions.offset = event.pageIndex;
		this.pageIndex  = event.pageIndex;
		this.queryOptions.limit = event.pageSize;
		this.pageSize = event.pageSize;
		this.getList(true);
	}

	/** Whether the number of selected elements matches the total number of rows. */
	public isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	public masterToggle() {
		this.isAllSelected() ?
			this.selection.clear() :
			this.dataSource.data.forEach(row => this.selection.select(row));
	}

	public calcualateRealAndPlan(result: any, project: any) {
		const customDatasets: any= {
			income: 0,
			cost: 0,
			total_line: 0,
			total_cost: 0,
		};
		const sheet: any = {income: 0, cost: 0, income_plan: 0, cost_plan: 0};
		const finalTotalLine: number = (result.total_line - (project.discount_type === '$'
			? +project.discount_amount
			: +result.total_line * project.discount_amount / 100)) * 1.1 + result.total_vo_quotation + result.total_vo_vat;
		const finalTotalCost: number = result.total_cost_has_po + result.total_cost_no_po * 1.1;
		let totalRevenue: number = 0;

		_.each(result.income, (item: any) => {
			sheet.income += item.final_total + item.final_total_vat;
		});

		_.each(result.cost, (item: any) => {
			sheet.cost += item.final_total + item.final_total_vat;
		});

		_.each(result.income_plan, (item: any) => {
			sheet.income_plan += (item.target_percent || 0) * (finalTotalLine || 0) / 100;
		});

		_.each(result.cost_plan, (item: any) => {
			sheet.cost_plan += (item.target_percent || 0) * (finalTotalCost || 0) / 100;
		});

		totalRevenue += (sheet.income || 0) - (sheet.cost || 0);
		sheet.revenue = _.clone(totalRevenue);
		customDatasets.total_income = finalTotalLine;
		customDatasets.income += sheet.income || 0;
		customDatasets.cost += sheet.cost || 0;

		return customDatasets;
	}

	/**
	 * Open dialog project to create/update
	 * @param {any} project - Project data need create/update
	 * @return {void}
	 */
	public openDialog( project?: any ) {
		const dialogComponent: any = project
			? DialogProjectComponent
			: DialogCreateProjectComponent;

		const dialogRef: any = this.dialog.open(
			dialogComponent,
			{
				width	: '800px',
				data	: project || null,
			}
		);

		dialogRef
			.afterClosed()
			.subscribe( ( result: any ) => {
				if ( !result ) return;

				this.getList();
			} );
	}

	/**
	 * Delete project
	 * @param {any} project - Project data need delete
	 * @return {void}
	 */
	public delete( project: any ) {
		const dialogRef: any = this.dialog.open(
			DialogConfirmComponent,
			{
				width: '400px',
				data: {
					title	: this.translateService.instant( 'FINANCE.PROJECT.TITLES.DELETE_PROJECT' ),
					content	: this.translateService.instant( 'FINANCE.PROJECT.MESSAGES.DELETE_PROJECT_CONFIRMATION', project ),
					actions: {
						yes: { color: 'warn' },
					},
				},
			}
		);

		dialogRef
			.afterClosed()
			.subscribe( ( _result: any ) => {
				if ( !_result ) return;

				this.isDeleting = true;
				this.setProcessing( true );

				this.projectService
					.delete( project.id )
					.subscribe( ( result: any ) => {
						this.isDeleting = false;
						this.setProcessing( false );

						if ( !result || !result.status ) {
							this.snackBarService.warn( 'FINANCE.PROJECT.MESSAGES.DELETE_PROJECT_FAIL', project );
							return;
						}

						this.snackBarService.success( 'FINANCE.PROJECT.MESSAGES.DELETE_PROJECT_SUCCESS', project );

						this.getList();
					} );
			} );
	}

	/**
	 * Format filter
	 * @return {string}
	 */
	public get format(): string {
		if ( !this.queryOptions ) return;

		return moment( this.queryOptions.begin ).format( 'DD/MM/YYYY' )
			+ ' - '
			+ moment( this.queryOptions.end ).format( 'DD/MM/YYYY' );
	}

	/**
	 * Quotation range change
	 * @param {any} event
	 * @return {void}
	 */
	public quotationRangeChange( event: any ) {
		if ( !event || !event.value ) return;

		this.getList();
	}

	/**
	 * Custom filter
	 * @return {void}
	 */
	public customFilter() {
		this.queryOptions.offset = 0;
		this.queryOptions.limit =this.pageSize;
		if (!this.firstLoad) {
			this.getList(true);
		}
	}

	public exportExcel() {
		const headerSetting = {
			header: [ 'Project Code', 'Project', 'Client', 'Project Manager', 'Received (M)', 'Payable (M)', 'Quotation Status', 'Project Status' ],
			noData: this.translateService.instant('FINANCE.SETTINGS.MESSAGES.NO_RECORD_DATA_NAME', { name: 'quotation' }),
			fgColor: 'ffffff',
			bgColor: '00245A',
		};
		const title = 'Quotation Management List';
		const sheetName = 'Quotations';
		const fileName = 'Quotation_Management_List';
		const exportData: any[] = [];
		const temp: any[] = this.selection.selected.length ? this.dataSource.filteredData.filter(it => this.selection.selected.map(t => t.id).indexOf(it.id) !== -1) : [];
		temp.forEach((item: any) => {
			const dataItem: any[] = [];
			const quotation_status = (item.quotation_status_name && item.quotation_status_name.name) ? { value: this.translateService.instant(item.quotation_status_name.name), fgColor: item.quotation_status_name.color ? item.quotation_status_name.color.replace('#', '') : 'FF0000' } : { value: '', fgColor: 'FF0000' };
			const project_status = (item.project_status_name && item.project_status_name.name) ? { value: this.translateService.instant(item.project_status_name.name), fgColor: item.project_status_name.color ? item.project_status_name.color.replace('#', '') : 'FF0000' } : { value: '', fgColor: 'FF0000' };
			dataItem.push(item.project_code || 'N/A');
			dataItem.push(item.name || 'N/A');
			dataItem.push(item.client_name || 'N/A');
			dataItem.push(item.project_manager_name || 'N/A');
			dataItem.push((item.receivable || item.receivable_plan) ? (TableUtil.getNumberFormatForExcel(item.receivable) + ' / ' + TableUtil.getNumberFormatForExcel(item.receivable_plan)) : '');
			dataItem.push((item.payable || item.payable_plan) ? (TableUtil.getNumberFormatForExcel(item.payable) + ' / ' + TableUtil.getNumberFormatForExcel(item.payable_plan)) : '');
			dataItem.push(quotation_status);
			dataItem.push(project_status);
			exportData.push(dataItem);
		});
		const extraData = [
			{ title: 'Receivable', value: TableUtil.getNumberFormatForExcel(this.footerRow.total_receivable), fgColors: [ '38AE00', '38AE00' ] },
			{ title: 'Receivable Plan', value: TableUtil.getNumberFormatForExcel(this.footerRow.total_receivable_plan), fgColors: [ '38AE00', '38AE00' ] },
			{ title: 'Payable', value: TableUtil.getNumberFormatForExcel(this.footerRow.total_payable), fgColors: [ 'FD8631', 'FD8631' ] },
			{ title: 'Payable Plan', value: TableUtil.getNumberFormatForExcel(this.footerRow.total_payable_plan), fgColors: [ 'FD8631', 'FD8631' ] },
		];
		this.excelService.exportArrayToExcel(
			exportData,
			title,
			headerSetting,
			sheetName,
			fileName,
			extraData
		);
	}
}
