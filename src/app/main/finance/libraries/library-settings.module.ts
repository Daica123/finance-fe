import { NgModule } from '@angular/core';

import { CoreModule } from '@core/core.module';
import { LezoModule } from '@ext/lezo/lezo.module';

/* Component Import (Do not remove) */
import { LibrarySettingsComponent } from './components/library-settings.component';
/* End Component Import (Do not remove) */

/* Service Import (Do not remove) */
import { LibrariesService } from './services/libraries.service';
import { CompanyLibrariesComponent } from './components/company-libraries.component';
import {ProjectLibrariesComponent} from "@finance/libraries/components/project-libraries.component";
/* End Service Import (Do not remove) */

@NgModule({
	imports: [ CoreModule, LezoModule ],
	declarations: [
		/* Component Inject (Do not remove) */
		LibrarySettingsComponent, CompanyLibrariesComponent, ProjectLibrariesComponent,
		/* End Component Inject (Do not remove) */
	],
	entryComponents: [
	],
	providers: [
		/* Service Inject (Do not remove) */
		LibrariesService,
		/* End Service Inject (Do not remove) */
	],
})
export class LibrarySettingsModule {

	/**
	* @constructor
	*/
	constructor() {}

}
