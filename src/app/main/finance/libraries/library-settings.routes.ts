import { Routes } from '@angular/router';
import { LibrarySettingsComponent } from './components/library-settings.component';
import { AuthGrantService } from '@auth/services/auth-grant.service';
import { AuthRoleService } from '@auth/services/auth-role.service';
import { CompanyLibrariesComponent } from './components/company-libraries.component';
import {ProjectLibrariesComponent} from "@finance/libraries/components/project-libraries.component";

export const librarySettingsRoutes: Routes = [
	{
		path		: 'libraries',
		component	: LibrarySettingsComponent,
		canActivate	: [ AuthGrantService, AuthRoleService ],
		data: {
			title		: 'Libraries',
			translate	: 'FINANCE.DIRECTION.LIBRARY',
			roles		: [
				'CEO', 'CFO', 'GENERAL_ACCOUNTANT',
				'LIABILITIES_ACCOUNTANT', 'PROCUREMENT_MANAGER', 'CONSTRUCTION_MANAGER',
				'PM', 'SALE',
				'QS', 'PURCHASING'
			]
		},
		children: [
			{ path: '', redirectTo: 'project-libraries', pathMatch: 'full' },
			{
				path		: 'company-libraries',
				component	: CompanyLibrariesComponent,
				data: {
					title		: 'Company Libraries',
					translate	: 'FINANCE.LIBRARY.LABELS.COMPANY',
				}
			},
			{
				path		: 'project-libraries',
				component	: ProjectLibrariesComponent,
				data: {
					title		: 'Project Libraries',
					translate	: 'FINANCE.LIBRARY.LABELS.PROJECT',
				}
			}
		]
	}
];
