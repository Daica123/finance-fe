import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {TranslateService} from '@ngx-translate/core';
import {FinanceBaseComponent} from '@finance/finance-base.component';
import {DialogConfirmComponent, SnackBarService} from '@core';
import {Direction} from '@finance/finance-direction.component';
import { LibrariesService } from '../services/libraries.service';

@Component({
	selector	: 'library-settings',
	templateUrl	: '../templates/library-settings.pug',
	styleUrls	: [ '../styles/library-settings.scss' ],
})
@Direction({
	path	: 'libraries',
	data	: { title: 'FINANCE.DIRECTION.LIBRARY', icon: 'icon icon-library' },
	priority: 0,
	roles: [
		'CEO', 'CFO', 'GENERAL_ACCOUNTANT',
		'LIABILITIES_ACCOUNTANT', 'PROCUREMENT_MANAGER', 'CONSTRUCTION_MANAGER',
		'PM', 'SALE',
		'QS', 'PURCHASING',
	]
})
export class LibrarySettingsComponent extends FinanceBaseComponent implements OnInit, OnDestroy {
	constructor(
		public injector			: Injector,
		public dialog			: MatDialog,
		public libraryService	: LibrariesService,
		public snackBarService	: SnackBarService,
		public translateService	: TranslateService
	) {
		super( injector );
	}

	/**
	* @constructor
	*/
	public ngOnInit() {
		this.initData();
	}

	/**
	* @constructor
	*/
	public ngOnDestroy() {
		super.ngOnDestroy();
	}

	/**
	* Init all data
	* @return {void}
	*/
	public initData() {
		this.getList();
	}

	/**
	* Get user list
	* @return {void}
	*/
	public getList() {

	}

	public delete( libraryItem: any ) {
		const dialogRef: any = this.dialog.open(
			DialogConfirmComponent,
			{
				width: '400px',
				data: {
					title	: this.translateService.instant( 'FINANCE.LIBRARY.TITLES.DELETE_LIBRARY_ITEM' ),
					content	: this.translateService.instant( 'FINANCE.LIBRARY.MESSAGES.DELETE_LIBRARY_ITEM_CONFIRMATION', libraryItem ),
					actions: {
						yes: { color: 'warn' },
					},
				},
			}
		);

		dialogRef
		.afterClosed()
		.subscribe( ( _result: any ) => {
			if ( !_result ) return;
		} );
	}

}
