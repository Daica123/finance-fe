import {Component, ElementRef, Injector, OnDestroy, OnInit} from '@angular/core';
import {SnackBarService} from 'angular-core';

import {FormBuilder} from '@angular/forms';
import {FinanceBaseComponent} from '@finance/finance-base.component';
import {FileService} from "@finance/procedure/services/file.service";
import {TableUtil} from "@app/utils/tableUtils";

@Component({
	selector	: 'project-libraries',
	templateUrl	: '../templates/project-libraries.pug',
	styleUrls	: [ '../styles/project-libraries.scss' ],
})
export class ProjectLibrariesComponent extends FinanceBaseComponent implements OnInit, OnDestroy {
	public files: any[] = [];
	public directories: any[] = [];
	public projectsCollapse: boolean[] = [];
	public projectsContent: any[] = [];
	public bills: any[] = [];
	public payables: any[] = [];
	public search: string = '';

	/**
	* @constructor
	* @param {Injector} injector
	* @param {FormBuilder} fb
	* @param {ElementRef} elementRef
	* @param {SnackBarService} snackBarService
	* @param {SettingService} settingService
	*/
	constructor(
		public injector			: Injector,
		public fb				: FormBuilder,
		public elementRef		: ElementRef,
		public snackBarService	: SnackBarService,
		public fileService		: FileService
	) {
		super( injector );
	}

	/**
	* @constructor
	*/
	public ngOnInit() {
		this.initData();
	}

	/**
	* @constructor
	*/
	public ngOnDestroy() {
		super.ngOnDestroy();
	}

	/**
	* Init all data
	* @return {void}
	*/
	public initData() {
		this.getAll();
	}

	public getAll() {
		Promise.all([
			this.fileService.getLibraryFiles(2, this.search),
			this.fileService.getLibraryFiles(1, this.search)
		]).then(res => {
			res[0].subscribe( billsData => {
				this.files = billsData.files;
				this.directories = billsData.seperate;
				res[1].subscribe( payablesData => {
					this.files = this.files.concat(payablesData.files);
					this.directories = this.directories.concat(payablesData.seperate);
				});
				const projects: any[] = this.itemsList('project');
				this.projectsCollapse = [];
				this.projectsContent = [];
				this.bills = [];
				this.payables = [];

				projects.forEach((project: any) => {
					this.projectsCollapse.push(true);
					const billItems: any[] = this.directories.filter(dr => dr.project.id === project.id).map(dr => dr.bill).filter(dr => dr);
					const payableItems: any[]  = this.directories.filter(dr => dr.project.id === project.id).map(dr => dr.payable).filter(dr => dr);
					if (billItems.length) {
						this.projectsContent.push({
							project: project.id,
							name: 'Bills',
							collapse: true
						});
						const billItem = {
							project: project.id,
							bills: []
						};
						billItems.forEach((bill: any) => {
							billItem.bills.push({
								...bill,
								collapse: true
							});
						});
						this.bills.push(billItem);
					}
					if (payableItems.length) {
						this.projectsContent.push({
							project: project.id,
							name: 'Payables',
							collapse: true
						});
						const payableItem = {
							project: project.id,
							payables: []
						};
						payableItems.forEach((payable: any) => {
							payableItem.payables.push({
								...payable,
								collapse: true
							});
						});
						this.payables.push(payableItem);
					}
				});
			});
		});
	}

	public itemsList(type: string) {
		const items: any[] = (this.directories && this.directories.length) ? this.directories.map(dr => dr[type]).filter(dr => dr) : [];
		const result: any[] = [];
		items.forEach((item: any) => {
			if (result.length < 1 || (result.length > 0 && result.map(r => r.id).indexOf(item.id) === -1)) {
				result.push(item);
			}
		});
		return result;
	}

	public getProjectInfo(item: any) {
		let result = 'Id: ' + item.id + '\n';
		result += 'Name: ' + item.name + '\n';
		let numOfItems = 0;
		if (this.directories.map(d => d.bill).filter(d => d).length > 0) {
			numOfItems++;
		}
		if (this.directories.map(d => d.payable).filter(d => d).length > 0) {
			numOfItems++;
		}
		result += 'Content: ' + numOfItems + ' item' + (numOfItems > 1 ? 's' : '');
		return result;
	}

	public getProjectContentInfo(item: any) {
		let result = 'Name: ' + item.name + '\n';
		let numOfItems = 0;
		if (item.name === 'Bills') {
			numOfItems = this.directories.map(d => d.bill).filter(d => d).length;
		} else {
			numOfItems = this.directories.map(d => d.payable).filter(d => d).length;
		}
		result += 'Content: ' + numOfItems + ' ' + ((item.name === 'Bills') ? 'bill' : 'payable') + (numOfItems > 1 ? 's' : '');
		return result;
	}

	public getBillInfo(projectId: number, item: any) {
		let result = 'Id: ' + item.id + '\n';
		result += 'Name: ' + item.name + '\n';
		const pattern = 'P' + TableUtil.pad(projectId, 5) + '_' + 'B' + TableUtil.pad(item.id, 5) + '_';
		const numOfItems = this.files.filter(file => file.real_name.startsWith(pattern)).length;
		result += 'Content: ' + numOfItems + ' file' + (numOfItems > 1 ? 's' : '');
		return result;
	}

	public getPayableInfo(projectId: number, item: any) {
		let result = 'Id: ' + item.id + '\n';
		result += 'Name: ' + item.name + '\n';
		const pattern = 'P' + TableUtil.pad(projectId, 5) + '_' + 'P' + TableUtil.pad(item.id, 5) + '_';
		const numOfItems = this.files.filter(file => file.real_name.startsWith(pattern)).length;
		result += 'Content: ' + numOfItems + ' file' + (numOfItems > 1 ? 's' : '');
		return result;
	}

	public getProjectContent(project: any) {
		const result = this.projectsContent.filter(ct => ct.project === project.id);
		return result;
	}

	public getBills(project: any) {
		return this.bills.find(b => b.project === project.id).bills;
	}

	public getPayables(project: any) {
		return this.payables.find(b => b.project === project.id).payables;
	}

	public getBillFiles(projectId: number, billId: number) {
		const pattern = 'P' + TableUtil.pad(projectId, 5) + '_' + 'B' + TableUtil.pad(billId, 5) + '_';
		return this.files.filter(file => file.real_name.startsWith(pattern));
	}

	public getPayableFiles(projectId: number, payableId: number) {
		const pattern = 'P' + TableUtil.pad(projectId, 5) + '_' + 'P' + TableUtil.pad(payableId, 5) + '_';
		return this.files.filter(file => file.real_name.startsWith(pattern));
	}

	public pad(num, size) {
		return TableUtil.pad(num, size);
	}

	public download(fileRealName: string):  void {
		this.fileService.download(fileRealName);
	}

	public checkSearch(event: any) {
		if (event.key.toLowerCase() === 'enter') {
			this.getAll();
		}
	}
}
