import {Component, ElementRef, Injector, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {SnackBarService} from 'angular-core';

import {FormBuilder} from '@angular/forms';
import {FinanceBaseComponent} from '@finance/finance-base.component';
import {FileService} from "@finance/procedure/services/file.service";
import {TableUtil} from "@app/utils/tableUtils";
import {AccountService} from "@account/services/account.service";

@Component({
	selector	: 'company-libraries',
	templateUrl	: '../templates/company-libraries.pug',
	styleUrls	: [ '../styles/company-libraries.scss' ],
})
export class CompanyLibrariesComponent extends FinanceBaseComponent implements OnInit, OnDestroy {
	@ViewChild('fileInput') fileInput: ElementRef;
	public search: string = '';
	public data: any[] = [];
	public userProfile: any = {};

	/**
	* @constructor
	* @param {Injector} injector
	* @param {FormBuilder} fb
	* @param {ElementRef} elementRef
	* @param {SnackBarService} snackBarService
	* @param {SettingService} settingService
	*/
	constructor(
		public injector			: Injector,
		public fb				: FormBuilder,
		public elementRef		: ElementRef,
		public snackBarService	: SnackBarService,
		public accountService	: AccountService,
		public fileService		: FileService
	) {
		super( injector );
		this.accountService.getProfile().subscribe((userProfile: any) => {
			this.userProfile = userProfile;
		});
	}

	/**
	* @constructor
	*/
	public ngOnInit() {
		this.initData();
	}

	/**
	* @constructor
	*/
	public ngOnDestroy() {
		super.ngOnDestroy();
	}

	/**
	* Init all data
	* @return {void}
	*/
	public initData() {
		this.getAll();
	}

	public getAll() {
		this.fileService.getLibraryFiles(0, this.search).subscribe( res => {
			this.data = res.files;
		});
	}

	public download(fileRealName: string):  void {
		this.fileService.downloadCompanyFile(fileRealName);
	}

	public checkSearch(event: any) {
		if (event.key.toLowerCase() === 'enter') {
			this.getAll();
		}
	}


	public remove(fileRealName: string):  void {
		this.fileService.removeCompanyFile(fileRealName).subscribe((res: any) => {
			if (res && res.status) {
				this.data.splice(this.data.findIndex(file => file.real_name === fileRealName), 1);
				this.snackBarService.success(res.message);
			} else {
				this.snackBarService.warn(res.message);
			}
		});
	}

	public onFileChange(event) {
		const numOfFiles: number = this.getNumOfFiles();
		const reader = new FileReader();

		if (event.target.files && event.target.files.length) {
			const fileCheck = event.target.files[0];
			if(fileCheck.size>=100*1024*1024) {
				alert("Only support file size less than 100Mb");
				this.fileInput.nativeElement.innerText = '';
				return;
			}

			if(!(fileCheck.type.match('image/*') || fileCheck.type.match('application/pdf')
				|| fileCheck.type.match('.rar')
				|| fileCheck.type.match('.zip')
				|| fileCheck.type.match('.7z')
				|| fileCheck.type.match('.doc')
				|| fileCheck.type.match('.docx')
				|| fileCheck.type.match('application/vnd.openxmlformats-officedocument.wordprocessingml.document')
				|| fileCheck.type.match('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
				|| fileCheck.type.match('application/vnd.ms-excel')
			)) {
				alert("Unsupport file type extension");
				this.fileInput.nativeElement.innerText = '';
				return;
			}


			const filename: string = event.target.files[0].name;
			const fileTypeExtension: string = (filename && filename.indexOf('.') !== -1) ? filename.substr(filename.lastIndexOf('.') + 1) : '';
			const modFileName: string = (filename && filename.indexOf('.') !== -1)
				? (filename.substr(0, filename.lastIndexOf('.'))  + '_' + numOfFiles + '.' + fileTypeExtension)
				: (filename + '_' + numOfFiles);
			const [file] = event.target.files;
			reader.readAsDataURL(file);

			reader.onload = () => {
				this.upload(reader.result, fileTypeExtension, modFileName);
			};
		}
	}

	private upload(file, fileTypeExtension, filename): void {
		const fileRealName: string = this.getNewFileName() + (fileTypeExtension ? ('.' + fileTypeExtension) : '');
		this.fileService.uploadCompanyFile(filename, fileRealName, file).subscribe((attachment: any) => {
			this.data.push(attachment);
			this.fileInput.nativeElement.innerText = '';
		});
	}

	private getFilePrefix(): string[] {
		return [
			'Company',
			'User' + TableUtil.pad(this.userProfile.id, 5)
		];
	}

	public getNewFileName(): string {
		const listNames: string[] = this.getFilePrefix();
		listNames.push(TableUtil.pad(this.data.length, 3));
		return listNames.join('_');
	}

	public uploadNewFile() {
		this.fileInput.nativeElement.click();
	}

	private getNumOfFiles() {
		return this.data.length;
	}

	public canDelete() {
		const list = ['CEO', 'CFO', 'GENERAL_ACCOUNTANT'];
		if (this.userProfile.role_key) {
			return list.indexOf(this.userProfile.role_key) !== -1;
		}
		return false;
	}
}
