import {NgModule} from '@angular/core';

import {CoreModule} from '@core/core.module';
import {LezoModule} from '@ext/lezo/lezo.module';
/* Component Import (Do not remove) */
import {ProcedureComponent} from './components/procedure.component';
import { DialogProcedureAttachmentComponent } from './components/dialog-procedure-attachment.component';

/* Service Import (Do not remove) */
import { FileService } from './services/file.service';
import { DialogProcedureApproveComponent } from './components/dialog-procedure-approve.component';
/* End Component Import (Do not remove) */

/* End Service Import (Do not remove) */

@NgModule({
	imports: [ CoreModule, LezoModule ],
	declarations: [
		/* Component Inject (Do not remove) */
		ProcedureComponent, DialogProcedureAttachmentComponent, DialogProcedureApproveComponent
		/* End Component Inject (Do not remove) */
	],
	entryComponents: [
		/* Component Inject (Do not remove) */
		DialogProcedureAttachmentComponent, DialogProcedureApproveComponent
		/* End Component Inject (Do not remove) */
	],
	providers: [
		/* Service Inject (Do not remove) */
		FileService
		/* End Service Inject (Do not remove) */
	],
})
export class ProcedureModule {

	/**
	* @constructor
	*/
	constructor() {}

}
