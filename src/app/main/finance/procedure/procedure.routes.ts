import { Routes } from '@angular/router';

import { ProcedureComponent } from './components/procedure.component';
import { AuthGrantService } from '@auth/services/auth-grant.service';
import { AuthRoleService } from '@auth/services/auth-role.service';

export const procedureRoutes: Routes = [
	{
		path		: 'procedure',
		component	: ProcedureComponent,
		canActivate	: [ AuthGrantService, AuthRoleService ],
		data: {
			title		: 'Procedure',
			translate	: 'FINANCE.PROCEDURES_NEED_HANDLED.LABELS.PROCEDURES',
			roles		: [
				'CEO', 'CFO', 'GENERAL_ACCOUNTANT',
				'LIABILITIES_ACCOUNTANT', 'PROCUREMENT_MANAGER', 'CONSTRUCTION_MANAGER',
				'PM', 'SALE',
				'QS', 'PURCHASING'
			]
		},
	},
];
