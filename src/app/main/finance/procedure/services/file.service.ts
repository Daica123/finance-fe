import {Injectable} from '@angular/core';
import {Observable, Observer} from 'rxjs';

import {FileUtils} from '@app/utils/fileUtils';
import {ApiService} from '@core';
import {ENVIRONMENT} from '@environments/environment';

@Injectable()
export class FileService {
    constructor(private apiService: ApiService) { }

    public getBillProcedureFiles(filePrefix: string): Observable<any> {
        return new Observable((observer: Observer<any>) => {
            this.apiService
                .call(
                    '/api/finance/bill_procedure/files/list/' + 1 + '/' + filePrefix,
                    'GET'
                )
                .subscribe(
                    (result: any) => observer.next(result),
                    (error: any) => observer.error(error),
                    () => observer.complete()
                );
        });
    }

    public getPayableProcedureFiles(filePrefix: string): Observable<any> {
        return new Observable((observer: Observer<any>) => {
            this.apiService
                .call(
                    '/api/finance/bill_procedure/files/list/' + 0 + '/' + filePrefix,
                    'GET'
                )
                .subscribe(
                    (result: any) => observer.next(result),
                    (error: any) => observer.error(error),
                    () => observer.complete()
                );
        });
    }

    // type = 2 : bill procedures, 1: payable procedures, 0: companies
    public getLibraryFiles(type: number, search?: string): Observable<any> {
        return new Observable((observer: Observer<any>) => {
            this.apiService
                .call(
                    '/api/finance/bill_procedure/libraries/list/' + type,
                    'GET'
                )
                .subscribe(
                    (result: any) => {
                        if (type) {
                            const seperate = search
                                ? result.seperate.filter(sp => [
                                    sp.project.name.toLowerCase(), sp.procedure.name.toLowerCase(),
                                    (sp.bill && sp.bill.id) ? sp.bill.name.toLowerCase() : null,
                                    (sp.payable && sp.payable.id) ? sp.payable.name.toLowerCase() : null,
                                    (sp.category && sp.category.id) ? sp.category.name.toLowerCase() : null
                                ].filter(it => it).join('_').indexOf(search.toLowerCase()) !== -1)
                                : result.seperate;
                            const originFiles: any[] = result.files;
                            const files: any[] = [];
                            originFiles.forEach(file => {
                                const filename = file.real_name;
                                const item = {
                                    project: null,
                                    bill: null,
                                    payable: null,
                                    procedure: null,
                                    category: null
                                };
                                const params = filename.split('_');
                                // type: 1 : B, 0: P
                                const type = params[1].startsWith('B') ? 1 : 0;
                                item.project = Number.parseInt(params[0].replace(/\D/g, ''), 10);
                                const billOrPayable = Number.parseInt(params[1].replace(/\D/g, ''), 10);
                                if (type) {
                                    delete item.payable;
                                    item.bill = billOrPayable;
                                } else {
                                    delete item.bill;
                                    item.payable = billOrPayable;
                                }
                                item.procedure = Number.parseInt(params[2].replace(/\D/g, ''), 10);
                                item.category = Number.parseInt(params[3].replace(/\D/g, ''), 10);
                                if (seperate.findIndex(sp => sp.project.id === item.project && sp.procedure.id === item.procedure
                                    && sp.category.id === item.category && ((sp.payable && sp.payable.id) ? sp.payable.id === item.payable : true)
                                    && ((sp.bill && sp.bill.id) ? sp.bill.id === item.bill : true)) !== -1) {
                                    files.push({
                                        ...file
                                    });
                                }
                            });
                            const modified = {
                                files,
                                seperate
                            };
                            observer.next(modified);
                        } else {
                            let searchFiles: any = [];
                            if (search) {
                                searchFiles = result.files.filter(file => file.name.toLowerCase().indexOf(search.toLowerCase()) !== -1 ||
                                                                    file.real_name.toLowerCase().indexOf(search.toLowerCase()) !== -1);
                            } else {
                                searchFiles = result.files;
                            }
                            observer.next({
                                files: searchFiles
                            });
                        }
                    },
                    (error: any) => observer.error(error),
                    () => observer.complete()
                );
        });
    }

    public upload(fileName: string, fileRealName: string, fileContent: string): Observable<any> {
        return new Observable((observer: Observer<any>) => {
            this.apiService
                .call(
                    '/api/finance/bill_procedure/files',
                    'PUT',
                    {name: fileName, real_name: fileRealName, content: fileContent}
                )
                .subscribe(
                    (result: any) => observer.next(result),
                    (error: any) => observer.error(error),
                    () => observer.complete()
                );
        });
    }

    public uploadInvoice(fileName: string, fileRealName: string, fileContent: string): Observable<any> {
        return new Observable((observer: Observer<any>) => {
            this.apiService
                .call(
                    '/api/finance/bill_procedure/files/invoice',
                    'PUT',
                    {name: fileName, real_name: fileRealName, content: fileContent}
                )
                .subscribe(
                    (result: any) => observer.next(result),
                    (error: any) => observer.error(error),
                    () => observer.complete()
                );
        });
    }

    public uploadCompanyFile(fileName: string, fileRealName: string, fileContent: string): Observable<any> {
        return new Observable((observer: Observer<any>) => {
            this.apiService
                .call(
                    '/api/finance/bill_procedure/files/companies',
                    'PUT',
                    {name: fileName, real_name: fileRealName, content: fileContent}
                )
                .subscribe(
                    (result: any) => observer.next(result),
                    (error: any) => observer.error(error),
                    () => observer.complete()
                );
        });
    }

    public download(fileRealName: string): void {
        const params: string[] = fileRealName.split('_');
        const billOrPayable: string = params[1].startsWith('B') ? 'bills' : 'payables';
        const url= ENVIRONMENT.SERVER_API_URL + '/public/uploads/projects/' + billOrPayable + '/procedures/' + fileRealName;
        FileUtils.download_file(url, fileRealName);
    }

    public downloadPublic(path: string): void {
        const url= ENVIRONMENT.SERVER_API_URL + '/' + path;
        FileUtils.download_file(url, '');
    }

    public downloadCompanyFile(fileRealName: string): void {
        const url= ENVIRONMENT.SERVER_API_URL + '/public/uploads/companies/' + fileRealName;
        FileUtils.download_file(url, fileRealName);
    }

    public downloadInvoiceFile(fileRealName: string): void {
        const params: string[] = fileRealName.split('_');
        const billOrPayable: string = params[1].startsWith('B') ? 'bills' : 'payables';
        const url= ENVIRONMENT.SERVER_API_URL + '/public/uploads/projects/' + billOrPayable + '/invoices/' + fileRealName;
        FileUtils.download_file(url, fileRealName);
    }

    public remove(fileRealName): Observable<any> {
        return new Observable((observer: Observer<any>) => {
            this.apiService
                .call(
                    `/api/finance/bill_procedure/files/${fileRealName}`,
                    'DELETE'
                )
                .subscribe(
                    (result: any) => observer.next(result),
                    (error: any) => observer.error(error),
                    () => observer.complete()
                );
        });
    }

    public removeCompanyFile(fileRealName): Observable<any> {
        return new Observable((observer: Observer<any>) => {
            this.apiService
                .call(
                    `/api/finance/bill_procedure/files/companies/${fileRealName}`,
                    'DELETE'
                )
                .subscribe(
                    (result: any) => observer.next(result),
                    (error: any) => observer.error(error),
                    () => observer.complete()
                );
        });
    }

    public removeInvoiceFile(fileRealName): Observable<any> {
        return new Observable((observer: Observer<any>) => {
            this.apiService
                .call(
                    `/api/finance/bill_procedure/files/invoice/${fileRealName}`,
                    'DELETE'
                )
                .subscribe(
                    (result: any) => observer.next(result),
                    (error: any) => observer.error(error),
                    () => observer.complete()
                );
        });
    }
}