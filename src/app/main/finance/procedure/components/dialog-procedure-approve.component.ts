import {Component, Inject, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {SnackBarService} from 'angular-core';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import _ from 'underscore';
import {BillProcedureService} from "@finance/project/services/bill-procedure.service";
import { BILL_PROCEDURE_STATUS } from '@resources';

@Component({
	selector		: 'dialog-procedure-approve',
	templateUrl		: '../templates/dialog-procedure-approve.pug',
	styleUrls		: [ '../styles/dialog-procedure-approve.scss' ],
	encapsulation	: ViewEncapsulation.None,
})
export class DialogProcedureApproveComponent implements OnInit, OnDestroy {
	public approveForm: FormGroup;
	public isSubmitting: boolean;
	public billProcedureApprove: any = {
		status: null,
		id: null,
		confirmation: null,
		comment: null
	};

	constructor(
		@Inject( MAT_DIALOG_DATA ) public data: any,
		public dialogRef				: MatDialogRef<DialogProcedureApproveComponent>,
		public dialog					: MatDialog,
		public fb						: FormBuilder,
		public snackBarService			: SnackBarService,
		public billProcedureService		: BillProcedureService,
		public translateService			: TranslateService
	) {
		this.approveForm = fb.group({
			comment: [{ value: null, disabled: false }],
		});
	}

	/**
	* @constructor
	*/
	public ngOnInit() {
		this.data && _.assign( this.billProcedureApprove, this.data );
	}

	/**
	* @constructor
	*/
	public ngOnDestroy() {
	}

	/**
	* Click No button event
	* @return {void}
	*/
	public onNoClick() {
		this.dialogRef.close();
	}

	/**
	* Click No button event
	* @return {void}
	*/
	public closeDiaglog() {
		this.dialogRef.close( true );
	}

	public update() {
		this.isSubmitting = true;

		const updateData: any = {
			id		: this.billProcedureApprove.id,
			status	: this.billProcedureApprove.status,
			comment	: this.billProcedureApprove.comment,
		};
		this.billProcedureService.updateStatus(updateData).subscribe((result: any) => {
			this.isSubmitting = false;
			if ( !result || !result.status ) {
				if (this.billProcedureApprove.status === BILL_PROCEDURE_STATUS.CONFIRMED) {
					this.snackBarService.warn('APPROVE_BILL_PROCEDURE_FAILED');
				} else {
					this.snackBarService.warn('REJECT_BILL_PROCEDURE_FAILED');
				}
				return;
			}
			if (this.billProcedureApprove.status === BILL_PROCEDURE_STATUS.CONFIRMED) {
				this.snackBarService.success('APPROVE_BILL_PROCEDURE_SUCCESS');
			} else {
				this.snackBarService.success('REJECT_BILL_PROCEDURE_SUCCESS');
			}
		});
		this.dialogRef.close( true );
	}
}
