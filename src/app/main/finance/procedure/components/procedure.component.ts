import {Component, Injector, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatTable, MatTableDataSource} from '@angular/material';
import {TranslateService} from '@ngx-translate/core';
import {DialogConfirmComponent, SnackBarService} from '@core';
import {Direction} from '@finance/finance-direction.component';
import {BillProcedureService} from '@finance/project/services/bill-procedure.service';
import {ProjectBaseComponent} from "@finance/project/project-base.component";
import {MatSort} from "@angular/material/sort";

import _ from 'underscore';
import { BILL_PROCEDURE_STATUS, COLORS} from '@resources';
import {AccountService} from "@account/services/account.service";
import {DialogProcedureAttachmentComponent} from "@finance/procedure/components/dialog-procedure-attachment.component";
import {FileService} from "@finance/procedure/services/file.service";
import {DialogProcedureApproveComponent} from "@finance/procedure/components/dialog-procedure-approve.component";
import {TableUtil} from "@app/utils/tableUtils";

const roles = [
	'CEO', 'CFO', 'GENERAL_ACCOUNTANT',
	'LIABILITIES_ACCOUNTANT', 'PROCUREMENT_MANAGER', 'CONSTRUCTION_MANAGER',
	'PM', 'SALE',
	'QS', 'PURCHASING'
];

@Component({
	selector	: 'procedure',
	templateUrl	: '../templates/procedure.pug',
	styleUrls	: [ '../styles/procedure.scss' ],
})
@Direction({
	path	: 'procedure',
	data	: { title: 'FINANCE.DIRECTION.PROCEDURES_NEED_HANDLED', icon: 'icon icon-procedure' },
	priority: 1,
	roles
})
export class ProcedureComponent extends ProjectBaseComponent implements OnInit, OnDestroy {
	@ViewChild(MatTable) matTable: MatTable<any>;
	@ViewChild('procedureSort') public procedureSort: MatSort;
	public COLORS: any = COLORS;
	public userProfile: any = {};

	public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>( [] );
	public displayedColumns: Array<string> = [
		'project', 'bill', 'name', 'belong_to', 'assignee', 'status',
		'deadline', 'attachments', 'comment', 'actions'
	];

	constructor(
		public injector				: Injector,
		public dialog				: MatDialog,
		public billProcedureService	: BillProcedureService,
		public accountService		: AccountService,
		public snackBarService		: SnackBarService,
		public translateService		: TranslateService,
		public fileService			: FileService
	) {
		super( injector );
	}

	/**
	* @constructor
	*/
	public ngOnInit() {
		this.initData();
		this.dataSource.sortingDataAccessor = (data: any, sortHeaderId: string) => {
			const value: any = data[ sortHeaderId ];
			return typeof value === 'string' ? value.toLowerCase() : value;
		};
	}

	/**
	* @constructor
	*/
	public ngOnDestroy() {
		super.ngOnDestroy();
	}

	/**
	* Init all data
	* @return {void}
	*/
	public initData() {
		this.getList();
	}

	/**
	* Get user list
	* @return {void}
	*/
	public getList() {
		Promise.all([
			this.accountService.getProfile(),
			this.billProcedureService.getProcedures()
		]).then(initRes => {
			initRes[0].subscribe((userProfile: any) => {
				this.userProfile = userProfile;
				initRes[1].subscribe((procedures: any[]) => {
					procedures.forEach((procedure: any) => {
						procedure.bill_procedure_status_name = _.findWhere( this.billProcedureStatus, { id: procedure.status || 0 } );
						procedure.bill_procedure_status_name = _.findWhere( this.billProcedureStatus, {
							id: (procedure.status === 2) ? procedure.status : ((this.datediff(procedure.deadline) > 0) ? 0 : 1)
						} );
						procedure.project_name = procedure.bill.project.name;
						procedure.bill_name = procedure.bill.name;
						procedure.assignee_full_name = procedure.assignee.full_name;
						procedure.doc_category_name = procedure.doc_category.name;
					});
					this.dataSource.data = procedures;
					this.dataSource.sort = this.procedureSort;
					this.matTable.renderRows();
				});
			});
		});
	}

	/**
	* Delete user
	* @param {any} user - User to delete
	* @return {void}
	*/
	public delete( user: any ) {
		const dialogRef: any = this.dialog.open(
			DialogConfirmComponent,
			{
				width: '400px',
				data: {
					title	: this.translateService.instant( 'FINANCE.USER.TITLES.DELETE_USER' ),
					content	: this.translateService.instant( 'FINANCE.USER.MESSAGES.DELETE_USER_CONFIRMATION', user ),
					actions: {
						yes: { color: 'warn' },
					},
				},
			}
		);

		dialogRef
		.afterClosed()
		.subscribe( ( _result: any ) => {
			if ( !_result ) return;
		} );
	}

	public canApprove() {
		const list = ['CEO', 'CFO', 'GENERAL_ACCOUNTANT', 'PM'];
		if (this.userProfile.role_key) {
			return list.indexOf(this.userProfile.role_key) !== -1;
		}
		return false;
	}

	public canReject() {
		const list = ['CEO', 'CFO'];
		if (this.userProfile.role_key) {
			return list.indexOf(this.userProfile.role_key) !== -1;
		}
		return false;
	}

	public canAttachment(item: any) {
		const can: boolean = this.userProfile.id == item.assignee.id;
		return can;
	}

	public openDialog(item) {
		const dialogRef: any = this.dialog.open(
			DialogProcedureAttachmentComponent,
			{
				hasBackdrop: true,
				position: {
					top: '70px'
				},
				width: '1050px',
				data: item
			}
		);
		dialogRef
			.afterClosed()
			.subscribe( ( _result: any ) => {
				this.getList();
			} );
	}

	public download(fileRealName: string):  void {
		this.fileService.download(fileRealName);
	}

	public approve( billProcedure: any ) {
		const dialogData: any = {
			id		: billProcedure.id,
			status	: BILL_PROCEDURE_STATUS.CONFIRMED,
			confirmation:  this.translateService.instant(
				'APPROVE_BILL_PROCEDURE_CONFIRMATION',
				{name: billProcedure.name}
			)
		};
		const dialogRef: any = this.dialog.open(
			DialogProcedureApproveComponent,
			{
				width	: '400px',
				data	: dialogData,
			}
		);

		dialogRef
			.afterClosed()
			.subscribe( ( _result: any ) => {
				if ( !_result ) return;
				this.getList();
			} );
	}

	public reject( billProcedure: any ) {
		const dialogData: any = {
			id			: billProcedure.id,
			status		: BILL_PROCEDURE_STATUS.DELAYED,
			confirmation:  this.translateService.instant(
				'REJECT_BILL_PROCEDURE_CONFIRMATION',
				{name: billProcedure.name}
			)
		};
		const dialogRef: any = this.dialog.open(
			DialogProcedureApproveComponent,
			{
				width	: '400px',
				data	: dialogData,
			}
		);

		dialogRef
			.afterClosed()
			.subscribe( ( _result: any ) => {
				if ( !_result ) return;
				this.getList();
			} );
	}

	public datediff(d2: Date) {
		const d1 = new Date();
		return TableUtil.datediff(d1, new Date(d2));
	}

	public datediff2(item: any) {
		const d2 = new Date(item.deadline);
		const d1 = new Date(item.updated_at);
		return TableUtil.datediff(d1, d2);
	}
}
