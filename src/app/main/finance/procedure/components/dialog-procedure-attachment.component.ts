import {Component, ElementRef, Inject, Injector, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatTable, MatTableDataSource} from '@angular/material';
import {SnackBarService} from 'angular-core';
import {TranslateService} from '@ngx-translate/core';

import {FinanceBaseComponent} from '@finance/finance-base.component';
import {ProjectService} from '@finance/project/services/project.service';
import {VOService} from '@finance/project/services/vo.service';
import {ExcelService} from '@ext/lezo/services/excel.service';
import {UserService} from "@finance/user/services/user.service";
import {AccountService} from "@account/services/account.service";
import {FileService} from '../services/file.service';
import {TableUtil} from "@app/utils/tableUtils";
import {CONSTANTS} from "@resources";

@Component({
	selector		: 'dialog-procedure-attachment',
	templateUrl		: '../templates/dialog-procedure-attachment.pug',
	styleUrls		: [ '../styles/dialog-procedure-attachment.scss' ],
	encapsulation	: ViewEncapsulation.None,
})
export class DialogProcedureAttachmentComponent extends FinanceBaseComponent implements OnInit, OnDestroy {
	@ViewChild('fileInput') fileInput: ElementRef;
	@ViewChild(MatTable) matTable: MatTable<any>;
	public procedure: any;
	public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>( [] );
	public displayedColumns: Array<string> = [
		'no', 'name', 'actions'
	];

	constructor(
		@Inject( MAT_DIALOG_DATA ) public data: any,
		public dialogRef				: MatDialogRef<DialogProcedureAttachmentComponent>,
		public dialog					: MatDialog,
		public injector					: Injector,
		public snackBarService			: SnackBarService,
		public translateService			: TranslateService,
		public excelService				: ExcelService,
		public projectService			: ProjectService,
		public userService				: UserService,
		public accountService			: AccountService,
		public voService				: VOService,
		public fileService				: FileService
	) {
		super( injector );
	}

	/**
	* @constructor
	*/
	public ngOnInit() {
		this.initData();
	}

	private initData() {
		this.procedure = this.data;
		const filePrefix: string = this.getFilePrefix().join('_') + '_';
		this.fileService.getBillProcedureFiles(filePrefix).subscribe((attachments: any[]) => {
			this.dataSource.data = attachments || [];
		});
	}

	/**
	* @constructor
	*/
	public ngOnDestroy() {
		super.ngOnDestroy();
	}

	/**
	* Click No button event
	* @return {void}
	*/
	public onNoClick() {
		this.dialogRef.close();
	}

	/**
	* Click No button event
	* @return {void}
	*/
	public closeDiaglog() {
		this.dialogRef.close( true );
	}

	public pad(num, size) {
		return TableUtil.pad(num, size);
	}

	public download(fileRealName: string):  void {
		this.fileService.download(fileRealName);
	}

	public remove(fileRealName: string):  void {
		this.fileService.remove(fileRealName).subscribe(() => {
			this.dataSource.data.splice(this.dataSource.data.findIndex(file => file.real_name === fileRealName), 1);
			this.matTable.renderRows();
		});
	}

	public onFileChange(event) {
		const numOfFiles: number = this.getNumOfFiles();
		const reader = new FileReader();

		if (event.target.files && event.target.files.length) {
			const fileCheck = event.target.files[0];
			if(!(fileCheck.type.match('image/*') || fileCheck.type.match('application/pdf'))) {
				this.snackBarService.warning( 'FORM_ERROR_MESSAGES.INVALID_FILE_TYPE' );
				this.fileInput.nativeElement.innerText = '';
				return;
			}

			if ( fileCheck.size > CONSTANTS.ALLOW_FILE_SIZE ) {
				this.snackBarService.warning( 'FORM_ERROR_MESSAGES.INVALID_FILE_SIZE' );
				this.fileInput.nativeElement.innerText = '';
				return;
			}

			const filename: string = event.target.files[0].name;
			const fileTypeExtension: string = (filename && filename.indexOf('.') !== -1) ? filename.substr(filename.lastIndexOf('.') + 1) : '';
			const modFileName: string = (filename && filename.indexOf('.') !== -1)
				? (filename.substr(0, filename.lastIndexOf('.'))  + '_' + numOfFiles + '.' + fileTypeExtension)
				: (filename + '_' + numOfFiles);
			const [file] = event.target.files;
			reader.readAsDataURL(file);

			reader.onload = () => {
				this.upload(reader.result, fileTypeExtension, modFileName);
			};
		}
	}

	private upload(file, fileTypeExtension, filename): void {
		const fileRealName: string = this.getNewFileName() + (fileTypeExtension ? ('.' + fileTypeExtension) : '');
		this.fileService.upload(filename, fileRealName, file).subscribe((attachment: any) => {
			this.dataSource.data.push(attachment);
			this.fileInput.nativeElement.innerText = '';
			this.matTable.renderRows();
		});
	}

	private getFilePrefix(): string[] {
		return [
			'P' + TableUtil.pad(this.procedure.bill.project.id, 5),
			'B' + TableUtil.pad(this.procedure.bill.id, 5),
			'Procedure' + TableUtil.pad(this.procedure.id, 3),
			'C' + TableUtil.pad(this.procedure.doc_category.id, 3)
		];
	}

	public getNewFileName(): string {
		const listNames: string[] = this.getFilePrefix();
		listNames.push(TableUtil.pad(this.dataSource.data.length, 3));
		return listNames.join('_');
	}

	public uploadNewFile() {
		this.fileInput.nativeElement.click();
	}

	private getNumOfFiles() {
		return this.dataSource.data.length;
	}
}
