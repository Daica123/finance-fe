import {
	AfterViewInit, OnInit, Injector,
	OnDestroy, Component
} from '@angular/core';

import { BootstrapComponent } from '@app/bootstrap.component';
import { TranslateService } from '@ngx-translate/core';
import {LANGUAGES } from '../../app.static';
import { AppState } from '../../app-state.service';

@Component({
	selector	: 'finance-app',
	templateUrl	: '../../../templates/finance-app.pug',
	host		: { class: 'flex-noshrink layout-column' },
})
export class FinanceComponent extends BootstrapComponent implements OnInit, OnDestroy, AfterViewInit {
	public selectedLanguage: any;
	public isLanguagesMenuOpened: boolean = false;
	/**
	* @constructor
	* @param {Injector} injector
	* @param {ApiService} apiService
	* @param {AccountService} accountService
	* @param {ServiceWorkerService} serviceWorkerService
	* @param {AuthService} authService
	* @param {TranslateService} translateService
	* @param {PageService} pageService
	*/
	constructor( public injector: Injector,
				 public translate: TranslateService,
				 private appState: AppState ) {
		super( injector );
		const _this = this;
		this.appState.locale = LANGUAGES[0];
		this.selectedLanguage = this.appState.locale;
		this.translate.onLangChange.subscribe(
			() => {
				_this.selectedLanguage = _this.appState.locale;
			}
		);
	}

	/**
	* @constructor
	*/
	public ngOnInit() {
		super.ngOnInit();
	}

	/**
	* @constructor
	*/
	public ngOnDestroy() {
		super.ngOnDestroy();
	}

	/**
	* @constructor
	*/
	public ngAfterViewInit() {
		super.ngAfterViewInit();
	}

	public languages() {
		return LANGUAGES;
	}

	updateLanguage(language: any) {
		this.appState.locale = language;
		this.translate.use(language.code);
		this.selectedLanguage = this.appState.locale;
	}
}
