import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { ENVIRONMENT } from './environments/environment';

if (!(window as any).Zone) {
	// @ts-ignore
	require('zone.js/dist/zone');
}

if ( ENVIRONMENT.production ) {
	enableProdMode();
} else {
	if ( ( <any> module ).hot ) ( <any> module ).hot.accept();
}

// Add title to <head>
const title: HTMLElement = document.createElement( 'title' );
title.innerHTML = ENVIRONMENT.APP_TITLE;
document.head.appendChild( title );
platformBrowserDynamic().bootstrapModule( AppModule )
/* tslint:disable-next-line */
.catch( ( error: any ) => console.log( error ) );
