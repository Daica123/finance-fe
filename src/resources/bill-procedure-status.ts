export const BILL_PROCEDURE_STATUS: any = {
	WAITING		: 0,
	DELAYED		: 1,
	CONFIRMED	: 2
};
