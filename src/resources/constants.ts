import { CONSTANTS as _CONSTANTS } from 'angular-core';

export const CONSTANTS: any = {
	..._CONSTANTS,
	ALLOW_FILE_SIZE: 100*1024*1024, // 100 mb
};
